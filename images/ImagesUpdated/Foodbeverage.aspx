﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Foodbeverage.aspx.cs" Inherits="Foodbeverage" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <!-- Sliding start -->
    
   <%-- <div class="b-headlines"style="background-image:url(images/ImagesUpdated/Food.jpg);height:350px;padding-top:0px;">
        <div class="container" >
          <div class="b-breadcrumbs">
           
          </div>
          
        </div>
      </div>--%>
     <!-- Sliding end -->

    <div class="b-events" style="background-image:url(images/ImagesUpdated/Food.jpg);background-attachment:fixed;">
        <div class="container"style="margin-bottom:100px;" >
          <div class="row" style="padding-top:250px;">
            <div class="b-tabs col-lg-8" style="width:99%;">
              <h6 class="s6-heading b-tabs__ttl" style="color:white;">FOOD & BEVERAGE MENU</h6>
            
              <div class="b-tabs__content">
                <div id="block-a" role="tabpanel" class="b-tabs__pane fade in active row">
                  <div class="b-tabs__pane-item col-sm-6">
                    <div class="c-box">
                      <div class="c-box__inner">
                        <div class="c-box__cont" >
                          <h3 class="c-box__ttl-s3">FOOD MENU</h3>
                          <hr class="hr-pattern">
<%--                          <p class="c-box__txt">Stay B, Andrew Feeling, Someone Else</p>--%>
                        </div>
                        <div  class="b-box__img-wrap  bg-image">
                            
                           <a href="pdf/HUL%20Food%20Menu.pdf"><img src="images/ImagesUpdated/foodMenuPic.jpg"  class="b-box__img-wrap bg-image" style="filter:opacity(100%);width:650px;height:420px;" /></a>   
                        </div>
                                                               <!-- <div class="btn-pointer-b__wrap"style="left:108px;bottom:-55PX;">
            <a href="pdf/HUL%20Food%20Menu.pdf" target="_blank" class="btn-pointer-b btn-anim-b"><b>FOOD MENU</b></a>
            <svg class="left" x="0px" y="0px" viewBox="0 0 5 50" enable-background="new 0 0 5 50" xml:space="preserve">
            <path fill="#000000" d="M5.0001,49.0002H0V33.8014l4.4054-4.4053L0,24.9905v-0.9612l4.4054-4.4051L0,15.2184V0h5.0001V49.0002z"/>
            </svg>
            
            <svg class="right" x="0px" y="0px" viewBox="0 0 5 50" enable-background="new 0 0 5 50" xml:space="preserve">
            <path fill="#000000" d="M0,0l5.0001,0v15.1988l-4.4054,4.4053l4.4054,4.4056v0.9612L0.5947,29.376l4.4054,4.4058v15.2184H0L0,0z"/>
            </svg>
            
          </div>-->
                      </div>
                    </div>
                  </div>
                       <div class="b-tabs__pane-item col-sm-6">
                    <div class="c-box">
                      <div class="c-box__inner">
                        <div class="c-box__cont">
                          <h3 class="c-box__ttl-s3">BEVERAGE MENU</h3>
                          <hr class="hr-pattern">
                        </div>
                        <div  class="b-box__img-wrap  bg-image">
                             <a href="pdf/Beverage%20Menu.pdf"><img src="images/ImagesUpdated/X-6744-960x500.jpg""  class="b-box__img-wrap bg-image" style="filter:opacity(100%);width:650px;height:420px;" /></a>  
                        </div>
                          
                        <div class="c-box__link-wrap">
                             <!--     <div class="btn-pointer-b__wrap"style="left:-25px;bottom:155PX;">
            <a href="pdf/HUL%20Beverage%20(1).pdf" target="_blank" class="btn-pointer-b btn-anim-b"><b>Beverage Menu</b></a>
            <svg class="left" x="0px" y="0px" viewBox="0 0 5 50" enable-background="new 0 0 5 50" xml:space="preserve">
            <path fill="#000000" d="M5.0001,49.0002H0V33.8014l4.4054-4.4053L0,24.9905v-0.9612l4.4054-4.4051L0,15.2184V0h5.0001V49.0002z"/>
            </svg>
            
            <svg class="right" x="0px" y="0px" viewBox="0 0 5 50" enable-background="new 0 0 5 50" xml:space="preserve">
            <path fill="#000000" d="M0,0l5.0001,0v15.1988l-4.4054,4.4053l4.4054,4.4056v0.9612L0.5947,29.376l4.4054,4.4058v15.2184H0L0,0z"/>
            </svg>
            
          </div>-->


                      </div>
                    </div>
                  </div>
                    </div>
                  </div>
                </div></div>
            </div>
        </div>
        </div>
</asp:Content>

