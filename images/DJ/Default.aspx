<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="Hul_login._Default" %>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
   <style>



       h1 {
    font-family: 'Univers LT W01_57 Condensed' !important;
    font-size: 10vw;
    width: 100vw;
    margin-top: calc(50vh - 10vw);
    text-align: center;
    background: linear-gradient(
       160deg,
        hsl(0, 75%, 50%) 10%,
       hsl(0, 0%, 70%) 10%,
        hsl(0, 0%, 70%) 25%,       
     hsl(0, 75%, 50%) 25%,
     hsl(0, 75%, 50%) 40%,     
     hsl(0, 0%, 70%) 40%,
     hsl(0, 0%, 70%) 55%,        
     hsl(0, 75%, 50%) 55%,
     hsl(0, 75%, 50%) 70%,
     hsl(0, 0%, 70%) 70%,
    hsl(0, 0%, 70%) 85%,
        hsl(0, 75%, 50%) 85%
    );

    /*text-shadow: 0.5px -0.6vw #fff4;*/
    color: #fff;
    -webkit-background-clip: text;
    -webkit-text-fill-color: transparent;
    animation: 10s BeProud linear infinite,
    3s Always ease alternate infinite;
}

@keyframes BeProud {
    100% { background-position: 100vw 0px; }
}

      @keyframes Always {
          100% {
              transform: scale(1.1);
          }
      }


      .mybackground{
            background-attachment:fixed;
            background-size:cover;
            background-repeat:no-repeat;
            height:100%;
            width:100%;
        }




      #content {
      padding: 0;
      border: 0px solid black;
      width: auto;
      height: 400px;
      }
      .galleryItem {
      border: 0px solid black;
      height: 100px;
      width: 250px;
      margin-left: 10px;
      clear: both;
      -webkit-transition: line-height 1s, height 1s, opacity 1s, border-radius 3s;
      }
      .galleryItem:hover {
      background-color:#202020;
      color: #fff;
      height: 233px;
      line-height:50px;
      display: block;
      border-radius: 20px;
      opacity:0.8;
      }
      .galleryItem p {
      display: none;
      margin-top:2px!important;
          font-size: 13px;
      }
      .galleryItem:hover p {
      display: block;
      }
      .box {
      position: relative; 
      top: -90px;
      padding: 5px 15px 7px 20px;
      }
      .title
      {
      font-weight: bold;
      color:#EE3124;
      /*font-size: 87px;*/
      margin-top: -105px;
      } 
      @media screen  and (max-width: 600px) {
      .title {
      color:#EE3124;
      font-weight: bold;
      font-size: 59px;
      margin-top: 110px;            
      }
      .mySlides {
      display: none;
      border-radius: 10px;
      }
      .c-box__inner{
      width:100%!important;
      }
      .b-panel-t{
      width:100%!important;
      margin-top:150px;
      line-height:20px;
      height:auto!important;
      }
      .txtfix{
      line-height:40px!important;
      margin-right: 10px;
      }
       .txtfix1{
     
 margin-top:60px!important;
      }
      }    
   </style>
     <script type="text/javascript" src="/Scripts/jquery-1.10.2.js"></script>
    <script type="text/javascript" src="/Scripts/jquery-1.10.2.min.js"></script>
    <script type="text/javascript">
        $(function () {
            var body = $('body');
            var backgrounds = [
                'url(slider1.jpg)',
                'url(slider2.jpg)',
                 'url(slider3.jpg)',
             'url(slider4.jpg)',
             'url(slider5.jpg)',
             'url(slider6.jpg)'];
            var current = 0;

            function nextBackground() {
                body.css(
                    'background-image',
                    backgrounds[current = ++current % backgrounds.length]);

                setTimeout(nextBackground, 5000);
               
            }
            setTimeout(nextBackground, 5000);
            body.css('background-image', backgrounds[0]);
            $("body").addClass("mybackground");
           
           
        });
    </script>





   <div class="main-slide ">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
      <div class="main-slide__btn-item " style="opacity: 0.1;"></div>
      <div id="sliderMain" class="main-slide__slider ">
         <div class="main-slide__item-a "> <div class="txtfix1" style="margin-top:-70px">
                     
                  <h1 style="font-size:80px;">HIGH Ultra Lounge</h1></div>
           <%-- <div data-bgimage="images/slider1.jpg" class="main-slide__img-wrap bg-image" style="left:0px; background-attachment:fixed;  position:absolute" ></div>--%>
            <%--<center >  <h1 class="title" style="color:#EE3124;margin-left:-530px;padding-left:474px;font-size: 70px;font-family:'Univers LT W01_57 Condensed' !important;"> Welcome to High Ultra Lounge</h1> </center>--%> 
            <div class="row">
               <div class="">
                  <div class="">
                     
                  </div>
               </div>
            </div>
            <div class="container">
               <div class="row">
                  <div class="b-tabs col-lg-8" style="width:99%;">
                     <div class="b-content container ">
                        <div class="row">
                           <div class="b-panel-t box col-xs-12 col-lg-2 col-md-2 col-sm-3 col-xm-12" style="top: 22px;   padding-top:20px; width:233px; height:60px; /*margin-bottom: -32px;    opacity: 0.7;    width: 20%;    height: 10px;   padding-bottom:28px;padding-top:20px; margin-left: -1.3%;*/ border-left: 0px solid #800080;" >
                              <b style="color:white; text-align:justify;font-family:'Univers LT W01_57 Condensed' !important; padding: 8px 15px 6px 15px !important; font-size: 24px; padding:0px!important;   /*padding-left:8%*/  ">FEATURED EVENTS</b>
                              <%-- <div class="b-panel-t__list col-md-12 col-xs-3 ">
                                 <p  style="color:white;text-align:inherit;"> </p>
                                 
                                         </div>--%>
                           </div>
                        </div>
                     </div>
                     <%--<h6 class="s6-heading b-tabs__ttl" style=" font-family:'Univers LT W01_57 Condensed' !important";>Featured Events</h6>--%>
                     <ul role="" class="">
                        <li role="presentation" class="active">
                           <%--<a href="#block-a" aria-controls="block-a" role="tab" data-toggle="tab">--%>
                           <%-- <svg baseProfile="tiny" width="10" height="10" x="0px" y="0px" viewBox="0 0 10 10" xml:space="preserve">
                              <path fill="#231F20" d="M0,4h4V0H0V4z M6,0v4h4V0H6z M0,10h4V6H0V10z M6,10h4V6H6V10z"/>
                              </svg>--%>
                       <%--    </a>--%>
                        </li>
                        <li role="presentation">
                           <a href="#block-b" aria-controls="block-b" role="tab" data-toggle="tab">
                           <%-- <svg baseProfile="tiny" width="10" height="10" x="0px" y="0px" viewBox="0 0 10 10" xml:space="preserve">
                              <path fill="#231F20" d="M0,0v4h10V0H0z M10,10V6H0v4H10z"/>
                              </svg>--%>
                           </a>
                        </li>
                     </ul>
                     <div class="b-tabs__content">
                        <div id="block-a" role="tabpanel" class="b-tabs__pane fade in active row">
                         
                           <div class="row">
                            
                              <div class="col-md-5 col-lg-5 col-sm-12 col-xm-12 col-xs-12 ">
                                 <div class="b-tabs__pane-item col-sm-6">
                                    <%--  <div class="c-box">--%>
                                    <div class="c-box__inner" style="height:342px; width:549.14px; margin-bottom:20px; background-image:url(images/DJ/silvr.jpg); background-size:cover!important">
                                       <div class="c-box__cont">
                                          <h3 class="c-box__ttl-s3" style=" font-family:'Univers LT W01_57 Condensed' !important">DJ Silvr</h3>
                                        <%--  <hr class="hr-pattern">--%>
                                          <%--       <p class="c-box__txt" style=" font-family:'Univers LT W01_57 Condensed' !important">EDM, Commercial, Hip Hop</p>--%>
                                          <p class="c-box__txt" style=" font-family:'Univers LT W01_57 Condensed' !important";>
                                             <!--Collapsive-->                       
                                          <div id="container">
                                             <div id="content">
                                                <div class="galleryContainer">
                                                   <div class="galleryItem">
                                                      About Event
                                                     <%-- <p> Dance the night away to the melodious Persian and Arabic tunes by DJ Electro Smashers</p>--%>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                          </p>     
                                       </div>
                                       <div data-bgimage="../uploads/events/EventsTiles_04_Retina.jpg" class="b-box__img-wrap b-box__grdnt-a bg-image"></div>
                                       <div class="c-box__date-item" style="margin:3px">
                                          <time datetime="2009-08-29T20:00" class="date c-box__date"><span class="date__dt">5</span><span class="date__rh"><span class="date__rh-m">Oct</span><span class="date__rh-d">Fri</span></span><span class="date__ad">9:00 pm</span></time>
                                       </div>
                                       <div class="c-box__link-wrap">
                                          <div class="btn-pointer__wrap"  style="margin-right:3px">
                                             <a class="btn btn-pointer" href="https://www.zomato.com/bangalore/high-ultra-lounge-malleshwaram" target="_blank"><span class="btn-pointer__txt" style=" color:#e15e32; font-family:'Univers LT W01_57 Condensed' !important";>BOOK NOW</span></a>
                                             <svg class="btn-pointer__left" x="0px" y="0px"
                                                viewBox="0 0 10 50" style="enable-background:new 0 0 10 50;" xml:space="preserve">
                                                <path fill="#A82743" d="M10,50H0l5-5l-5-5l0,0v0l0,0l5-5l-5-5l0,0v0l0,0l5-5l-5-5l0,0v0l0,0l5-5l-5-5l0,0v0l0,0l5-5L0,0l0,0v0h10V50z"/>
                                             </svg>
                                          </div>
                                       </div>
                                       <a href="<%--event-open.html--%>" rel="nofollow" class="b-box__link" style="/*background-color: #a54399;opacity: 0.5;*/"></a>
                                    </div>
                                    <%--</div>--%>
                                 </div>
                              </div>
                              <div class="col-md-1 col-lg-1 col-sm-1 col-xm-1 col-xs-1 "></div>
                              <div class="col-md-6 col-lg-5 col-sm-12 col-xm-12 col-xs-12 ">
                                 <div class="b-tabs__pane-item col-sm-6">
                                    <%--  <div class="c-box">--%>
                                    <div class="c-box__inner" style="height:342px; width:549.14px; margin-bottom:20px; background-image:url(images/DJ/mkshft.jpg); background-size:cover!important">
                                       <div class="c-box__cont">
                                          <h3 class="c-box__ttl-s3" style=" font-family:'Univers LT W01_57 Condensed' !important">DJ Mkshft </h3>
                                       <%--   <hr class="hr-pattern">--%>
                                          <%-- <p class="c-box__txt" style=" font-family:'Univers LT W01_57 Condensed' !important">Commercial & EDM</p>--%>
                                          <p class="c-box__txt" style=" font-family:'Univers LT W01_57 Condensed' !important";>
                                             <!--Collapsive-->                       
                                          <div id="container">
                                             <div id="content">
                                                <div class="galleryContainer">
                                                   <div class="galleryItem">
                                                      About Event
                                                     <%-- <p>This Saturday night, put on your dance shoes and groove to the scintillating beats of commercial music.</p>--%>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                          </p>     
                                       </div>
                                       <div data-bgimage="../uploads/events/EventsTiles_04_Retina.jpg" class="b-box__img-wrap b-box__grdnt-a bg-image"></div>
                                       <div class="c-box__date-item" style="margin:3px">
                                          <time datetime="2009-08-29T20:00" class="date c-box__date"><span class="date__dt">06</span><span class="date__rh"><span class="date__rh-m">Oct</span><span class="date__rh-d">Sat</span></span><span class="date__ad">9:00 pm</span></time>
                                       </div>
                                       <div class="c-box__link-wrap">
                                          <div class="btn-pointer__wrap"  style="margin-right:3px">
                                             <a class="btn btn-pointer" href="https://www.zomato.com/bangalore/high-ultra-lounge-malleshwaram" target="_blank"><span class="btn-pointer__txt" style=" color:#e15e32; font-family:'Univers LT W01_57 Condensed' !important";>BOOK NOW</span></a>
                                             <svg class="btn-pointer__left" x="0px" y="0px"
                                                viewBox="0 0 10 50" style="enable-background:new 0 0 10 50;" xml:space="preserve">
                                                <path fill="#A82743" d="M10,50H0l5-5l-5-5l0,0v0l0,0l5-5l-5-5l0,0v0l0,0l5-5l-5-5l0,0v0l0,0l5-5l-5-5l0,0v0l0,0l5-5L0,0l0,0v0h10V50z"/>
                                             </svg>
                                          </div>
                                       </div>
                                       <a href="<%--event-open.html--%>" rel="nofollow" class="b-box__link" style="/*background-color: #a54399;opacity: 0.5;*/"></a>
                                    </div>
                                    <%--</div>--%>
                                 </div>
                              </div>


                           </div>
                           <div class="row">
                              <div class="col-md-6 col-lg-5 col-sm-12 col-xm-12 col-xs-12 ">
                                 <div class="b-tabs__pane-item col-md-12 col-lg-12 col-sm-12 col-xm-12 col-xs-12 ">
                                    <%--  <div class="c-box">--%>
                                    <div class="c-box__inner" style="height:342px; width:549.1px; margin-bottom:20px; background-image:url(images/DJ/vip.jpg); background-size:cover!important">
                                       <div class="c-box__cont">
                                          <h3 class="c-box__ttl-s3" style=" font-family:'Univers LT W01_57 Condensed' !important">DJ Vipul Khurana</h3>
                                       <%--   <hr class="hr-pattern">--%>
                                          <%--<p class="c-box__txt" style=" font-family:'Univers LT W01_57 Condensed' !important">Mix Bag</p>--%>
                                          <p class="c-box__txt" style=" font-family:'Univers LT W01_57 Condensed' !important";>
                                             <!--Collapsive-->                       
                                          <div id="container">
                                             <div id="content">
                                                <div class="galleryContainer">
                                                   <div class="galleryItem">
                                                      About Event
                                                    <%--  <p>Chivas Open House presents the electrifying band A26 to perform for you at the Sunday Sundowner. The band is a collaboration of 8 skillful artists, who have performed on the world stage.</p>--%>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                          </p>     
                                       </div>
                                       <div data-bgimage="../uploads/events/EventsTiles_04_Retina.jpg" class="b-box__img-wrap b-box__grdnt-a bg-image"></div>
                                       <div class="c-box__date-item" style="margin:3px">
                                          <time datetime="2009-08-29T20:00" class="date c-box__date"><span class="date__dt">12</span><span class="date__rh"><span class="date__rh-m">Oct</span><span class="date__rh-d">Fri</span></span><span class="date__ad">5:00 pm</span></time>
                                       </div>
                                       <div class="c-box__link-wrap">
                                          <div class="row">
                                             <div class="btn-pointer__wrap <%--<%--col-lg-12 col-sm-12 col-md-5 col-xm-12 col-xs-12--%>--%>"  style="margin-right:17px">
                                                <a class="btn btn-pointer" href="https://www.zomato.com/bangalore/high-ultra-lounge-malleshwaram" target="_blank"><span class="btn-pointer__txt" style=" color:#e15e32; font-family:'Univers LT W01_57 Condensed' !important";>BOOK NOW</span></a>
                                                <svg class="btn-pointer__left" x="0px" y="0px"
                                                   viewBox="0 0 10 50" style="enable-background:new 0 0 10 50;" xml:space="preserve">
                                                   <path fill="#A82743" d="M10,50H0l5-5l-5-5l0,0v0l0,0l5-5l-5-5l0,0v0l0,0l5-5l-5-5l0,0v0l0,0l5-5l-5-5l0,0v0l0,0l5-5L0,0l0,0v0h10V50z"/>
                                                </svg>
                                             </div>
                                          </div>
                                       </div>
                                       <a href="<%--event-open.html--%>" rel="nofollow" class="b-box__link" style="/*background-color: #a54399;opacity: 0.5;*/"></a>
                                    </div>
                                 </div>
                              </div>
                              <div class="col-md-1 col-lg-1 col-sm-1 col-xm-1 col-xs-1 "></div>
                              <div class="col-md-5 col-lg-5 col-sm-10 col-xm-12 col-xs-12 ">
                                 <div class="b-tabs__pane-item  col-sm-12">
                                    <%-- <div class="c-box">--%>
                                    <div class="c-box__inner" style="height:342px; width:549.14px; margin-bottom:20px; background-image:url(images/DJ/hus.jpg); background-size:cover!important">
                                       <div class="c-box__cont">
                                          <h3 class="c-box__ttl-s3" style=" font-family:'Univers LT W01_57 Condensed' !important";> DJ Hussain </h3>
                                          <%--<hr class="hr-pattern">--%>
                                          <p class="c-box__txt" style=" font-family:'Univers LT W01_57 Condensed' !important";>
                                             <!--Collapsive-->                       
                                          <div id="container">
                                             <div id="content">
                                                <div class="galleryContainer">
                                                   <div class="galleryItem">
                                                      About Event
                                                      <%--<p>DJ Adaa is all set for a scintillating performance, specializing in playing a mix of genre to keep you grooving.</p>--%>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                          </p>     
                                       </div>
                                       <div data-bgimage="../uploads/events/EventsTiles_01_Retina.jpg" class="b-box__img-wrap b-box__grdnt-a bg-image"></div>
                                       <div class="c-box__date-item" style="margin:3px">
                                          <time datetime="2018-06-22T21:00" class="date c-box__date"><span class="date__dt" style="margin:3px">13</span><span class="date__rh"><span class="date__rh-m">Oct</span><span class="date__rh-d">Sat</span></span><span class="date__ad">9:00 pm</span></time>
                                       </div>
                                       <div class="c-box__link-wrap">
                                          <div class="btn-pointer__wrap" style="margin-right:3px">
                                             <a class="btn btn-pointer" href="https://www.zomato.com/bangalore/high-ultra-lounge-malleshwaram"target="_blank"><span class="btn-pointer__txt" style=" color:#e15e32; font-family:'Univers LT W01_57 Condensed' !important";>BOOK NOW</span></a>
                                             <svg class="btn-pointer__left" x="0px" y="0px"
                                                viewBox="0 0 10 50" style="enable-background:new 0 0 10 50;" xml:space="preserve">
                                                <path fill="#A82743" d="M10,50H0l5-5l-5-5l0,0v0l0,0l5-5l-5-5l0,0v0l0,0l5-5l-5-5l0,0v0l0,0l5-5l-5-5l0,0v0l0,0l5-5L0,0l0,0v0h10V50z"/>
                                             </svg>
                                          </div>
                                       </div>
                                       <a href="#" rel="nofollow" class="b-box__link" style="/*background-color: #a54399;opacity: 0.5;*/"></a>
                                    </div>
                                    <%--      </div>--%>
                                 </div>
                              </div>


                           </div>
                           <%--<div class="b-box__btn-wrap txt--centr col-sm-12">
                              <div class="btn-icon__wrap">
                              <a href="#" class="btn btn-icon">More events</a>
                              </div>
                              </div>--%>
                           <div class="row">
                             
                              <div class="col-md-5 col-lg-5 col-sm-12 col-xm-10 col-xs-12">
                                 <div class="b-tabs__pane-item   col-sm-12">
                                    <%-- <div class="c-box">--%>
                                    <div class="c-box__inner" style="height:342px; width:549.14px; margin-bottom:20px; background-image:url(images/DJ/james.jpg); background-size:cover!important">
                                       <div class="c-box__cont">
                                          <h3 class="c-box__ttl-s3" style=" font-family:'Univers LT W01_57 Condensed' !important">DJ James Mittal </h3>
                                        <%--  <hr class="hr-pattern">--%>
                                          <p class="c-box__txt" style=" font-family:'Univers LT W01_57 Condensed' !important";></p>
                                           <div id="container">
                                             <div id="content">
                                                <div class="galleryContainer">
                                                   <div class="galleryItem">
                                                      About Event
                                                     <%-- <p>The DJ is all set to play foot-tapping Retro Bollywood tracks, non-stop Bhangra and exhilarating Bollywood remix beats to lift up your mood.</p>--%>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                       <div data-bgimage="../uploads/events/EventsTiles_02_Retina.jpg" class="b-box__img-wrap b-box__grdnt-a bg-image"></div>
                                       <div class="c-box__date-item" style="margin:3px" >
                                          <time datetime="2009-08-29T20:00" class="date c-box__date"><span class="date__dt">19</span><span class="date__rh"><span class="date__rh-m">Oct</span><span class="date__rh-d" >Fri</span></span><span class="date__ad">09:00 pm</span></time>
                                       </div>
                                       <div class="c-box__link-wrap">
                                          <div class="btn-pointer__wrap"  style="margin-right:3px">
                                             <a class="btn btn-pointer" href="https://www.zomato.com/bangalore/high-ultra-lounge-malleshwaram"  target="_blank"><span class="btn-pointer__txt" style=" color:#e15e32; font-family:'Univers LT W01_57 Condensed' !important";>BOOK NOW</span></a>
                                             <svg class="btn-pointer__left" x="0px" y="0px"
                                                viewBox="0 0 10 50" style="enable-background:new 0 0 10 50;" xml:space="preserve">
                                                <path fill="#A82743" d="M10,50H0l5-5l-5-5l0,0v0l0,0l5-5l-5-5l0,0v0l0,0l5-5l-5-5l0,0v0l0,0l5-5l-5-5l0,0v0l0,0l5-5L0,0l0,0v0h10V50z"/>
                                             </svg>
                                          </div>
                                       </div>
                                       <a href="<%--event-open.html--%>" rel="nofollow" class="b-box__link" style="background-color: #202020;opacity: 0.5;"></a>
                                    </div>
                                    <%--   </div>--%>
                                 </div>
                              </div>
                              <div class="col-md-1 col-lg-1 col-sm-1 col-xm-1 col-xs-1 "></div>
                                <div class="col-md-5 col-lg-5 col-sm-12 col-xm-10 col-xs-12">
                                 <div class="b-tabs__pane-item   col-sm-12">
                                    <%-- <div class="c-box">--%>
                                    <div class="c-box__inner" style="height:342px; width:549.14px; margin-bottom:20px; background-image:url(images/DJ/arrivals1.jpg); background-size:cover!important">
                                       <div class="c-box__cont">
                                          <h3 class="c-box__ttl-s3" style=" font-family:'Univers LT W01_57 Condensed' !important">Arrivals Dj Vishnu,<br /> Dj Blaque and Harish </h3>
                                        <%--  <hr class="hr-pattern">--%>
                                          <p class="c-box__txt" style=" font-family:'Univers LT W01_57 Condensed' !important";></p>
                                           <div id="container">
                                             <div id="content">
                                                <div class="galleryContainer">
                                                   <div class="galleryItem">
                                                      About Event
                                                     <%-- <p>An astounding sundowner awaits you with Bangalore's outstanding Electro Pop band, 'Best Kept Secret'. They will mesmerize you with their chartbusters, ranging from electro to pop.</p>--%>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                       <div data-bgimage="../uploads/events/EventsTiles_02_Retina.jpg" class="b-box__img-wrap b-box__grdnt-a bg-image"></div>
                                       <div class="c-box__date-item" style="margin:3px" >
                                          <time datetime="2009-08-29T20:00" class="date c-box__date"><span class="date__dt">20</span><span class="date__rh"><span class="date__rh-m">Oct</span><span class="date__rh-d" >Sat</span></span><span class="date__ad">09:00 pm</span></time>
                                       </div>
                                       <div class="c-box__link-wrap">
                                          <div class="btn-pointer__wrap"  style="margin-right:3px">
                                             <a class="btn btn-pointer" href="https://www.zomato.com/bangalore/high-ultra-lounge-malleshwaram"  target="_blank"><span class="btn-pointer__txt" style=" color:#e15e32; font-family:'Univers LT W01_57 Condensed' !important";>BOOK NOW</span></a>
                                             <svg class="btn-pointer__left" x="0px" y="0px"
                                                viewBox="0 0 10 50" style="enable-background:new 0 0 10 50;" xml:space="preserve">
                                                <path fill="#A82743" d="M10,50H0l5-5l-5-5l0,0v0l0,0l5-5l-5-5l0,0v0l0,0l5-5l-5-5l0,0v0l0,0l5-5l-5-5l0,0v0l0,0l5-5L0,0l0,0v0h10V50z"/>
                                             </svg>
                                          </div>
                                       </div>
                                       <a href="<%--event-open.html--%>" rel="nofollow" class="b-box__link" style="background-color: #202020;opacity: 0.5;"></a>
                                    </div>
                                    <%--   </div>--%>
                                 </div>
                              </div>
                           </div>


                               <div class="row">
                             
                              <div class="col-md-5 col-lg-5 col-sm-12 col-xm-10 col-xs-12">
                                 <div class="b-tabs__pane-item   col-sm-12">
                                    <%-- <div class="c-box">--%>
                                    <div class="c-box__inner" style="height:342px; width:549.14px; margin-bottom:20px; background-image:url(images/DJ/nash1.jpg); background-size:cover!important">
                                       <div class="c-box__cont">
                                          <h3 class="c-box__ttl-s3" style=" font-family:'Univers LT W01_57 Condensed' !important">DJ Nash </h3>
                                        <%--  <hr class="hr-pattern">--%>
                                          <p class="c-box__txt" style=" font-family:'Univers LT W01_57 Condensed' !important";></p>
                                           <div id="container">
                                             <div id="content">
                                                <div class="galleryContainer">
                                                   <div class="galleryItem">
                                                      About Event
                                                    <%--  <p>The DJ is all set to play foot-tapping Retro Bollywood tracks, non-stop Bhangra and exhilarating Bollywood remix beats to lift up your mood.</p>--%>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                       <div data-bgimage="../uploads/events/EventsTiles_02_Retina.jpg" class="b-box__img-wrap b-box__grdnt-a bg-image"></div>
                                       <div class="c-box__date-item" style="margin:3px" >
                                          <time datetime="2009-08-29T20:00" class="date c-box__date"><span class="date__dt">26</span><span class="date__rh"><span class="date__rh-m">Oct</span><span class="date__rh-d" >Fri</span></span><span class="date__ad">09:00 pm</span></time>
                                       </div>
                                       <div class="c-box__link-wrap">
                                          <div class="btn-pointer__wrap"  style="margin-right:3px">
                                             <a class="btn btn-pointer" href="https://www.zomato.com/bangalore/high-ultra-lounge-malleshwaram"  target="_blank"><span class="btn-pointer__txt" style=" color:#e15e32; font-family:'Univers LT W01_57 Condensed' !important";>BOOK NOW</span></a>
                                             <svg class="btn-pointer__left" x="0px" y="0px"
                                                viewBox="0 0 10 50" style="enable-background:new 0 0 10 50;" xml:space="preserve">
                                                <path fill="#A82743" d="M10,50H0l5-5l-5-5l0,0v0l0,0l5-5l-5-5l0,0v0l0,0l5-5l-5-5l0,0v0l0,0l5-5l-5-5l0,0v0l0,0l5-5L0,0l0,0v0h10V50z"/>
                                             </svg>
                                          </div>
                                       </div>
                                       <a href="<%--event-open.html--%>" rel="nofollow" class="b-box__link" style="background-color: #202020;opacity: 0.5;"></a>
                                    </div>
                                    <%--   </div>--%>
                                 </div>
                              </div>
                              <div class="col-md-1 col-lg-1 col-sm-1 col-xm-1 col-xs-1 "></div>
                                <div class="col-md-5 col-lg-5 col-sm-12 col-xm-10 col-xs-12">
                                 <div class="b-tabs__pane-item   col-sm-12">
                                    <%-- <div class="c-box">--%>
                                    <div class="c-box__inner" style="height:342px; width:549.14px; margin-bottom:20px; background-image:url(images/DJ/ivan.jpg); background-size:cover!important">
                                       <div class="c-box__cont">
                                          <h3 class="c-box__ttl-s3" style=" font-family:'Univers LT W01_57 Condensed' !important">Ivan </h3>
                                        <%--  <hr class="hr-pattern">--%>
                                          <p class="c-box__txt" style=" font-family:'Univers LT W01_57 Condensed' !important";></p>
                                           <div id="container">
                                             <div id="content">
                                                <div class="galleryContainer">
                                                   <div class="galleryItem">
                                                      About Event
                                                   <%--   <p>An astounding sundowner awaits you with Bangalore's outstanding Electro Pop band, 'Best Kept Secret'. They will mesmerize you with their chartbusters, ranging from electro to pop.</p>--%>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                       <div data-bgimage="../uploads/events/EventsTiles_02_Retina.jpg" class="b-box__img-wrap b-box__grdnt-a bg-image"></div>
                                       <div class="c-box__date-item" style="margin:3px" >
                                          <time datetime="2009-08-29T20:00" class="date c-box__date"><span class="date__dt">27</span><span class="date__rh"><span class="date__rh-m">Oct</span><span class="date__rh-d" >Sat</span></span><span class="date__ad">09:00 pm</span></time>
                                       </div>
                                       <div class="c-box__link-wrap">
                                          <div class="btn-pointer__wrap"  style="margin-right:3px">
                                             <a class="btn btn-pointer" href="https://www.zomato.com/bangalore/high-ultra-lounge-malleshwaram"  target="_blank"><span class="btn-pointer__txt" style=" color:#e15e32; font-family:'Univers LT W01_57 Condensed' !important";>BOOK NOW</span></a>
                                             <svg class="btn-pointer__left" x="0px" y="0px"
                                                viewBox="0 0 10 50" style="enable-background:new 0 0 10 50;" xml:space="preserve">
                                                <path fill="#A82743" d="M10,50H0l5-5l-5-5l0,0v0l0,0l5-5l-5-5l0,0v0l0,0l5-5l-5-5l0,0v0l0,0l5-5l-5-5l0,0v0l0,0l5-5L0,0l0,0v0h10V50z"/>
                                             </svg>
                                          </div>
                                       </div>
                                       <a href="<%--event-open.html--%>" rel="nofollow" class="b-box__link" style="background-color: #202020;opacity: 0.5;"></a>
                                    </div>
                                    <%--   </div>--%>
                                 </div>
                              </div>
                           </div>
                               <div class="row">
                             
                              <div class="col-md-5 col-lg-5 col-sm-12 col-xm-10 col-xs-12">
                                 <div class="b-tabs__pane-item   col-sm-12">
                                    <%-- <div class="c-box">--%>
                                    <div class="c-box__inner" style="height:342px; width:549.14px; margin-bottom:20px; background-image:url(images/DJ/Band.jpg); background-size:cover!important">
                                       <div class="c-box__cont">
                                          <h3 class="c-box__ttl-s3" style=" font-family:'Univers LT W01_57 Condensed' !important">Best Kept Secret </h3>
                                        <%--  <hr class="hr-pattern">--%>
                                          <p class="c-box__txt" style=" font-family:'Univers LT W01_57 Condensed' !important";></p>
                                           <div id="container">
                                             <div id="content">
                                                <div class="galleryContainer">
                                                   <div class="galleryItem">
                                                      About Event
                                                    <%--  <p>An astounding sundowner awaits you with Bangalore's outstanding Electro Pop band, 'Best Kept Secret'. They will mesmerize you with their chartbusters, ranging from electro to pop.</p>--%>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                       <div data-bgimage="../uploads/events/EventsTiles_02_Retina.jpg" class="b-box__img-wrap b-box__grdnt-a bg-image"></div>
                                       <div class="c-box__date-item" style="margin:3px" >
                                          <time datetime="2009-08-29T20:00" class="date c-box__date"><span class="date__dt">29</span><span class="date__rh"><span class="date__rh-m">Sept</span><span class="date__rh-d" >Sat</span></span><span class="date__ad">09:00 pm</span></time>
                                       </div>
                                       <div class="c-box__link-wrap">
                                          <div class="btn-pointer__wrap"  style="margin-right:3px">
                                             <a class="btn btn-pointer" href="https://www.zomato.com/bangalore/high-ultra-lounge-malleshwaram"  target="_blank"><span class="btn-pointer__txt" style=" color:#e15e32; font-family:'Univers LT W01_57 Condensed' !important";>BOOK NOW</span></a>
                                             <svg class="btn-pointer__left" x="0px" y="0px"
                                                viewBox="0 0 10 50" style="enable-background:new 0 0 10 50;" xml:space="preserve">
                                                <path fill="#A82743" d="M10,50H0l5-5l-5-5l0,0v0l0,0l5-5l-5-5l0,0v0l0,0l5-5l-5-5l0,0v0l0,0l5-5l-5-5l0,0v0l0,0l5-5L0,0l0,0v0h10V50z"/>
                                             </svg>
                                          </div>
                                       </div>
                                       <a href="<%--event-open.html--%>" rel="nofollow" class="b-box__link" style="background-color: #202020;opacity: 0.5;"></a>
                                    </div>
                                    <%--   </div>--%>
                                 </div>
                              </div>
                              <div class="col-md-1 col-lg-1 col-sm-1 col-xm-1 col-xs-1 "></div>
                                <div class="col-md-5 col-lg-5 col-sm-12 col-xm-10 col-xs-12">
                                 <div class="b-tabs__pane-item   col-sm-12">
                                    <%-- <div class="c-box">--%>
                                    <div class="c-box__inner" style="height:342px; width:549.14px; margin-bottom:20px; background-image:url(/*images/DJ/Band.jpg*/); background-size:cover!important">
                                       <div class="c-box__cont">
                                          <h3 class="c-box__ttl-s3" style=" font-family:'Univers LT W01_57 Condensed' !important">Coming Soon </h3>
                                        <%--  <hr class="hr-pattern">--%>
                                          <p class="c-box__txt" style=" font-family:'Univers LT W01_57 Condensed' !important";></p>
                                           <div id="container">
                                             <div id="content">
                                                <div class="galleryContainer">
                                                   <div class="galleryItem">
                                                      About Event
                                                     <%-- <p>An astounding sundowner awaits you with Bangalore's outstanding Electro Pop band, 'Best Kept Secret'. They will mesmerize you with their chartbusters, ranging from electro to pop.</p>--%>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                       <div data-bgimage="../uploads/events/EventsTiles_02_Retina.jpg" class="b-box__img-wrap b-box__grdnt-a bg-image"></div>
                                       <div class="c-box__date-item" style="margin:3px" >
                                          <time datetime="2009-08-29T20:00" class="date c-box__date"><span class="date__dt">00</span><span class="date__rh"><span class="date__rh-m">Nov</span><span class="date__rh-d" >Day</span></span><span class="date__ad">00:00 pm</span></time>
                                       </div>
                                       <div class="c-box__link-wrap">
                                          <div class="btn-pointer__wrap"  style="margin-right:3px">
                                             <a class="btn btn-pointer" href="https://www.zomato.com/bangalore/high-ultra-lounge-malleshwaram"  target="_blank"><span class="btn-pointer__txt" style=" color:#e15e32; font-family:'Univers LT W01_57 Condensed' !important";>BOOK NOW</span></a>
                                             <svg class="btn-pointer__left" x="0px" y="0px"
                                                viewBox="0 0 10 50" style="enable-background:new 0 0 10 50;" xml:space="preserve">
                                                <path fill="#A82743" d="M10,50H0l5-5l-5-5l0,0v0l0,0l5-5l-5-5l0,0v0l0,0l5-5l-5-5l0,0v0l0,0l5-5l-5-5l0,0v0l0,0l5-5L0,0l0,0v0h10V50z"/>
                                             </svg>
                                          </div>
                                       </div>
                                       <a href="<%--event-open.html--%>" rel="nofollow" class="b-box__link" style="background-color: #202020;opacity: 0.5;"></a>
                                    </div>
                                    <%--   </div>--%>
                                 </div>
                              </div>
                           </div>



                            
                        </div>
                        <div   id="bottom_link" style="width: 100%" ><iframe width="100%" height="800" src="https://maps.google.com/maps?width=100%&amp;height=600&amp;hl=en&amp;q=No.%2026%2F1%2C%20Rooftop%2C%20World%20Trade%20Center%2C%20Bangalore%20Brigade%20Gateway%20Campus%2C%20Dr.%20Rajkumar%20Road%2C%20Malleswaram%2C%20Bengaluru%2C%20Karnataka%20560055+(high%20ultra%20lounge)&amp;ie=UTF8&amp;t=&amp;z=14&amp;iwloc=B&amp;output=embed" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"><a href="https://www.maps.ie/create-google-map/">Add map to website</a></iframe></div>
                        <br />
                     </div>
                  </div>
               </div>
            </div>
         </div>
        =
         
         <!--end-->
      </div>
   </div>
   <script type="text/javascript">
      var myIndex = 0;
      carousel();
      
      function carousel() {
          var i;
          var x = document.getElementsByClassName("mySlides");
          for (i = 0; i < x.length; i++) {
              x[i].style.display = "none";
          }
          myIndex++;
          if (myIndex > x.length) { myIndex = 1 }
          x[myIndex - 1].style.display = "block";
          setTimeout(carousel, 500); // Change image every 2 seconds
      }
      
   </script>
</asp:Content>