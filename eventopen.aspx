﻿<%@ Page Title="" Language="C#" MasterPageFile="MasterPage.master" AutoEventWireup="true" CodeFile="eventopen.aspx.cs" Inherits="eventopen" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
 
   <div class="c-headlines">
        <div data-stellar-background-ratio="1.2" data-bgimage="images/super-high11.jpg" class="c-headlines__img-wrap parallax bg-image"></div>
        <div class="container">
          <div class="b-breadcrumbs b-breadcrumbs--wht">
            <ul class="b-breadcrumbs__list x-small-txt">
              <li><a href="index.html" class="link">main</a></li>
              <li><a href="events.html" class="link">events</a></li>
              <li>Private house party</li>
            </ul>
          </div>
<%--          <div class="c-headlines__ttl">FRIENDS Private House Party Private Private House Party House Party</div>--%>
            
                <div class="col-12>"
       
            
          <div class="c-headlines__date-lg" style="right:900px;padding-top:50px; ">
            <time datetime="2015-09-23T08:00" class="date-lg" style="width:280px;"><span class="date-lg__dt" style="font-size:40px;">4-29</span><span class="date-lg__rh"><span class="date-lg__rh-m" >May 2018 -</span><span class="date-lg__rh-m">June 2018</span><span class="date-lg__rh-t"></span></span><span class="date-lg__ad" ><a href="#Friday">Fri</a></span></time>
          </div>>
            <li><div class="c-headlines__date-lg" style="right:600px;padding-top:50px; ">
            <time datetime="2015-09-23T08:00" class="date-lg" style="width:280px;"><span class="date-lg__dt"style="font-size:40px;">5-30</span><span class="date-lg__rh"><span class="date-lg__rh-m" >May 2018 -</span><span class="date-lg__rh-m">June 2018</span><span class="date-lg__rh-t"></span></span><span class="date-lg__ad" ><a href="#Saturday">Sat</a></span></time>
          </div>
                 <li><div class="c-headlines__date-lg" style="right:300px;padding-top:50px; ">
            <time datetime="2015-09-23T08:00" class="date-lg" style="width:280px;"><span class="date-lg__dt"style="font-size:40px;">6-24</span><span class="date-lg__rh"><span class="date-lg__rh-m" >May 2018 -</span><span class="date-lg__rh-m">June 2018</span><span class="date-lg__rh-t"></span></span><span class="date-lg__ad" ><a href="#Saturday">Sun</a></span></time>
          </div>
                 <li><div class="c-headlines__date-lg" style="right:0px;padding-top:50px; ">
            <time datetime="2015-09-23T08:00" class="date-lg" style="width:280px;"><span class="date-lg__dt"style="font-size:40px;">-</span><span class="date-lg__rh"><span class="date-lg__rh-m" >May 2018 -</span><span class="date-lg__rh-m">June 2018</span><span class="date-lg__rh-t"></span></span><span class="date-lg__ad" ><a href="#Saturday">Others</a></span></time>
          </div>
                
                    </div>
               

        </div>
      </div>
      <div class="b-content container">
        <div class="b-panel-t col-xs-12">
          <ul class="b-panel-t__list b-panel-t__arts col-sm-8 col-md-9">
            <li>
              <p>Andrew Feeling</p><span>Donetsk</span>
            </li>
            <li>
              <p>Shalim</p><span>Donetsk</span>
            </li>
            <li>
              <p>Stay B</p><span>Donetsk</span>
            </li>
            <li>
              <p>Someone Else</p><span>Mariupol</span>
            </li>
            <li>
              <p>Dranga</p><span>Mariupol</span>
            </li>
            <li>
              <p>Andrew Feeling</p><span>Donetsk</span>
            </li>
            <li>
              <p>Shalim</p><span>Donetsk</span>
            </li>
            <li>
              <p>Stay B</p><span>Donetsk</span>
            </li>
          </ul>
          <ul class="b-panel-t__list col-sm-4 col-md-3">
            <li><a href="mailto:tickets@cumeclub.com" class="link">tickets@cumeclub.com</a></li>
            <li class="tel"><a href="tel:+380969776011" class="link">+38 (096) 977-60-11</a></li>
          </ul>
        </div>

</div>
    <!--Friday Events start-->
    <div class="b-gallery">
        <div data-bgimage="" class="b-gallery__img-wrap bg-image"></div>
        <div class="container">
          <div class="row">
            <div class="b-item__header col-xs-12">
              <h6 class="s6-heading b-item__ttl-header" id="Friday">Friday</h6>
            </div>
            <div id="sliderGallery" class="b-gallery__slider btn-white">
              <article class="c-box c-box--sm">
                <div class="c-box__inner">
                  <div data-bgimage="images/fri1.jpg" class="b-box__img-wrap b-box__grdnt-b bg-image"></div>
                  <div class="c-box__date-item c-box__date-item--float">
                    <time datetime="2009-08-29T20:00" class="date-sm c-box__date"><span class="date-sm__dt">23</span><span class="date-sm__rh"><span class="date-sm__rh-m">sept</span><span class="date-sm__rh-d">2014</span></span></time>
                  </div>
                  <ul class="c-box__views">
                   
                  <li>  <button class="b-form__btn indent btn-bg--act-b"STYLE="min-height:30PX;">BOOK</button></LI>
                      
                  </ul><a href="eventlists.aspx" rel="nofollow" class="b-box__link"></a>
                </div>
                      <div id="headingIconClrOne" role="tab" class="b-accordion__panel-heading">
                        <h4 class="b-accordion__panel-ttl"style="color:white;"><a role="button" data-toggle="collapse" data-parent="#accordionIconClr" href="#blockIconClrOne" aria-expanded="true" aria-controls="blockIconClrOne" style="color:white;"><b class="fa fa-music"style="color:white;"></b> <B>DJ NIGHT </B><b class="fa fa-angle-down"></b></a></h4>
                      </div>
                      <div id="blockIconClrOne" role="tabpanel" aria-labelledby="headingIconClrOne" class="b-accordion__collapse collapse in">
                        <div class="b-accordion__body">
                          <p>
                            DJ:Rohit Barker</p>

<p>Period: 4th May, 2018 </p>

<p>Time: 9:00 PM Onwards</p>
                           
                          </p>
                        </div>
                      </div>
              </article>
              <article class="c-box c-box--sm">
                <div class="c-box__inner">
                  <div data-bgimage="images/fri2.jpg" class="b-box__img-wrap b-box__grdnt-b bg-image"></div>
                  <div class="c-box__date-item c-box__date-item--float">
                    <time datetime="2009-08-29T20:00" class="date-sm c-box__date"><span class="date-sm__dt">23</span><span class="date-sm__rh"><span class="date-sm__rh-m">sept</span><span class="date-sm__rh-d">2014</span></span></time>
                  </div>
                  <ul class="c-box__views">
                    <li class="c-box__view txt">32</li>
                    <li class="c-box__video txt">2</li>
                  </ul><a href="gallery-open.html" rel="nofollow" class="b-box__link"></a>
                </div>
                <h6><a href="gallery-open.html" class="c-box__link">Californication nights</a></h6>
              </article>
              <article class="c-box c-box--sm">
                <div class="c-box__inner">
                  <div data-bgimage="images/fri3.jpg" class="b-box__img-wrap b-box__grdnt-b bg-image"></div>
                  <div class="c-box__date-item c-box__date-item--float">
                    <time datetime="2009-08-29T20:00" class="date-sm c-box__date"><span class="date-sm__dt">23</span><span class="date-sm__rh"><span class="date-sm__rh-m">sept</span><span class="date-sm__rh-d">2014</span></span></time>
                  </div>
                  <ul class="c-box__views">
                    <li class="c-box__view txt">32</li>
                    <li class="c-box__video txt">2</li>
                  </ul><a href="gallery-open.html" rel="nofollow" class="b-box__link"></a>
                </div>
                <h6><a href="gallery-open.html" class="c-box__link">Mash up your heads: christmas night mash up your heads: christmas night</a></h6>
              </article>
              <article class="c-box c-box--sm">
                <div class="c-box__inner">
                  <div data-bgimage="images/fri6.jpg" class="b-box__img-wrap b-box__grdnt-b bg-image"></div>
                  <div class="c-box__date-item c-box__date-item--float">
                    <time datetime="2009-08-29T20:00" class="date-sm c-box__date"><span class="date-sm__dt">23</span><span class="date-sm__rh"><span class="date-sm__rh-m">sept</span><span class="date-sm__rh-d">2014</span></span></time>
                  </div>
                  <ul class="c-box__views">
                    <li class="c-box__view txt">32</li>
                    <li class="c-box__video txt">2</li>
                  </ul><a href="gallery-open.html" rel="nofollow" class="b-box__link"></a>
                </div>
                <h6><a href="gallery-open.html" class="c-box__link">Californication nights</a></h6>
              </article>
              <article class="c-box c-box--sm">
                <div class="c-box__inner">
                  <div data-bgimage="../uploads/gallery/Gallery_07_Original.jpg" class="b-box__img-wrap b-box__grdnt-b bg-image"></div>
                  <div class="c-box__date-item c-box__date-item--float">
                    <time datetime="2009-08-29T20:00" class="date-sm c-box__date"><span class="date-sm__dt">23</span><span class="date-sm__rh"><span class="date-sm__rh-m">sept</span><span class="date-sm__rh-d">2014</span></span></time>
                  </div>
                  <ul class="c-box__views">
                    <li class="c-box__view txt">32</li>
                    <li class="c-box__video txt">2</li>
                  </ul><a href="gallery-open.html" rel="nofollow" class="b-box__link"></a>
                </div>
                <h6><a href="gallery-open.html" class="c-box__link">Mash up your heads: christmas night mash up your heads: christmas night</a></h6>
              </article>
              <article class="c-box c-box--sm">
                <div class="c-box__inner">
                  <div data-bgimage="../uploads/gallery/Gallery_05_Original.jpg" class="b-box__img-wrap b-box__grdnt-b bg-image"></div>
                  <div class="c-box__date-item c-box__date-item--float">
                    <time datetime="2009-08-29T20:00" class="date-sm c-box__date"><span class="date-sm__dt">23</span><span class="date-sm__rh"><span class="date-sm__rh-m">sept</span><span class="date-sm__rh-d">2014</span></span></time>
                  </div>
                  <ul class="c-box__views">
                    <li class="c-box__view txt">32</li>
                    <li class="c-box__video txt">2</li>
                  </ul><a href="gallery-open.html" rel="nofollow" class="b-box__link"></a>
                </div>
                <h6><a href="gallery-open.html" class="c-box__link">Mash up your heads: christmas night mash up your heads: christmas night</a></h6>
              </article>
              <article class="c-box c-box--sm">
                <div class="c-box__inner">
                  <div data-bgimage="../uploads/gallery/Gallery_06_Original.jpg" class="b-box__img-wrap b-box__grdnt-b bg-image"></div>
                  <div class="c-box__date-item c-box__date-item--float">
                    <time datetime="2009-08-29T20:00" class="date-sm c-box__date"><span class="date-sm__dt">23</span><span class="date-sm__rh"><span class="date-sm__rh-m">sept</span><span class="date-sm__rh-d">2014</span></span></time>
                  </div>
                  <ul class="c-box__views">
                    <li class="c-box__view txt">32</li>
                    <li class="c-box__video txt">2</li>
                  </ul><a href="gallery-open.html" rel="nofollow" class="b-box__link"></a>
                </div>
                <h6><a href="gallery-open.html" class="c-box__link">Mash up your heads: christmas night mash up your heads: christmas night</a></h6>
              </article>
              <article class="c-box c-box--sm">
                <div class="c-box__inner">
                  <div data-bgimage="../uploads/gallery/Gallery_07_Original.jpg" class="b-box__img-wrap b-box__grdnt-b bg-image"></div>
                  <div class="c-box__date-item c-box__date-item--float">
                    <time datetime="2009-08-29T20:00" class="date-sm c-box__date"><span class="date-sm__dt">23</span><span class="date-sm__rh"><span class="date-sm__rh-m">sept</span><span class="date-sm__rh-d">2014</span></span></time>
                  </div>
                  <ul class="c-box__views">
                    <li class="c-box__view txt">32</li>
                    <li class="c-box__video txt">2</li>
                  </ul><a href="gallery-open.html" rel="nofollow" class="b-box__link"></a>
                </div>
                <h6><a href="gallery-open.html" class="c-box__link">Californication nights</a></h6>
              </article>
              <article class="c-box c-box--sm">
                <div class="c-box__inner">
                  <div data-bgimage="../uploads/gallery/Gallery_04_Original.jpg" class="b-box__img-wrap b-box__grdnt-b bg-image"></div>
                  <div class="c-box__date-item c-box__date-item--float">
                    <time datetime="2009-08-29T20:00" class="date-sm c-box__date"><span class="date-sm__dt">23</span><span class="date-sm__rh"><span class="date-sm__rh-m">sept</span><span class="date-sm__rh-d">2014</span></span></time>
                  </div>
                  <ul class="c-box__views">
                    <li class="c-box__view txt">32</li>
                    <li class="c-box__video txt">2</li>
                  </ul><a href="gallery-open.html" rel="nofollow" class="b-box__link"></a>
                </div>
                <h6><a href="gallery-open.html" class="c-box__link">Mash up your heads: christmas night mash up your heads: christmas night</a></h6>
              </article>
              <article class="c-box c-box--sm">
                <div class="c-box__inner">
                  <div data-bgimage="../uploads/gallery/Gallery_06_Original.jpg" class="b-box__img-wrap b-box__grdnt-b bg-image"></div>
                  <div class="c-box__date-item c-box__date-item--float">
                    <time datetime="2009-08-29T20:00" class="date-sm c-box__date"><span class="date-sm__dt">23</span><span class="date-sm__rh"><span class="date-sm__rh-m">sept</span><span class="date-sm__rh-d">2014</span></span></time>
                  </div>
                  <ul class="c-box__views">
                    <li class="c-box__view txt">32</li>
                    <li class="c-box__video txt">2</li>
                  </ul><a href="gallery-open.html" rel="nofollow" class="b-box__link"></a>
                </div>
                <h6><a href="gallery-open.html" class="c-box__link">Californication nights</a></h6>
              </article>
              <article class="c-box c-box--sm">
                <div class="c-box__inner">
                  <div data-bgimage="../uploads/gallery/Gallery_06_Original.jpg" class="b-box__img-wrap b-box__grdnt-b bg-image"></div>
                  <div class="c-box__date-item c-box__date-item--float">
                    <time datetime="2009-08-29T20:00" class="date-sm c-box__date"><span class="date-sm__dt">23</span><span class="date-sm__rh"><span class="date-sm__rh-m">sept</span><span class="date-sm__rh-d">2014</span></span></time>
                  </div>
                  <ul class="c-box__views">
                    <li class="c-box__view txt">32</li>
                    <li class="c-box__video txt">2</li>
                  </ul><a href="gallery-open.html" rel="nofollow" class="b-box__link"></a>
                </div>
                <h6><a href="gallery-open.html" class="c-box__link">Californication nights</a></h6>
              </article>
              <article class="c-box c-box--sm">
                <div class="c-box__inner">
                  <div data-bgimage="../uploads/gallery/Gallery_04_Original.jpg" class="b-box__img-wrap b-box__grdnt-b bg-image"></div>
                  <div class="c-box__date-item c-box__date-item--float">
                    <time datetime="2009-08-29T20:00" class="date-sm c-box__date"><span class="date-sm__dt">23</span><span class="date-sm__rh"><span class="date-sm__rh-m">sept</span><span class="date-sm__rh-d">2014</span></span></time>
                  </div>
                  <ul class="c-box__views">
                    <li class="c-box__view txt">32</li>
                    <li class="c-box__video txt">2</li>
                  </ul><a href="gallery-open.html" rel="nofollow" class="b-box__link"></a>
                </div>
                <h6><a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">>Mash up your heads<b class="fa fa-angle-down"></b></a></h6>
              </article>
            </div>
          </div>
        </div>
      </div>
    <!--friday events end-->
    <br />
 

   <!--sat Events start-->
   <div class="b-gallery">
        <div data-bgimage="" class="b-gallery__img-wrap bg-image" style="background-image: url(&quot;../uploads/gallery/Gallery_05_Original.jpg&quot;);"></div>
        <div class="container">
          <div class="row">
            <div class="b-item__header col-xs-12">
              <h6 class="s6-heading b-item__ttl-header"id="saturday">Satuday</h6>
            </div>
            <div id="sliderGallery" class="b-gallery__slider btn-white slick-initialized slick-slider"><button type="button" data-role="none" class="slick-prev slick-arrow slick-disabled" aria-label="Previous" role="button" aria-disabled="true" style="display: block;">Previous</button>
              <div aria-live="polite" class="slick-list draggable"><div class="slick-track" style="opacity: 1; width: 3516px; left: 0px;">
                  <article class="c-box c-box--sm slick-slide slick-current slick-active" data-slick-index="0" aria-hidden="false" style="width: 293px;">
                <div class="c-box__inner">
                  <div data-bgimage="../uploads/gallery/Gallery_05_Original.jpg" class="b-box__img-wrap b-box__grdnt-b bg-image" style="background-image: url(&quot;../uploads/gallery/Gallery_05_Original.jpg&quot;);"></div>
                  <div class="c-box__date-item c-box__date-item--float">
                    <time datetime="2009-08-29T20:00" class="date-sm c-box__date"><span class="date-sm__dt">23</span><span class="date-sm__rh"><span class="date-sm__rh-m">sept</span><span class="date-sm__rh-d">2014</span></span></time>
                  </div>
                  <ul class="c-box__views">
                    <li class="c-box__view txt">32</li>
                    <li class="c-box__video txt">2</li>
                  </ul><a href="gallery-open.html" rel="nofollow" class="b-box__link"></a>
                </div>
                <h6><a href="gallery-open.html" class="c-box__link">Mash up your heads: christmas night mash up your heads: christmas night</a></h6>
              </article>
                  <article class="c-box c-box--sm slick-slide slick-active" data-slick-index="1" aria-hidden="false" style="width: 293px;">
                <div class="c-box__inner">
                  <div data-bgimage="../uploads/gallery/Gallery_07_Original.jpg" class="b-box__img-wrap b-box__grdnt-b bg-image" style="background-image: url(&quot;../uploads/gallery/Gallery_07_Original.jpg&quot;);"></div>
                  <div class="c-box__date-item c-box__date-item--float">
                    <time datetime="2009-08-29T20:00" class="date-sm c-box__date"><span class="date-sm__dt">23</span><span class="date-sm__rh"><span class="date-sm__rh-m">sept</span><span class="date-sm__rh-d">2014</span></span></time>
                  </div>
                  <ul class="c-box__views">
                    <li class="c-box__view txt">32</li>
                    <li class="c-box__video txt">2</li>
                  </ul><a href="gallery-open.html" rel="nofollow" class="b-box__link"></a>
                </div>
                <h6><a href="gallery-open.html" class="c-box__link">Californication nights</a></h6>
              </article>
                  <article class="c-box c-box--sm slick-slide slick-active" data-slick-index="2" aria-hidden="false" style="width: 293px;">
                <div class="c-box__inner">
                  <div data-bgimage="../uploads/gallery/Gallery_04_Original.jpg" class="b-box__img-wrap b-box__grdnt-b bg-image" style="background-image: url(&quot;../uploads/gallery/Gallery_04_Original.jpg&quot;);"></div>
                  <div class="c-box__date-item c-box__date-item--float">
                    <time datetime="2009-08-29T20:00" class="date-sm c-box__date"><span class="date-sm__dt">23</span><span class="date-sm__rh"><span class="date-sm__rh-m">sept</span><span class="date-sm__rh-d">2014</span></span></time>
                  </div>
                  <ul class="c-box__views">
                    <li class="c-box__view txt">32</li>
                    <li class="c-box__video txt">2</li>
                  </ul><a href="gallery-open.html" rel="nofollow" class="b-box__link"></a>
                </div>
                <h6><a href="gallery-open.html" class="c-box__link">Mash up your heads: christmas night mash up your heads: christmas night</a></h6>
              </article>
                  <article class="c-box c-box--sm slick-slide slick-active" data-slick-index="3" aria-hidden="false" style="width: 293px;">
                <div class="c-box__inner">
                  <div data-bgimage="../uploads/gallery/Gallery_06_Original.jpg" class="b-box__img-wrap b-box__grdnt-b bg-image" style="background-image: url(&quot;../uploads/gallery/Gallery_06_Original.jpg&quot;);"></div>
                  <div class="c-box__date-item c-box__date-item--float">
                    <time datetime="2009-08-29T20:00" class="date-sm c-box__date"><span class="date-sm__dt">23</span><span class="date-sm__rh"><span class="date-sm__rh-m">sept</span><span class="date-sm__rh-d">2014</span></span></time>
                  </div>
                  <ul class="c-box__views">
                    <li class="c-box__view txt">32</li>
                    <li class="c-box__video txt">2</li>
                  </ul><a href="gallery-open.html" rel="nofollow" class="b-box__link"></a>
                </div>
                <h6><a href="gallery-open.html" class="c-box__link">Californication nights</a></h6>
              </article>
                  <article class="c-box c-box--sm slick-slide" data-slick-index="4" aria-hidden="true" style="width: 293px;">
                <div class="c-box__inner">
                  <div data-bgimage="../uploads/gallery/Gallery_07_Original.jpg" class="b-box__img-wrap b-box__grdnt-b bg-image" style="background-image: url(&quot;../uploads/gallery/Gallery_07_Original.jpg&quot;);"></div>
                  <div class="c-box__date-item c-box__date-item--float">
                    <time datetime="2009-08-29T20:00" class="date-sm c-box__date"><span class="date-sm__dt">23</span><span class="date-sm__rh"><span class="date-sm__rh-m">sept</span><span class="date-sm__rh-d">2014</span></span></time>
                  </div>
                  <ul class="c-box__views">
                    <li class="c-box__view txt">32</li>
                    <li class="c-box__video txt">2</li>
                  </ul><a href="gallery-open.html" rel="nofollow" class="b-box__link"></a>
                </div>
                <h6><a href="gallery-open.html" class="c-box__link">Mash up your heads: christmas night mash up your heads: christmas night</a></h6>
              </article>
                  <article class="c-box c-box--sm slick-slide" data-slick-index="5" aria-hidden="true" style="width: 293px;">
                <div class="c-box__inner">
                  <div data-bgimage="../uploads/gallery/Gallery_05_Original.jpg" class="b-box__img-wrap b-box__grdnt-b bg-image" style="background-image: url(&quot;../uploads/gallery/Gallery_05_Original.jpg&quot;);"></div>
                  <div class="c-box__date-item c-box__date-item--float">
                    <time datetime="2009-08-29T20:00" class="date-sm c-box__date"><span class="date-sm__dt">23</span><span class="date-sm__rh"><span class="date-sm__rh-m">sept</span><span class="date-sm__rh-d">2014</span></span></time>
                  </div>
                  <ul class="c-box__views">
                    <li class="c-box__view txt">32</li>
                    <li class="c-box__video txt">2</li>
                  </ul><a href="gallery-open.html" rel="nofollow" class="b-box__link"></a>
                </div>
                <h6><a href="gallery-open.html" class="c-box__link">Mash up your heads: christmas night mash up your heads: christmas night</a></h6>
              </article>
                  <article class="c-box c-box--sm slick-slide" data-slick-index="6" aria-hidden="true" style="width: 293px;">
                <div class="c-box__inner">
                  <div data-bgimage="../uploads/gallery/Gallery_06_Original.jpg" class="b-box__img-wrap b-box__grdnt-b bg-image" style="background-image: url(&quot;../uploads/gallery/Gallery_06_Original.jpg&quot;);"></div>
                  <div class="c-box__date-item c-box__date-item--float">
                    <time datetime="2009-08-29T20:00" class="date-sm c-box__date"><span class="date-sm__dt">23</span><span class="date-sm__rh"><span class="date-sm__rh-m">sept</span><span class="date-sm__rh-d">2014</span></span></time>
                  </div>
                  <ul class="c-box__views">
                    <li class="c-box__view txt">32</li>
                    <li class="c-box__video txt">2</li>
                  </ul><a href="gallery-open.html" rel="nofollow" class="b-box__link"></a>
                </div>
                <h6><a href="gallery-open.html" class="c-box__link">Mash up your heads: christmas night mash up your heads: christmas night</a></h6>
              </article>
                  <article class="c-box c-box--sm slick-slide" data-slick-index="7" aria-hidden="true" style="width: 293px;">
                <div class="c-box__inner">
                  <div data-bgimage="../uploads/gallery/Gallery_07_Original.jpg" class="b-box__img-wrap b-box__grdnt-b bg-image" style="background-image: url(&quot;../uploads/gallery/Gallery_07_Original.jpg&quot;);"></div>
                  <div class="c-box__date-item c-box__date-item--float">
                    <time datetime="2009-08-29T20:00" class="date-sm c-box__date"><span class="date-sm__dt">23</span><span class="date-sm__rh"><span class="date-sm__rh-m">sept</span><span class="date-sm__rh-d">2014</span></span></time>
                  </div>
                  <ul class="c-box__views">
                    <li class="c-box__view txt">32</li>
                    <li class="c-box__video txt">2</li>
                  </ul><a href="gallery-open.html" rel="nofollow" class="b-box__link"></a>
                </div>
                <h6><a href="gallery-open.html" class="c-box__link">Californication nights</a></h6>
              </article>
                  <article class="c-box c-box--sm slick-slide" data-slick-index="8" aria-hidden="true" style="width: 293px;">
                <div class="c-box__inner">
                  <div data-bgimage="../uploads/gallery/Gallery_04_Original.jpg" class="b-box__img-wrap b-box__grdnt-b bg-image" style="background-image: url(&quot;../uploads/gallery/Gallery_04_Original.jpg&quot;);"></div>
                  <div class="c-box__date-item c-box__date-item--float">
                    <time datetime="2009-08-29T20:00" class="date-sm c-box__date"><span class="date-sm__dt">23</span><span class="date-sm__rh"><span class="date-sm__rh-m">sept</span><span class="date-sm__rh-d">2014</span></span></time>
                  </div>
                  <ul class="c-box__views">
                    <li class="c-box__view txt">32</li>
                    <li class="c-box__video txt">2</li>
                  </ul><a href="gallery-open.html" rel="nofollow" class="b-box__link"></a>
                </div>
                <h6><a href="gallery-open.html" class="c-box__link">Mash up your heads: christmas night mash up your heads: christmas night</a></h6>
              </article>
                  <article class="c-box c-box--sm slick-slide" data-slick-index="9" aria-hidden="true" style="width: 293px;">
                <div class="c-box__inner">
                  <div data-bgimage="../uploads/gallery/Gallery_06_Original.jpg" class="b-box__img-wrap b-box__grdnt-b bg-image" style="background-image: url(&quot;../uploads/gallery/Gallery_06_Original.jpg&quot;);"></div>
                  <div class="c-box__date-item c-box__date-item--float">
                    <time datetime="2009-08-29T20:00" class="date-sm c-box__date"><span class="date-sm__dt">23</span><span class="date-sm__rh"><span class="date-sm__rh-m">sept</span><span class="date-sm__rh-d">2014</span></span></time>
                  </div>
                  <ul class="c-box__views">
                    <li class="c-box__view txt">32</li>
                    <li class="c-box__video txt">2</li>
                  </ul><a href="gallery-open.html" rel="nofollow" class="b-box__link"></a>
                </div>
                <h6><a href="gallery-open.html" class="c-box__link">Californication nights</a></h6>
              </article>
                  <article class="c-box c-box--sm slick-slide" data-slick-index="10" aria-hidden="true" style="width: 293px;">
                <div class="c-box__inner">
                  <div data-bgimage="../uploads/gallery/Gallery_06_Original.jpg" class="b-box__img-wrap b-box__grdnt-b bg-image" style="background-image: url(&quot;../uploads/gallery/Gallery_06_Original.jpg&quot;);"></div>
                  <div class="c-box__date-item c-box__date-item--float">
                    <time datetime="2009-08-29T20:00" class="date-sm c-box__date"><span class="date-sm__dt">23</span><span class="date-sm__rh"><span class="date-sm__rh-m">sept</span><span class="date-sm__rh-d">2014</span></span></time>
                  </div>
                  <ul class="c-box__views">
                    <li class="c-box__view txt">32</li>
                    <li class="c-box__video txt">2</li>
                  </ul><a href="gallery-open.html" rel="nofollow" class="b-box__link"></a>
                </div>
                <h6><a href="gallery-open.html" class="c-box__link">Californication nights</a></h6>
              </article>
                  <article class="c-box c-box--sm slick-slide" data-slick-index="11" aria-hidden="true" style="width: 293px;">
                <div class="c-box__inner">
                  <div data-bgimage="../uploads/gallery/Gallery_04_Original.jpg" class="b-box__img-wrap b-box__grdnt-b bg-image" style="background-image: url(&quot;../uploads/gallery/Gallery_04_Original.jpg&quot;);"></div>
                  <div class="c-box__date-item c-box__date-item--float">
                    <time datetime="2009-08-29T20:00" class="date-sm c-box__date"><span class="date-sm__dt">23</span><span class="date-sm__rh"><span class="date-sm__rh-m">sept</span><span class="date-sm__rh-d">2014</span></span></time>
                  </div>
                  <ul class="c-box__views">
                    <li class="c-box__view txt">32</li>
                    <li class="c-box__video txt">2</li>
                  </ul><a href="gallery-open.html" rel="nofollow" class="b-box__link"></a>
                </div>
                <h6><a href="gallery-open.html" class="c-box__link">Mash up your heads: christmas night mash up your heads: christmas night</a></h6>
              </article>

                                                                   </div>

              </div>
              
              
               <button type="button" data-role="none" class="slick-next slick-arrow" aria-label="Next" role="button" style="display: block;" aria-disabled="false">Next</button></div>
          </div>
        </div>
      </div>

    <!--sat events end-->
   
    <br />


    <!--sun events starts-->
    <div class="b-gallery">
        <div data-bgimage="" class="b-gallery__img-wrap bg-image" style="background-image: url(&quot;../uploads/gallery/Gallery_05_Original.jpg&quot;);"></div>
        <div class="container">
          <div class="row">
            <div class="b-item__header col-xs-12">
              <h6 class="s6-heading b-item__ttl-header">Sunday</h6>
            </div>
            <div id="sliderGallery" class="b-gallery__slider btn-white slick-initialized slick-slider"><button type="button" data-role="none" class="slick-prev slick-arrow slick-disabled" aria-label="Previous" role="button" aria-disabled="true" style="display: block;">Previous</button>
              <div aria-live="polite" class="slick-list draggable"><div class="slick-track" style="opacity: 1; width: 3516px; left: 0px;"><article class="c-box c-box--sm slick-slide slick-current slick-active" data-slick-index="0" aria-hidden="false" style="width: 293px;">
                <div class="c-box__inner">
                  <div data-bgimage="../uploads/gallery/Gallery_05_Original.jpg" class="b-box__img-wrap b-box__grdnt-b bg-image" style="background-image: url(&quot;../uploads/gallery/Gallery_05_Original.jpg&quot;);"></div>
                  <div class="c-box__date-item c-box__date-item--float">
                    <time datetime="2009-08-29T20:00" class="date-sm c-box__date"><span class="date-sm__dt">23</span><span class="date-sm__rh"><span class="date-sm__rh-m">sept</span><span class="date-sm__rh-d">2014</span></span></time>
                  </div>
                  <ul class="c-box__views">
                    <li class="c-box__view txt">32</li>
                    <li class="c-box__video txt">2</li>
                  </ul><a href="gallery-open.html" rel="nofollow" class="b-box__link"></a>
                </div>
                <h6><a href="gallery-open.html" class="c-box__link">Mash up your heads: christmas night mash up your heads: christmas night</a></h6>
              </article><article class="c-box c-box--sm slick-slide slick-active" data-slick-index="1" aria-hidden="false" style="width: 293px;">
                <div class="c-box__inner">
                  <div data-bgimage="../uploads/gallery/Gallery_07_Original.jpg" class="b-box__img-wrap b-box__grdnt-b bg-image" style="background-image: url(&quot;../uploads/gallery/Gallery_07_Original.jpg&quot;);"></div>
                  <div class="c-box__date-item c-box__date-item--float">
                    <time datetime="2009-08-29T20:00" class="date-sm c-box__date"><span class="date-sm__dt">23</span><span class="date-sm__rh"><span class="date-sm__rh-m">sept</span><span class="date-sm__rh-d">2014</span></span></time>
                  </div>
                  <ul class="c-box__views">
                    <li class="c-box__view txt">32</li>
                    <li class="c-box__video txt">2</li>
                  </ul><a href="gallery-open.html" rel="nofollow" class="b-box__link"></a>
                </div>
                <h6><a href="gallery-open.html" class="c-box__link">Californication nights</a></h6>
              </article><article class="c-box c-box--sm slick-slide slick-active" data-slick-index="2" aria-hidden="false" style="width: 293px;">
                <div class="c-box__inner">
                  <div data-bgimage="../uploads/gallery/Gallery_04_Original.jpg" class="b-box__img-wrap b-box__grdnt-b bg-image" style="background-image: url(&quot;../uploads/gallery/Gallery_04_Original.jpg&quot;);"></div>
                  <div class="c-box__date-item c-box__date-item--float">
                    <time datetime="2009-08-29T20:00" class="date-sm c-box__date"><span class="date-sm__dt">23</span><span class="date-sm__rh"><span class="date-sm__rh-m">sept</span><span class="date-sm__rh-d">2014</span></span></time>
                  </div>
                  <ul class="c-box__views">
                    <li class="c-box__view txt">32</li>
                    <li class="c-box__video txt">2</li>
                  </ul><a href="gallery-open.html" rel="nofollow" class="b-box__link"></a>
                </div>
                <h6><a href="gallery-open.html" class="c-box__link">Mash up your heads: christmas night mash up your heads: christmas night</a></h6>
              </article><article class="c-box c-box--sm slick-slide slick-active" data-slick-index="3" aria-hidden="false" style="width: 293px;">
                <div class="c-box__inner">
                  <div data-bgimage="../uploads/gallery/Gallery_06_Original.jpg" class="b-box__img-wrap b-box__grdnt-b bg-image" style="background-image: url(&quot;../uploads/gallery/Gallery_06_Original.jpg&quot;);"></div>
                  <div class="c-box__date-item c-box__date-item--float">
                    <time datetime="2009-08-29T20:00" class="date-sm c-box__date"><span class="date-sm__dt">23</span><span class="date-sm__rh"><span class="date-sm__rh-m">sept</span><span class="date-sm__rh-d">2014</span></span></time>
                  </div>
                  <ul class="c-box__views">
                    <li class="c-box__view txt">32</li>
                    <li class="c-box__video txt">2</li>
                  </ul><a href="gallery-open.html" rel="nofollow" class="b-box__link"></a>
                </div>
                <h6><a href="gallery-open.html" class="c-box__link">Californication nights</a></h6>
              </article><article class="c-box c-box--sm slick-slide" data-slick-index="4" aria-hidden="true" style="width: 293px;">
                <div class="c-box__inner">
                  <div data-bgimage="../uploads/gallery/Gallery_07_Original.jpg" class="b-box__img-wrap b-box__grdnt-b bg-image" style="background-image: url(&quot;../uploads/gallery/Gallery_07_Original.jpg&quot;);"></div>
                  <div class="c-box__date-item c-box__date-item--float">
                    <time datetime="2009-08-29T20:00" class="date-sm c-box__date"><span class="date-sm__dt">23</span><span class="date-sm__rh"><span class="date-sm__rh-m">sept</span><span class="date-sm__rh-d">2014</span></span></time>
                  </div>
                  <ul class="c-box__views">
                    <li class="c-box__view txt">32</li>
                    <li class="c-box__video txt">2</li>
                  </ul><a href="gallery-open.html" rel="nofollow" class="b-box__link"></a>
                </div>
                <h6><a href="gallery-open.html" class="c-box__link">Mash up your heads: christmas night mash up your heads: christmas night</a></h6>
              </article><article class="c-box c-box--sm slick-slide" data-slick-index="5" aria-hidden="true" style="width: 293px;">
                <div class="c-box__inner">
                  <div data-bgimage="../uploads/gallery/Gallery_05_Original.jpg" class="b-box__img-wrap b-box__grdnt-b bg-image" style="background-image: url(&quot;../uploads/gallery/Gallery_05_Original.jpg&quot;);"></div>
                  <div class="c-box__date-item c-box__date-item--float">
                    <time datetime="2009-08-29T20:00" class="date-sm c-box__date"><span class="date-sm__dt">23</span><span class="date-sm__rh"><span class="date-sm__rh-m">sept</span><span class="date-sm__rh-d">2014</span></span></time>
                  </div>
                  <ul class="c-box__views">
                    <li class="c-box__view txt">32</li>
                    <li class="c-box__video txt">2</li>
                  </ul><a href="gallery-open.html" rel="nofollow" class="b-box__link"></a>
                </div>
                <h6><a href="gallery-open.html" class="c-box__link">Mash up your heads: christmas night mash up your heads: christmas night</a></h6>
              </article><article class="c-box c-box--sm slick-slide" data-slick-index="6" aria-hidden="true" style="width: 293px;">
                <div class="c-box__inner">
                  <div data-bgimage="../uploads/gallery/Gallery_06_Original.jpg" class="b-box__img-wrap b-box__grdnt-b bg-image" style="background-image: url(&quot;../uploads/gallery/Gallery_06_Original.jpg&quot;);"></div>
                  <div class="c-box__date-item c-box__date-item--float">
                    <time datetime="2009-08-29T20:00" class="date-sm c-box__date"><span class="date-sm__dt">23</span><span class="date-sm__rh"><span class="date-sm__rh-m">sept</span><span class="date-sm__rh-d">2014</span></span></time>
                  </div>
                  <ul class="c-box__views">
                    <li class="c-box__view txt">32</li>
                    <li class="c-box__video txt">2</li>
                  </ul><a href="gallery-open.html" rel="nofollow" class="b-box__link"></a>
                </div>
                <h6><a href="gallery-open.html" class="c-box__link">Mash up your heads: christmas night mash up your heads: christmas night</a></h6>
              </article><article class="c-box c-box--sm slick-slide" data-slick-index="7" aria-hidden="true" style="width: 293px;">
                <div class="c-box__inner">
                  <div data-bgimage="../uploads/gallery/Gallery_07_Original.jpg" class="b-box__img-wrap b-box__grdnt-b bg-image" style="background-image: url(&quot;../uploads/gallery/Gallery_07_Original.jpg&quot;);"></div>
                  <div class="c-box__date-item c-box__date-item--float">
                    <time datetime="2009-08-29T20:00" class="date-sm c-box__date"><span class="date-sm__dt">23</span><span class="date-sm__rh"><span class="date-sm__rh-m">sept</span><span class="date-sm__rh-d">2014</span></span></time>
                  </div>
                  <ul class="c-box__views">
                    <li class="c-box__view txt">32</li>
                    <li class="c-box__video txt">2</li>
                  </ul><a href="gallery-open.html" rel="nofollow" class="b-box__link"></a>
                </div>
                <h6><a href="gallery-open.html" class="c-box__link">Californication nights</a></h6>
              </article><article class="c-box c-box--sm slick-slide" data-slick-index="8" aria-hidden="true" style="width: 293px;">
                <div class="c-box__inner">
                  <div data-bgimage="../uploads/gallery/Gallery_04_Original.jpg" class="b-box__img-wrap b-box__grdnt-b bg-image" style="background-image: url(&quot;../uploads/gallery/Gallery_04_Original.jpg&quot;);"></div>
                  <div class="c-box__date-item c-box__date-item--float">
                    <time datetime="2009-08-29T20:00" class="date-sm c-box__date"><span class="date-sm__dt">23</span><span class="date-sm__rh"><span class="date-sm__rh-m">sept</span><span class="date-sm__rh-d">2014</span></span></time>
                  </div>
                  <ul class="c-box__views">
                    <li class="c-box__view txt">32</li>
                    <li class="c-box__video txt">2</li>
                  </ul><a href="gallery-open.html" rel="nofollow" class="b-box__link"></a>
                </div>
                <h6><a href="gallery-open.html" class="c-box__link">Mash up your heads: christmas night mash up your heads: christmas night</a></h6>
              </article><article class="c-box c-box--sm slick-slide" data-slick-index="9" aria-hidden="true" style="width: 293px;">
                <div class="c-box__inner">
                  <div data-bgimage="../uploads/gallery/Gallery_06_Original.jpg" class="b-box__img-wrap b-box__grdnt-b bg-image" style="background-image: url(&quot;../uploads/gallery/Gallery_06_Original.jpg&quot;);"></div>
                  <div class="c-box__date-item c-box__date-item--float">
                    <time datetime="2009-08-29T20:00" class="date-sm c-box__date"><span class="date-sm__dt">23</span><span class="date-sm__rh"><span class="date-sm__rh-m">sept</span><span class="date-sm__rh-d">2014</span></span></time>
                  </div>
                  <ul class="c-box__views">
                    <li class="c-box__view txt">32</li>
                    <li class="c-box__video txt">2</li>
                  </ul><a href="gallery-open.html" rel="nofollow" class="b-box__link"></a>
                </div>
                <h6><a href="gallery-open.html" class="c-box__link">Californication nights</a></h6>
              </article><article class="c-box c-box--sm slick-slide" data-slick-index="10" aria-hidden="true" style="width: 293px;">
                <div class="c-box__inner">
                  <div data-bgimage="../uploads/gallery/Gallery_06_Original.jpg" class="b-box__img-wrap b-box__grdnt-b bg-image" style="background-image: url(&quot;../uploads/gallery/Gallery_06_Original.jpg&quot;);"></div>
                  <div class="c-box__date-item c-box__date-item--float">
                    <time datetime="2009-08-29T20:00" class="date-sm c-box__date"><span class="date-sm__dt">23</span><span class="date-sm__rh"><span class="date-sm__rh-m">sept</span><span class="date-sm__rh-d">2014</span></span></time>
                  </div>
                  <ul class="c-box__views">
                    <li class="c-box__view txt">32</li>
                    <li class="c-box__video txt">2</li>
                  </ul><a href="gallery-open.html" rel="nofollow" class="b-box__link"></a>
                </div>
                <h6><a href="gallery-open.html" class="c-box__link">Californication nights</a></h6>
              </article><article class="c-box c-box--sm slick-slide" data-slick-index="11" aria-hidden="true" style="width: 293px;">
                <div class="c-box__inner">
                  <div data-bgimage="../uploads/gallery/Gallery_04_Original.jpg" class="b-box__img-wrap b-box__grdnt-b bg-image" style="background-image: url(&quot;../uploads/gallery/Gallery_04_Original.jpg&quot;);"></div>
                  <div class="c-box__date-item c-box__date-item--float">
                    <time datetime="2009-08-29T20:00" class="date-sm c-box__date"><span class="date-sm__dt">23</span><span class="date-sm__rh"><span class="date-sm__rh-m">sept</span><span class="date-sm__rh-d">2014</span></span></time>
                  </div>
                  <ul class="c-box__views">
                    <li class="c-box__view txt">32</li>
                    <li class="c-box__video txt">2</li>
                  </ul><a href="gallery-open.html" rel="nofollow" class="b-box__link"></a>
                </div>
                <h6><a href="gallery-open.html" class="c-box__link">Mash up your heads: christmas night mash up your heads: christmas night</a></h6>
              </article></div></div>
              
              
              <button type="button" data-role="none" class="slick-next slick-arrow" aria-label="Next" role="button" style="display: block;" aria-disabled="false">Next</button></div>
          </div>
        </div>
      </div>
    <!--sun ends here-->

    <div class="container">
          <div class="row">
            <div class="b-item__header col-xs-12">
              <h6 class="s6-heading b-item__ttl-header" id="Friday">Friday</h6>
            </div>
            <div id="sliderGallery" class="b-gallery__slider btn-white slick-initialized slick-slider"><button type="button" data-role="none" class="slick-prev slick-arrow slick-disabled" aria-label="Previous" role="button" aria-disabled="true" style="display: block;">Previous</button>
              <div aria-live="polite" class="slick-list draggable"><div class="slick-track" style="opacity: 1; width: 3516px; left: 0px;"><article class="c-box c-box--sm slick-slide slick-current slick-active" data-slick-index="0" aria-hidden="false" style="width: 293px;">
                <div class="c-box__inner">
                  <div data-bgimage="images/fri1.jpg" class="b-box__img-wrap b-box__grdnt-b bg-image" style="background-image: url(&quot;images/fri1.jpg&quot;);"></div>
                  <div class="c-box__date-item c-box__date-item--float">
                    <time datetime="2009-08-29T20:00" class="date-sm c-box__date"><span class="date-sm__dt">23</span><span class="date-sm__rh"><span class="date-sm__rh-m">sept</span><span class="date-sm__rh-d">2014</span></span></time>
                  </div>
                  <ul class="c-box__views">
                    <li>  <button class="b-form__btn indent btn-bg--act-b" style="min-height:30PX;">BOOK</button></li>
                  </ul><a href="eventlists.aspx" rel="nofollow" class="b-box__link"></a>
                </div>
                <div id="headingIconClrOne" role="tab" class="b-accordion__panel-heading">
                        <h4 class="b-accordion__panel-ttl" style="color:white;"><a role="button" data-toggle="collapse" data-parent="#accordionIconClr" href="#blockIconClrOne" aria-expanded="true" aria-controls="blockIconClrOne" style="color:white;"><b class="fa fa-music" style="color:white;"></b> <b>DJ NIGHT </b><b class="fa fa-angle-down"></b></a></h4>
                      </div>

                      <div id="blockIconClrOne" role="tabpanel" aria-labelledby="headingIconClrOne" class="b-accordion__collapse collapse in">
                        <div class="b-accordion__body">
                          <p>
                            DJ:Rohit Barker</p>

                           <p>Period: 4th May, 2018 </p>

                            <p>Time: 9:00 PM Onwards</p>
                           
                          <p></p>
                        </div>
                      </div>
              </article><article class="c-box c-box--sm slick-slide slick-active" data-slick-index="1" aria-hidden="false" style="width: 293px;">
                <div class="c-box__inner">
                  <div data-bgimage="images/fri2.jpg" class="b-box__img-wrap b-box__grdnt-b bg-image" style="background-image: url(&quot;images/fri2.jpg&quot;);"></div>
                  <div class="c-box__date-item c-box__date-item--float">
                    <time datetime="2009-08-29T20:00" class="date-sm c-box__date"><span class="date-sm__dt">23</span><span class="date-sm__rh"><span class="date-sm__rh-m">sept</span><span class="date-sm__rh-d">2014</span></span></time>
                  </div>
                  <ul class="c-box__views">
                   <li>  <button class="b-form__btn indent btn-bg--act-b" style="min-height:30PX;">BOOK</button></li>
                  </ul><a href="eventlists.aspx" rel="nofollow" class="b-box__link"></a>
                </div>
                 <div id="headingIconClrtwo" role="tab" class="b-accordion__panel-heading">
                        <h4 class="b-accordion__panel-ttl" style="color:white;"><a role="button" data-toggle="collapse" data-parent="#accordionIconClr" href="#blockIconClrtwo" aria-expanded="true" aria-controls="blockIconClrtwo" style="color:white;"><b class="fa fa-music" style="color:white;"></b> <b>DJ NIGHT </b><b class="fa fa-angle-down"></b></a></h4>
                      </div>
                      <div id="blockIconClrtwo" role="tabpanel" aria-labelledby="headingIconClrtwo" class="b-accordion__collapse collapse in">
                        <div class="b-accordion__body">
                          <p>
                            DJ:</p>

                           <p>Period: 4th May, 2018 </p>

                            <p>Time: 9:00 PM Onwards</p>
                           
                          <p></p>
                        </div>
                      </div>
              </article><article class="c-box c-box--sm slick-slide slick-active" data-slick-index="2" aria-hidden="false" style="width: 293px;">
                <div class="c-box__inner">
                  <div data-bgimage="images/fri3.jpg" class="b-box__img-wrap b-box__grdnt-b bg-image" style="background-image: url(&quot;images/fri3.jpg&quot;);"></div>
                  <div class="c-box__date-item c-box__date-item--float">
                    <time datetime="2009-08-29T20:00" class="date-sm c-box__date"><span class="date-sm__dt">23</span><span class="date-sm__rh"><span class="date-sm__rh-m">sept</span><span class="date-sm__rh-d">2014</span></span></time>
                  </div>
                  <ul class="c-box__views">
                   <li>  <button class="b-form__btn indent btn-bg--act-b" style="min-height:30PX;">BOOK</button></li>
                  </ul><a href="gallery-open.html" rel="nofollow" class="b-box__link"></a>
                </div>
                <h6><a href="gallery-open.html" class="c-box__link">Mash up your heads: christmas night mash up your heads: christmas night</a></h6>
              </article><article class="c-box c-box--sm slick-slide slick-active" data-slick-index="3" aria-hidden="false" style="width: 293px;">
                <div class="c-box__inner">
                  <div data-bgimage="images/fri6.jpg" class="b-box__img-wrap b-box__grdnt-b bg-image" style="background-image: url(&quot;images/fri6.jpg&quot;);"></div>
                  <div class="c-box__date-item c-box__date-item--float">
                    <time datetime="2009-08-29T20:00" class="date-sm c-box__date"><span class="date-sm__dt">23</span><span class="date-sm__rh"><span class="date-sm__rh-m">sept</span><span class="date-sm__rh-d">2014</span></span></time>
                  </div>
                  <ul class="c-box__views">
                    <li>  <button class="b-form__btn indent btn-bg--act-b" style="min-height:30PX;">BOOK</button></li>
                  </ul><a href="gallery-open.html" rel="nofollow" class="b-box__link"></a>
                </div>
                <h6><a href="gallery-open.html" class="c-box__link">Californication nights</a></h6>
              </article><article class="c-box c-box--sm slick-slide" data-slick-index="4" aria-hidden="true" style="width: 293px;">
                <div class="c-box__inner">
                  <div data-bgimage="../uploads/gallery/Gallery_07_Original.jpg" class="b-box__img-wrap b-box__grdnt-b bg-image" style="background-image: url(&quot;../uploads/gallery/Gallery_07_Original.jpg&quot;);"></div>
                  <div class="c-box__date-item c-box__date-item--float">
                    <time datetime="2009-08-29T20:00" class="date-sm c-box__date"><span class="date-sm__dt">23</span><span class="date-sm__rh"><span class="date-sm__rh-m">sept</span><span class="date-sm__rh-d">2014</span></span></time>
                  </div>
                  <ul class="c-box__views">
                    <li>  <button class="b-form__btn indent btn-bg--act-b" style="min-height:30PX;">BOOK</button></li>
                  </ul><a href="gallery-open.html" rel="nofollow" class="b-box__link"></a>
                </div>
                <h6><a href="gallery-open.html" class="c-box__link">Mash up your heads: christmas night mash up your heads: christmas night</a></h6>
              </article><article class="c-box c-box--sm slick-slide" data-slick-index="5" aria-hidden="true" style="width: 293px;">
                <div class="c-box__inner">
                  <div data-bgimage="../uploads/gallery/Gallery_05_Original.jpg" class="b-box__img-wrap b-box__grdnt-b bg-image" style="background-image: url(&quot;../uploads/gallery/Gallery_05_Original.jpg&quot;);"></div>
                  <div class="c-box__date-item c-box__date-item--float">
                    <time datetime="2009-08-29T20:00" class="date-sm c-box__date"><span class="date-sm__dt">23</span><span class="date-sm__rh"><span class="date-sm__rh-m">sept</span><span class="date-sm__rh-d">2014</span></span></time>
                  </div>
                  <ul class="c-box__views">
                    <li>  <button class="b-form__btn indent btn-bg--act-b" style="min-height:30PX;">BOOK</button></li>
                  </ul><a href="gallery-open.html" rel="nofollow" class="b-box__link"></a>
                </div>
                <h6><a href="gallery-open.html" class="c-box__link">Mash up your heads: christmas night mash up your heads: christmas night</a></h6>
              </article><article class="c-box c-box--sm slick-slide" data-slick-index="6" aria-hidden="true" style="width: 293px;">
                <div class="c-box__inner">
                  <div data-bgimage="../uploads/gallery/Gallery_06_Original.jpg" class="b-box__img-wrap b-box__grdnt-b bg-image" style="background-image: url(&quot;../uploads/gallery/Gallery_06_Original.jpg&quot;);"></div>
                  <div class="c-box__date-item c-box__date-item--float">
                    <time datetime="2009-08-29T20:00" class="date-sm c-box__date"><span class="date-sm__dt">23</span><span class="date-sm__rh"><span class="date-sm__rh-m">sept</span><span class="date-sm__rh-d">2014</span></span></time>
                  </div>
                  <ul class="c-box__views">
                   <li>  <button class="b-form__btn indent btn-bg--act-b" style="min-height:30PX;">BOOK</button></li>
                  </ul><a href="gallery-open.html" rel="nofollow" class="b-box__link"></a>
                </div>
                <h6><a href="gallery-open.html" class="c-box__link">Mash up your heads: christmas night mash up your heads: christmas night</a></h6>
              </article><article class="c-box c-box--sm slick-slide" data-slick-index="7" aria-hidden="true" style="width: 293px;">
                <div class="c-box__inner">
                  <div data-bgimage="../uploads/gallery/Gallery_07_Original.jpg" class="b-box__img-wrap b-box__grdnt-b bg-image" style="background-image: url(&quot;../uploads/gallery/Gallery_07_Original.jpg&quot;);"></div>
                  <div class="c-box__date-item c-box__date-item--float">
                    <time datetime="2009-08-29T20:00" class="date-sm c-box__date"><span class="date-sm__dt">23</span><span class="date-sm__rh"><span class="date-sm__rh-m">sept</span><span class="date-sm__rh-d">2014</span></span></time>
                  </div>
                  <ul class="c-box__views">
                    <li class="c-box__view txt">32</li>
                    <li class="c-box__video txt">2</li>
                  </ul><a href="gallery-open.html" rel="nofollow" class="b-box__link"></a>
                </div>
                <h6><a href="gallery-open.html" class="c-box__link">Californication nights</a></h6>
              </article><article class="c-box c-box--sm slick-slide" data-slick-index="8" aria-hidden="true" style="width: 293px;">
                <div class="c-box__inner">
                  <div data-bgimage="../uploads/gallery/Gallery_04_Original.jpg" class="b-box__img-wrap b-box__grdnt-b bg-image" style="background-image: url(&quot;../uploads/gallery/Gallery_04_Original.jpg&quot;);"></div>
                  <div class="c-box__date-item c-box__date-item--float">
                    <time datetime="2009-08-29T20:00" class="date-sm c-box__date"><span class="date-sm__dt">23</span><span class="date-sm__rh"><span class="date-sm__rh-m">sept</span><span class="date-sm__rh-d">2014</span></span></time>
                  </div>
                  <ul class="c-box__views">
                    <li class="c-box__view txt">32</li>
                    <li class="c-box__video txt">2</li>
                  </ul><a href="gallery-open.html" rel="nofollow" class="b-box__link"></a>
                </div>
                <h6><a href="gallery-open.html" class="c-box__link">Mash up your heads: christmas night mash up your heads: christmas night</a></h6>
              </article><article class="c-box c-box--sm slick-slide" data-slick-index="9" aria-hidden="true" style="width: 293px;">
                <div class="c-box__inner">
                  <div data-bgimage="../uploads/gallery/Gallery_06_Original.jpg" class="b-box__img-wrap b-box__grdnt-b bg-image" style="background-image: url(&quot;../uploads/gallery/Gallery_06_Original.jpg&quot;);"></div>
                  <div class="c-box__date-item c-box__date-item--float">
                    <time datetime="2009-08-29T20:00" class="date-sm c-box__date"><span class="date-sm__dt">23</span><span class="date-sm__rh"><span class="date-sm__rh-m">sept</span><span class="date-sm__rh-d">2014</span></span></time>
                  </div>
                  <ul class="c-box__views">
                    <li class="c-box__view txt">32</li>
                    <li class="c-box__video txt">2</li>
                  </ul><a href="gallery-open.html" rel="nofollow" class="b-box__link"></a>
                </div>
                <h6><a href="gallery-open.html" class="c-box__link">Californication nights</a></h6>
              </article><article class="c-box c-box--sm slick-slide" data-slick-index="10" aria-hidden="true" style="width: 293px;">
                <div class="c-box__inner">
                  <div data-bgimage="../uploads/gallery/Gallery_06_Original.jpg" class="b-box__img-wrap b-box__grdnt-b bg-image" style="background-image: url(&quot;../uploads/gallery/Gallery_06_Original.jpg&quot;);"></div>
                  <div class="c-box__date-item c-box__date-item--float">
                    <time datetime="2009-08-29T20:00" class="date-sm c-box__date"><span class="date-sm__dt">23</span><span class="date-sm__rh"><span class="date-sm__rh-m">sept</span><span class="date-sm__rh-d">2014</span></span></time>
                  </div>
                  <ul class="c-box__views">
                    <li class="c-box__view txt">32</li>
                    <li class="c-box__video txt">2</li>
                  </ul><a href="gallery-open.html" rel="nofollow" class="b-box__link"></a>
                </div>
                <h6><a href="gallery-open.html" class="c-box__link">Californication nights</a></h6>
              </article><article class="c-box c-box--sm slick-slide" data-slick-index="11" aria-hidden="true" style="width: 293px;">
                <div class="c-box__inner">
                  <div data-bgimage="../uploads/gallery/Gallery_04_Original.jpg" class="b-box__img-wrap b-box__grdnt-b bg-image" style="background-image: url(&quot;../uploads/gallery/Gallery_04_Original.jpg&quot;);"></div>
                  <div class="c-box__date-item c-box__date-item--float">
                    <time datetime="2009-08-29T20:00" class="date-sm c-box__date"><span class="date-sm__dt">23</span><span class="date-sm__rh"><span class="date-sm__rh-m">sept</span><span class="date-sm__rh-d">2014</span></span></time>
                  </div>
                  <ul class="c-box__views">
                    <li class="c-box__view txt">32</li>
                    <li class="c-box__video txt">2</li>
                  </ul><a href="gallery-open.html" rel="nofollow" class="b-box__link"></a>
                </div>
                <h6><a href="gallery-open.html" class="c-box__link">Mash up your heads: christmas night mash up your heads: christmas night</a></h6>
              </article></div></div>
              
              
              
              
              
              
              
              
              
              
              
         <button type="button" data-role="none" class="slick-next slick-arrow" aria-label="Next" role="button" style="display: block;" aria-disabled="false">Next</button></div>
          </div>
        </div>
						
					
								  
						
</asp:Content>

