﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeFile="Why HIGH.aspx.cs" Inherits="html_themeclubcube_design_Why_HIGH" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <!-- Sliding start -->
    <div class="main-slide">
        <div class="main-slide__btn-item"></div>
        <div id="sliderMain" class="main-slide__slider">
          <div class="main-slide__item-a">
            <div data-bgimage="images/high-dine.jpg" class="main-slide__img-wrap bg-image"></div>
            <div class="container" >
              
            </div>
          </div>
        
          <div class="main-slide__item-b" >
             
            <div class="main-slide__bg-wrap bg-grdnt"  style="background-image:url(images/high-mix.jpg)"></div>
            <div class="container">
              
            </div>
          </div>
        </div>
      </div>
     <!-- Sliding end -->

    <!--why high-->

    <div class="b-promo" style="background-color:black; border-top:solid;border-color:darkgoldenrod;">
        <div class="container">
          <h2 class="b-promo__s2-ttl" style="color:goldenrod;">‘Luxury in not necessity for me but good thing is’, Oh, please stay away from HIGH!'</h2>
          <p style="color:white;">
          3000 Feet above the sea level, the garden city is unique combination of being Pub and Startup capital of India.
          The city is situated even above Dehradun and enjoys pleasant climate throughout the year. With the panoramic view 
          of the city, the best lunge bar of Bangalore is on 31st floor of the World Trade Center. Spread lavishly across 10,000
          square feet, High Ultra Lounge is a modern Asian bar and rooftop restaurant that is based on the theme of timelessness. 
          Blending the outdoors and indoors with breath taking views of the city combined with sensuous interiors, the venue raises 
          the bar and redefines the experience of fine dining. Embark on an exciting gastronomical journey or dance the night away 
          to the pumped up tunes of the best DJ’s in town. <br /><br />
          The venue sits atop the World Trade Centre in Bangalore at a height of 421 feet, making it the tallest dinning point in 
          South India. With the theme of timeliness one of the best Lounge Bar in the world, the venue is divided into HIGH Edge, 
          HIGH Dine, High Mix, HIGH View and High Live, We’ve got the right HIGH for every mood!
            
          </p>
          
        </div>
      </div>
    
    <!--why high-->
          
</asp:Content>

