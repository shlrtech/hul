﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Gallery1.aspx.cs" Inherits="Gallery1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<div class="b-promo" style="background-color: black; border-top: solid; border-color: darkgoldenrod;">
   <div class="container">
       <h1 class="b-headlines__ttl" style="color: goldenrod;">High Gallery</h1>


        <div class="container">
            <div class="row">
               <%-- <div class="b-tabs col-lg-8" style="width: 99%">--%>
                    <%--<h6 class="s6-heading b-tabs__ttl">Current Offers</h6>--%>
                   
                    <div class="b-tabs__content">
                        <div id="block-a" role="tabpanel" class="b-tabs__pane fade in active row">
                            <div class ="col-md-12">
                            <div class="b-tabs__pane-item col-sm-6">
                                <div class="c-box">
                                    <div class="c-box__inner">
                                        <div class="c-box__cont">
                                           <%-- <h3 class="c-box__ttl-s3">TRAP  ONLY. best records plus fit</h3>
                                            <hr class="hr-pattern">
                                            <p class="c-box__txt">Stay B, Andrew Feeling, Someone Else</p>--%>
                                        </div>
                                        <div data-bgimage="images/high-food.jpg" class="b-box__img-wrap b-box__grdnt-a bg-image"></div>
                                        <%--<div class="c-box__date-item">
                                            <time datetime="2009-08-29T20:00" class="date c-box__date"><span class="date__dt">23</span><span class="date__rh"><span class="date__rh-m">sept</span><span class="date__rh-d">Mon</span></span><span class="date__ad">8:00 pm</span></time>
                                        </div>--%>
                                        <div class="c-box__link-wrap">
                                            <div class="btn-pointer__wrap">
                                                <a class="btn btn-pointer" href="AMBIANCE.aspx"><span class="btn-pointer__txt">AMBIANCE</span></a>
                                                <svg class="btn-pointer__left" x="0px" y="0px"
                                                    viewbox="0 0 10 50" style="enable-background: new 0 0 10 50;" xml:space="preserve">
                                                    <path fill="#A82743" d="M10,50H0l5-5l-5-5l0,0v0l0,0l5-5l-5-5l0,0v0l0,0l5-5l-5-5l0,0v0l0,0l5-5l-5-5l0,0v0l0,0l5-5L0,0l0,0v0h10V50z"/></svg>

                                            </div>
                                        </div>
                                        <a href="AMBIANCE.aspx" rel="nofollow" class="b-box__link"></a>
                                    </div>
                                </div>
                            </div>

                            <div class="b-tabs__pane-item col-sm-6">
                                <div class="c-box">
                                    <div class="c-box__inner">
                                        <div class="c-box__cont">
                                            <%--<h3 class="c-box__ttl-s3">TRAP  ONLY. best records plus fit</h3>
                                            <hr class="hr-pattern">
                                            <p class="c-box__txt">Stay B, Andrew Feeling, Someone Else</p>--%>
                                        </div>
                                        <div data-bgimage="images/super-high.jpg" class="b-box__img-wrap b-box__grdnt-a bg-image"></div>
                                       <%-- <div class="c-box__date-item">
                                            <time datetime="2009-08-29T20:00" class="date c-box__date"><span class="date__dt">23</span><span class="date__rh"><span class="date__rh-m">sept</span><span class="date__rh-d">Mon</span></span><span class="date__ad">8:00 pm</span></time>
                                        </div>--%>
                                        <div class="c-box__link-wrap">
                                            <div class="btn-pointer__wrap">
                                                <a class="btn btn-pointer" href="FOOD_BEVERAGES.aspx"><span class="btn-pointer__txt">FOOD & BEVERAGES</span></a>
                                                <svg class="btn-pointer__left" x="0px" y="0px"
                                                    viewbox="0 0 10 50" style="enable-background: new 0 0 10 50;" xml:space="preserve">
                                                    <path fill="#A82743" d="M10,50H0l5-5l-5-5l0,0v0l0,0l5-5l-5-5l0,0v0l0,0l5-5l-5-5l0,0v0l0,0l5-5l-5-5l0,0v0l0,0l5-5L0,0l0,0v0h10V50z"/></svg>

                                            </div>
                                        </div>
                                        <a href="FOOD_BEVERAGES.aspx" rel="nofollow" class="b-box__link"></a>
                                    </div>
                                </div>
                            </div>

                            <div class="b-tabs__pane-item col-sm-6">
                                <div class="c-box">
                                    <div class="c-box__inner">
                                        <div class="c-box__cont">
                                            <%--<h3 class="c-box__ttl-s3">TRAP  ONLY. best records plus fit</h3>
                                            <hr class="hr-pattern">
                                            <p class="c-box__txt">Stay B, Andrew Feeling, Someone Else</p>--%>
                                        </div>
                                        <div data-bgimage="images/fri1.jpg" class="b-box__img-wrap b-box__grdnt-a bg-image"></div>
                                       <%-- <div class="c-box__date-item">
                                            <time datetime="2009-08-29T20:00" class="date c-box__date"><span class="date__dt">23</span><span class="date__rh"><span class="date__rh-m">sept</span><span class="date__rh-d">Mon</span></span><span class="date__ad">8:00 pm</span></time>
                                        </div>--%>
                                        <div class="c-box__link-wrap">
                                            <div class="btn-pointer__wrap">
                                                <a class="btn btn-pointer" href="PARTY.aspx"><span class="btn-pointer__txt">PARTY</span></a>
                                                <svg class="btn-pointer__left" x="0px" y="0px"
                                                    viewbox="0 0 10 50" style="enable-background: new 0 0 10 50;" xml:space="preserve">
                                                    <path fill="#A82743" d="M10,50H0l5-5l-5-5l0,0v0l0,0l5-5l-5-5l0,0v0l0,0l5-5l-5-5l0,0v0l0,0l5-5l-5-5l0,0v0l0,0l5-5L0,0l0,0v0h10V50z"/></svg>

                                            </div>
                                        </div>
                                        <a href="PARTY.aspx" rel="nofollow" class="b-box__link"></a>
                                    </div>
                                </div>
                            </div>

                            </div>
                            
                        </div>
                    </div>
               <%-- </div>--%>
            </div>
        </div>
    </div>
 </div>

</asp:Content>

