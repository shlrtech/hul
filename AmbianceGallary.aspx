﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeFile="AmbianceGallary.aspx.cs" Inherits="AmbianceGallary" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server"></asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
 <head> <style type="text/css">
	 
	 
	 @media screen and (max-width:640px) {
            .slideshow-container{
                margin-left: 5px !important;
                margin-top: 216px !important;
                margin-right: 5px !important;
}
               
            }

* {box-sizing: border-box}
body {font-family: Verdana, sans-serif; margin:0}
.mySlides {display: none}
img {vertical-align: middle;}

/* Slideshow container */
.slideshow-container {
  max-width: 1000px;
  position: relative;
  margin: auto;
}

/* Next & previous buttons */
.prev, .next {
  cursor: pointer;
  position: absolute;
  top: 50%;
  width: auto;
  padding: 16px;
  margin-top: -22px;
  color: white;
  font-weight: bold;
  font-size: 40px;
  transition: 0.9s ease;
  border-radius: 0 3px 3px 0;
}

/* Position the "next button" to the right */
.next {
  right: 0;
  border-radius: 3px 0 0 3px;
}

/* On hover, add a black background color with a little bit see-through */
.prev:hover, .next:hover {
  background-color: rgba(0,0,0,0.8);
}

/* Caption text */
.text {
  color: #f2f2f2;
  font-size: 15px;
  padding: 8px 12px;
  position: absolute;
  bottom: 8px;
  width: 100%;
  text-align: center;
}

/* Number text (1/3 etc) */
.numbertext {
  color: #f2f2f2;
  font-size: 12px;
  padding: 8px 12px;
  position: absolute;
  top: 0;
}

/* The dots/bullets/indicators */
.dot {
  cursor: pointer;
  height: 15px;
  width: 15px;
  margin: 0 2px;
  background-color: #bbb;
  border-radius: 50%;
  display: inline-block;
  transition: background-color 0.6s ease;
}

.active, .dot:hover {
  background-color: #717171;
}

/* Fading animation */
.fade {
  -webkit-animation-name: fade;
  -webkit-animation-duration: 3.5s;
  animation-name: fade;
  animation-duration: 3.5s;
}

@-webkit-keyframes fade {
  from {opacity: .4} 
  to {opacity: 1}
}

@keyframes fade {
  from {opacity: .4} 
  to {opacity: 1}
}

/* On smaller screens, decrease text size */
@media only screen and (max-width: 300px) {
  .prev, .next,.text {font-size: 11px}
}
	 .dialog {
  /*background: #ddd;
  border: 1px solid #ccc;
  border-radius: 5px;
  float: left;*/
  /*height: 900px;
  margin: 20px;*/
 position: relative;
  /*width: 900px;*/
  
}

.close-thik:after {
  content: '✖'; /* UTF-8 symbol */
}
</style>
</head>
<body>

<div class="slideshow-container" style="margin-top:100px;">
  
    <div class="mySlides  ">
  <div class="numbertext"></div><div class="dialog">
  <a href="AMBIANCE.aspx" class="close-thik" style="font-size:40px"></a>
  <img src="images/Ambience14/50434652_1187518384735875_1490958073869631488_o.jpg" style="width:100%">
	<div class="text"></div></div>
</div>
    <div class="mySlides  ">
  <div class="numbertext"></div><div class="dialog">
  <a href="AMBIANCE.aspx" class="close-thik" style="font-size:40px"></a>
  <img src="images/Ambience14/High%20Dine.jpg" style="width:100%">
	<div class="text"></div></div>
</div>
<div class="mySlides  ">
  <div class="numbertext"></div><div class="dialog">
  <a href="AMBIANCE.aspx" class="close-thik" style="font-size:40px"></a>
  <img src="images/Ambience14/High%20Edge.jpg" style="width:100%">
	<div class="text"></div></div>
</div> 
    <div class="mySlides  ">
  <div class="numbertext"></div><div class="dialog">
  <a href="AMBIANCE.aspx" class="close-thik" style="font-size:40px"></a>
  <img src="images/Ambience14/High%20Mix.jpg" style="width:100%">
	<div class="text"></div></div>
</div>
    <div class="mySlides  ">
  <div class="numbertext"></div><div class="dialog">
  <a href="AMBIANCE.aspx" class="close-thik" style="font-size:40px"></a>
  <img src="images/Ambience14/High%20View-1.jpg" style="width:100%">
	<div class="text"></div></div>
</div>
     <div class="mySlides  ">
  <div class="numbertext"></div><div class="dialog">
  <a href="AMBIANCE.aspx" class="close-thik" style="font-size:40px"></a>
  <img src="images/Ambience14/61497414_1283346955153017_4807223015987216384_o.jpg" style="width:100%">
	<div class="text"></div></div>
</div>
    <%--;lld;--%>
<div class="mySlides  ">
  <div class="numbertext"></div><div class="dialog">
  <a href="AMBIANCE.aspx" class="close-thik" style="font-size:40px"></a>
  <img src="images/ambi/xx1.jpg" style="width:100%">
	<div class="text"></div></div>
</div>

<div class="mySlides">
  <div class="numbertext"></div><div class="dialog">
  <a href="AMBIANCE.aspx" class="close-thik" style="font-size:40px"></a>
  <img src="images/ambi/xx2.jpg" style="width:100%">
	<div class="text"></div></div>
</div>

<div class="mySlides ">
  <div class="numbertext"></div><div class="dialog">
  <a href="AMBIANCE.aspx" class="close-thik" style="font-size:40px"></a>
  <img src="images/ambi/xx3.jpg" style="width:100%">
	<div class="text"></div></div>
</div>
    <!--<div class="mySlides  ">
  <div class="numbertext">4 / 4</div>
  <img src="images/ambi/a3.jpg" style="width:100%">
  <div class="text"></div>
</div>-->

<a class="prev" onclick="plusSlides(-1)">&#10094;</a>
<a class="next" onclick="plusSlides(1)">&#10095;</a>

</div>
<br>

<div style="text-align:center">
  <span class="dot" onclick="currentSlide(2)"></span> 
  <span class="dot" onclick="currentSlide(2)"></span> 
  <span class="dot" onclick="currentSlide(4)"></span> 
    <span class="dot" onclick="currentSlide(5)"></span> 
     <span class="dot" onclick="currentSlide(6)"></span> 
  <span class="dot" onclick="currentSlide(7)"></span> 
  <span class="dot" onclick="currentSlide(8)"></span> 
    <span class="dot" onclick="currentSlide(9)"></span> 
    <span class="dot" onclick="currentSlide(10)"></span> 
</div>

<script>
//var slideIndex = 1;
//var slideIndex = getParameterByName('ImgIndex');
var slideIndex = window.location.search;
    slideIndex = slideIndex .split("?")[1].split("=")[1];
showSlides(slideIndex);

function plusSlides(n) {
  showSlides(slideIndex += n);
}

function currentSlide(n) {
  showSlides(slideIndex = n);
}

function showSlides(n) {
  var i;
  var slides = document.getElementsByClassName("mySlides");
  var dots = document.getElementsByClassName("dot");
  if (n > slides.length) {slideIndex = 1}    
  if (n < 1) {slideIndex = slides.length}
  for (i = 0; i < slides.length; i++) {
      slides[i].style.display = "none";  
  }
  for (i = 0; i < dots.length; i++) {
      dots[i].className = dots[i].className.replace(" active", "");
  }
  slides[slideIndex-1].style.display = "block";  
  dots[slideIndex-1].className += " active";
}
</script>

</asp:Content>

