﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeFile="eventlists.aspx.cs" Inherits="eventlists" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
      
		<div id="cs-content" class="cs-content"><div id="x-section-1"  class="x-section"  style="margin: 0px;padding: 0px 0px 20px; background-color: transparent;"   >
            <div  class="x-container"  style="margin: 0px auto;padding: 0px;"   >
                <div class="row">
                    <div  class=" col-xs-12 col-md-12 cs-col-custom" style="padding: 0px;" >
<div class="main-slide">
  <div class="main-slide__btn-item"></div>
  <div id="sliderMain" class="main-slide__slider">
              <div class="main-slide__item-a">
            <div data-bgimage="images/Gallery_05_Original.jpg" class="main-slide__img-wrap bg-image"></div>
            <div class="container">
              <div class="main-slide__item-inr">
                <h1 class="main-slide__s1-ttl cubify-clr">DJ NIGHT</h1>
                <hr class="hr-pattern">
                <div data-year="2019" data-month="05" data-day="01" data-hour="06" data-minute="05" class="b-timer"></div>
                   
       
            
         <div class="c-headlines__date-lg" style="right:600px;padding-top:50px; ">
            <time datetime="2015-09-23T08:00" class="date-lg" style="width:280px;"><span class="date-lg__dt">4</span><span class="date-lg__rh"><span class="date-lg__rh-m" >May 2018 </span><span class="date-lg__rh-m"></span>Fri<span class="date-lg__rh-t"></span></span><span class="date-lg__ad" ><a href="#Friday">Fri</a></span></time>
          </div>
                 <ul>
                     <li><p style="padding-left:315px;width:1000px;padding-bottom:4px;">DJ:Rohit Barker</p></li>
                     <li>
                         <p style="padding-left:315px;width:1000px;padding-bottom:4px;"">Period: 10th March, 2018 | 4th May, 2018 |</p></li>
                        <li><p style="padding-left:315px;width:1000px;padding-bottom:4px;">Time: 9:00 PM Onwards</p></li>
                     <li><p style="padding-left:315px;width:1000px;padding-bottom:4px;">Location: Rooftop, World Trade Center.</p>

                       <li><p style="padding-left:315px;width:1000px;padding-bottom:4px;">Reservation Contact Details: 080 4567-4567 | +91 97310-28800</p></li>
                 </ul> 
                  <div class="main-slide__link-wrap">
                    <div class="btn-pointer__wrap">
                      <a class="btn btn-pointer btn-pointer--lg btn-anim-a"><b>Buy Tickets</b></a>
                      <svg class="btn-pointer__left" x="0px" y="0px" viewBox="0 0 10 50" style="enable-background:new 0 0 10 50;" xml:space="preserve">
                      <path fill="#A82743" d="M10,50H0l5-5l-5-5l0,0v0l0,0l5-5l-5-5l0,0v0l0,0l5-5l-5-5l0,0v0l0,0l5-5l-5-5l0,0v0l0,0l5-5L0,0l0,0v0h10V50z"/></svg>

                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
                    <!--event high-->

    <div class="b-promo" style="background-color:black; border-top:solid;border-color:darkgoldenrod;">
        <div class="container">
          <h2 class="b-promo__s2-ttl "style="color:goldenrod" >‘EVENTS @ HIGH!'</h2>
          <p style="color:white;">
          Find The right mood for Corporate Parties, The right setup for Themed Parties, The right venue 
              for Social Gatherings and The most cozy place for Private Events in Bangalore. Also a customer
               testimonial elaborating why High is the best place to host events. <br /><br />
          
            
          </p>

            <div class ="col-md-3">

            </div>
             <div class ="col-md-6">

                 <div class="col-md-12">
                       <div class="form-box">
                          <input type="text" name="name" placeholder="First Name" required style="width:100%;height: 35px;" >
                       </div>
                 </div>
                 <br />
                 <br />
                 <br />
                 <br />
                  <div class="col-md-12">
                       <div class="form-box">
                          <input type="text" name="name" placeholder="Email" required style="width:100%;height: 35px;" >
                       </div>
                 </div>
                  <br />
                 <br />
                 <br />
                 <br />
                  <div class="col-md-12">
                       <div class="form-box">
                          <input type="text" name="name" placeholder="Mobile" required style="width:100%;height: 35px;" >
                       </div>
                 </div>
                  <br />
                 <br />
                 <br />
                 <br />
                  <div class="col-md-12">
                       <div class="form-box">
                          <input type="datetime-local" name="name" placeholder="Date" required style="width:100%;height: 35px;" >
                       </div>
                 </div>  
                 <br />
                 <br />
                 <br />
                 <br />
                  <div class="col-md-12">
                       <div class="form-box">
                         <select style="width:100%;height: 35px;">
                          <option value="volvo">No of People</option>
                          <option value="volvo">1</option>
                          <option value="saab">2</option>
                          <option value="mercedes">3</option>
                          <option value="audi">4</option>
                          <option value="audi">5</option>
                        </select>
                       </div>
                 </div> 
                 <br />
                 <br />
                 <br />
                 <br />
                  <div class="col-md-12">
                       <div class="form-box">
                         <select style="width:100%;height: 35px;">
                          <option value="volvo">Type of Events</option>
                          <option value="volvo">Social</option>
                          <option value="saab">Corporate</option>
                          <option value="mercedes">Other Celebrations</option>
                         
                        </select>
                       </div>
                 </div>  
                  <br />
                 <br />
                 <br />
                 <br />
                  <div class="col-md-12 col-xs-6 col-sm-3">
                       <div class="form-box">
                           <button type="submit" style="height: 45px;width: 27%;" class="b-form__btn indent btn-bg--act-a">Submit</button>
                               
                       
                       </div>
                 </div>                       
             </div>
             <div class ="col-md-3">
                                            
            </div>
          
        </div>
      </div>
    
 
    
    <!-- event high--> 
</asp:Content>

