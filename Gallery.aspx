﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeFile="Gallery.aspx.cs" Inherits="Gallery" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
             <div class="b-gallery__tiles"style="width:1580px;">
        <div class="b-headlines b-headlines--color-a" style="background-color:black;margin-right:172px;">
          <div class="container">
            <div class="b-breadcrumbs b-breadcrumbs--wht">
              <ul class="b-breadcrumbs__list x-small-txt">
                <li><a href="Default.aspx" class="link">main</a></li>
                <li><a href="Gallery.aspx" class="link">gallery</a></li>
<%--                <li>High</li>--%>
              </ul>
            </div>
            <h1 class="b-headlines__ttl">High Gallery</h1>
          </div>
            <br />
           

                <div id="GlrMagic" class="b-gallery__popup" style="background-color:black;">
    
<article class="c-box c-box--sm col-xs-10 col-xm-6 col-md-4 col-lg-3"style="border-style:double;border-color:white; margin-left:100px;height:300px;background-color:transparent;">
              <div class="c-box__inner"style="max-width:320px;height:288px;margin-top:2px;">
                <div data-bgimage="images/AMB5.jpg" class="b-box__img-wrap b-box__grdnt-b bg-image"style="background-color:transparent;"></div>
              </div>
              <h6><a href="images/AMB5.jpg" class="c-box__link" style="color:white;">AMBIANCE</a></h6>
            </article>         
<article class="c-box c-box--sm col-xs-10 col-xm-6 col-md-4 col-lg-3"style="border-style:double;border-color:white; margin-left:30px;height:300px;background-color:transparent;">
              <div class="c-box__inner"style="max-width:320px;height:288px;margin-top:2px;">
                <div data-bgimage="images/super-high.jpg" class="b-box__img-wrap b-box__grdnt-b bg-image"style="background-color:transparent;"></div>
              </div>
              <h6><a href="images/super-high.jpg" class="c-box__link"style="color:white;">FODD & BEVERAGES</a></h6>
            </article>         
<article class="c-box c-box--sm col-xs-10 col-xm-6 col-md-4 col-lg-3"style="border-style:double;border-color:white; margin-left:20px;height:300px;background-color:transparent;">
              <div class="c-box__inner"style="max-width:320px;height:288px;margin-top:2px;">
                <div data-bgimage="images/fri1.jpg" class="b-box__img-wrap b-box__grdnt-b bg-image"style="background-color:transparent;"></div>
              </div>
              <h6><a href="images/fri1.jpg" class="c-box__link"style="color:white;">PARTY</a></h6>
            </article>         

</div>
          
            <br />       
            <br />
            <br />

            <div class="heading" style="height:50PX;">
                <h2 id="Ambiance" style="text-align:center;">AMBIANCE</h2>
            </div>
            <br />
         
            <div id="GlrMagic" class="b-gallery__popup"  style="background-color:black;margin-left:144PX;">

    
          <div class="col-xs-12 col-md-4 col-lg-2" style="border-style:double;border-color:white; margin-left:50px;"><a href="images/AMB1.jpg"><img src="images/AMB1.jpg" alt="foto-gallery"></a></div>
          <div class="col-xs-12 col-md-4 col-lg-2"style="border-style:double;border-color:white; margin-left:50px;"><a href="images/AMB2.jpg"><img src="images/AMB2.jpg" alt="foto-gallery"></a></div>
          <div class="col-xs-12 col-md-4 col-lg-2"style="border-style:double;border-color:white; margin-left:50px;"><a href="images/AMB3.jpg"><img src="images/AMB3.jpg" alt="foto-gallery"></a></div>
          <div class="col-xs-12 col-md-4 col-lg-2"style="border-style:double;border-color:white; margin-left:50px;"><a href="images/AMB6.jpg"><img src="images/AMB6.jpg" alt="foto-gallery"></a></div>

</div>
 <div class="heading" style="height:50PX;">
                <h2 id="Food&Beverages"style="text-align:center;">FOOD & BEVERAGES</h2>
            </div>
            <br />
                    <div id="GlrMagic" class="b-gallery__popup" style="background-color:black;margin-left:144PX;">
                        
 <div class="col-xs-6 col-md-4 col-lg-2"style="border-style:double;border-color:white; margin-left:50px;"><a href="images/g8.jpg"><img src="images/g8.jpg" alt="foto-gallery"></a></div>
          <div class="col-xs-6 col-md-4 col-lg-2"style="border-style:double;border-color:white; margin-left:50px;"><a href="images/g7.jpg"><img src="images/g7.jpg" alt="foto-gallery"></a></div>
          <div class="col-xs-6 col-md-4 col-lg-2"style="border-style:double;border-color:white; margin-left:50px;"><a href="images/g6.jpg"><img src="images/g6.jpg" alt="foto-gallery"></a></div>
       <div class="col-xs-6 col-md-4 col-lg-2"style="border-style:double;border-color:white; margin-left:50px;"><a href="images/high-food9.jpg"><img src="images/high-food9.jpg" alt="foto-gallery"></a></div>
          <div class="col-xs-6 col-md-4 col-lg-2"style="border-style:double;border-color:white; margin-left:50px;"><a href="images/high-food7.jpg"><img src="images/high-food7.jpg" alt="foto-gallery"></a></div>
          <div class="col-xs-6 col-md-4 col-lg-2"style="border-style:double;border-color:white; margin-left:50px;"><a href="images/high-food6.jpg"><img src="images/high-food6.jpg" alt="foto-gallery"></a></div>
          <div class="col-xs-6 col-md-4 col-lg-2"style="border-style:double;border-color:white; margin-left:50px;"><a href="images/high-food5.jpg"><img src="images/high-food5.jpg" alt="foto-gallery"></a></div>
          <div class="col-xs-6 col-md-4 col-lg-2"style="border-style:double;border-color:white; margin-left:50px;"><a href="images/high-food4.jpg"><img src="images/high-food4.jpg" alt="foto-gallery"></a></div>
          <div class="col-xs-6 col-md-4 col-lg-2"style="border-style:double;border-color:white; margin-left:50px;"><a href="images/high-food17.jpg"><img src="images/high-food17.jpg" alt="foto-gallery"></a></div>
          <div class="col-xs-6 col-md-4 col-lg-2"style="border-style:double;border-color:white; margin-left:50px;"><a href="images/high-food16.jpg"><img src="images/high-food16.jpg" alt="foto-gallery"></a></div>
          <div class="col-xs-6 col-md-4 col-lg-2"style="border-style:double;border-color:white; margin-left:50px;"><a href="images/high-food15.jpg"><img src="images/high-food15.jpg" alt="foto-gallery"></a></div>
          <div class="col-xs-6 col-md-4 col-lg-2"style="border-style:double;border-color:white; margin-left:50px;"><a href="images/high-food13.jpg"><img src="images/high-food13.jpg" alt="foto-gallery"></a></div>
          <div class="col-xs-6 col-md-4 col-lg-2"style="border-style:double;border-color:white; margin-left:50px;"><a href="images/high-food12.jpg"><img src="images/high-food12.jpg" alt="foto-gallery"></a></div>
          <div class="col-xs-6 col-md-4 col-lg-2"style="border-style:double;border-color:white; margin-left:50px;"><a href="images/high-food11.jpg"><img src="images/high-food11.jpg" alt="foto-gallery"></a></div>
          <div class="col-xs-6 col-md-4 col-lg-2"style="border-style:double;border-color:white; margin-left:50px;"><a href="images/high-food.jpg"><img src="images/high-food.jpg" alt="foto-gallery"></a></div>
          <div class="col-xs-6 col-md-4 col-lg-2"style="border-style:double;border-color:white; margin-left:50px;"><a href="images/high-food10.jpg"><img src="images/high-food10.jpg" alt="foto-gallery"></a></div>

</div>
            <div class="heading" style="height:50PX;">
                <h2 id="HighParty"style="text-align:center;">HIGH PARTY</h2>
            </div>
            <br />
        <div id="GlrMagic" class="b-gallery__popup" style="background-color:black;margin-left:144PX;"">

       <div class="col-xs-6 col-md-4 col-lg-2" style="border-style:double;border-color:white; margin-left:50px;"><a href="images/fri1.jpg"><img src="images/fri1.jpg" alt="foto-gallery" ></a></div>
         <div class="col-xs-6 col-md-4 col-lg-2"style="border-style:double;border-color:white; margin-left:50px;"><a href="images/fri2.jpg"><img src="images/fri2.jpg" alt="foto-gallery"></a></div>
          <div class="col-xs-6 col-md-4 col-lg-2"style="border-style:double;border-color:white; margin-left:50px;"><a href="images/fri3.jpg"><img src="images/fri3.jpg" alt="foto-gallery"></a></div>
          <div class="col-xs-6 col-md-4 col-lg-2"style="border-style:double;border-color:white; margin-left:50px;"><a href="images/fri6.jpg"><img src="images/fri6.jpg" alt="foto-gallery"></a></div>
          <div class="col-xs-6 col-md-4 col-lg-2"style="border-style:double;border-color:white; margin-left:50px;"><a href="images/sat1.jpg"><img src="images/sat1.jpg" alt="foto-gallery"></a></div>
          <div class="col-xs-6 col-md-4 col-lg-2"style="border-style:double;border-color:white; margin-left:50px;"><a href="images/sat2.jpg"><img src="images/sat2.jpg" alt="foto-gallery"></a></div><br />
          <div class="col-xs-6 col-md-4 col-lg-2"style="border-style:double;border-color:white; margin-left:50px;"><a href="images/sat3.jpg"><img src="images/sat3.jpg" alt="foto-gallery"></a></div><br />
          <div class="col-xs-6 col-md-4 col-lg-2"style="border-style:double;border-color:white; margin-left:50px;"><a href="images/sat4.jpg"><img src="images/sat4.jpg" alt="foto-gallery"></a></div><br />
          <div class="col-xs-6 col-md-4 col-lg-2"style="border-style:double;border-color:white; margin-left:50px;"><a href="images/sun1.jpg"><img src="images/sun1.jpg" alt="foto-gallery"></a></div><br />
          <div class="col-xs-6 col-md-4 col-lg-2"style="border-style:double;border-color:white; margin-left:50px;"><a href="images/sun2.jpg"><img src="images/sun2.jpg" alt="foto-gallery"></a></div><br />
          <div class="col-xs-6 col-md-4 col-lg-2"style="border-style:double;border-color:white; margin-left:50px;"><a href="images/sun3.jpg"><img src="images/sun3.jpg" alt="foto-gallery"></a></div><br />
          <div class="col-xs-6 col-md-4 col-lg-2"style="border-style:double;border-color:white; margin-left:50px;"><a href="images/sun4.jpg"><img src="images/sun4.jpg" alt="foto-gallery"></a></div><br />
          <div class="col-xs-6 col-md-4 col-lg-2"style="border-style:double;border-color:white; margin-left:50px;"><a href="images/ot1.jpg"><img src="images/ot1.jpg" alt="foto-gallery"></a></div><br />
          <div class="col-xs-6 col-md-4 col-lg-2"style="border-style:double;border-color:white; margin-left:50px;"><a href="images/ot2.jpg"><img src="images/ot2.jpg" alt="foto-gallery"></a></div><br />
          <div class="col-xs-6 col-md-4 col-lg-2"style="border-style:double;border-color:white; margin-left:50px;"><a href="images/ot4.jpg"><img src="images/ot4.jpg" alt="foto-gallery"></a></div><br />
          <div class="col-xs-6 col-md-4 col-lg-2"style="border-style:double;border-color:white; margin-left:50px;"><a href="images/oth3.jpg"><img src="images/oth3.jpg" alt="foto-gallery"></a></div><br />
          <div class="col-xs-6 col-md-4 col-lg-2"style="border-style:double;border-color:white; margin-left:50px;"><a href="images/g1.jpg"><img src="images/g1.jpg" alt="foto-gallery"></a></div><br />
          <div class="col-xs-6 col-md-4 col-lg-2"style="border-style:double;border-color:white; margin-left:50px;"><a href="images/g2.jpg"><img src="images/g2.jpg" alt="foto-gallery"></a></div><br />
          <div class="col-xs-6 col-md-4 col-lg-2"style="border-style:double;border-color:white; margin-left:50px;"><a href="images/g3.jpg"><img src="images/g3.jpg" alt="foto-gallery"></a></div><br />
          <div class="col-xs-6 col-md-4 col-lg-2"style="border-style:double;border-color:white; margin-left:50px;"><a href="images/g4.jpg"><img src="images/g4.jpg" alt="foto-gallery"></a></div><br />
      </>
          </div>
      </div>
            

</asp:Content>

