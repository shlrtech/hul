<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeFile="FOOD_BEVERAGES.aspx.cs" Inherits="FODD_BEVERAGES" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <style type="text/css">
        @media screen and (max-width:640px) {
            .b-video-item {
                width: 300px !important;
            }
        }

        @media screen and (max-width:360px) {
            .b-video-item {
                width: 300px !important;
            }
        }
    </style>

    <div class="b-promo" style="background-color: black; border-top: solid; border-color: darkgoldenrod;">
        <div class="container">
            <h1 class="b-headlines__ttl" style="color: white; font-family: 'Univers LT W01_57 Condensed' !important; margin-top: 100px;">DRINKS & BITES</h1>


            <div class="container">
                <div class="row">
                    <%-- <div class="b-tabs col-lg-8" style="width: 99%">--%>
                    <%--<h6 class="s6-heading b-tabs__ttl">Current Offers</h6>--%>

                    <div class="b-tabs__content">
                        <div id="block-a" role="tabpanel" class="b-tabs__pane fade in active row">
                            <div class="col-md-12">
                              <%--  <div class="b-tabs__pane-item col-sm-6">
                                   
                                    <video class="b-video-item" width="560" height="360" controls style="margin-top: 0!important;"> <source src="/images/videos/sheraton.mp4" type="video/mp4">
                    </video>
                                   
                                </div>--%>

                                <div class="b-tabs__pane-item col-sm-6">
                                    <%--<div class="c-box">--%>
                                    <div class="c-box__inner" style="margin-bottom: 20px; height: 360px;">
                                        <div class="c-box__cont">
                                            <%--<h3 class="c-box__ttl-s3">TRAP  ONLY. best records plus fit</h3>
                                            <hr class="hr-pattern">
                                            <p class="c-box__txt">Stay B, Andrew Feeling, Someone Else</p>--%>
                                        </div>
                                        <div data-bgimage="images/Drinks&bites14/53540930_1218747291612984_2185264366473969664_o.jpg" class="b-box__img-wrap b-box__grdnt-a bg-image"></div>
                                        <%-- <div class="c-box__date-item">
                                            <time datetime="2009-08-29T20:00" class="date c-box__date"><span class="date__dt">23</span><span class="date__rh"><span class="date__rh-m">sept</span><span class="date__rh-d">Mon</span></span><span class="date__ad">8:00 pm</span></time>
                                        </div>--%>

                                        <a href="DrinBiteGallary.aspx?ImgIndex=15" rel="nofollow" class="b-box__link"></a>
                                    </div>
                                    <%--</div>--%>
                                </div>
                                

                                <div class="b-tabs__pane-item col-sm-6">
                                    <%--<div class="c-box">--%>
                                    <div class="c-box__inner" style="margin-bottom: 20px; height: 360px;">
                                        <div class="c-box__cont">
                                            <%--<h3 class="c-box__ttl-s3">TRAP  ONLY. best records plus fit</h3>
                                            <hr class="hr-pattern">
                                            <p class="c-box__txt">Stay B, Andrew Feeling, Someone Else</p>--%>
                                        </div>
                                        <div data-bgimage="images/Drinks&bites14/68739601_1352950191526026_5203271287610277888_n.jpg" class="b-box__img-wrap b-box__grdnt-a bg-image"></div>
                                        <%-- <div class="c-box__date-item">
                                            <time datetime="2009-08-29T20:00" class="date c-box__date"><span class="date__dt">23</span><span class="date__rh"><span class="date__rh-m">sept</span><span class="date__rh-d">Mon</span></span><span class="date__ad">8:00 pm</span></time>
                                        </div>--%>

                                        <a href="DrinBiteGallary.aspx?ImgIndex=15" rel="nofollow" class="b-box__link"></a>
                                    </div>
                                    <%--</div>--%>
                                </div>

                                 <div class="b-tabs__pane-item col-sm-6">
                                    <%--<div class="c-box">--%>
                                    <div class="c-box__inner" style="margin-bottom: 20px; height: 360px;">
                                        <div class="c-box__cont">
                                            <%--<h3 class="c-box__ttl-s3">TRAP  ONLY. best records plus fit</h3>
                                            <hr class="hr-pattern">
                                            <p class="c-box__txt">Stay B, Andrew Feeling, Someone Else</p>--%>
                                        </div>
                                        <div data-bgimage="images/Drinks&bites14/60221623_1270040119817034_9204655853574029312_o.jpg" class="b-box__img-wrap b-box__grdnt-a bg-image"></div>
                                        <%-- <div class="c-box__date-item">
                                            <time datetime="2009-08-29T20:00" class="date c-box__date"><span class="date__dt">23</span><span class="date__rh"><span class="date__rh-m">sept</span><span class="date__rh-d">Mon</span></span><span class="date__ad">8:00 pm</span></time>
                                        </div>--%>

                                        <a href="DrinBiteGallary.aspx?ImgIndex=15" rel="nofollow" class="b-box__link"></a>
                                    </div>
                                    <%--</div>--%>
                                </div>

                                <div class="b-tabs__pane-item col-sm-6">
                                    <%--<div class="c-box">--%>
                                    <div class="c-box__inner" style="margin-bottom: 20px; height: 360px;">
                                        <div class="c-box__cont">
                                            <%--<h3 class="c-box__ttl-s3">TRAP  ONLY. best records plus fit</h3>
                                            <hr class="hr-pattern">
                                            <p class="c-box__txt">Stay B, Andrew Feeling, Someone Else</p>--%>
                                        </div>
                                        <div data-bgimage="images/Drinks&bites14/67769554_1339015202919525_8323800443901706240_o.jpg" class="b-box__img-wrap b-box__grdnt-a bg-image"></div>
                                        <%-- <div class="c-box__date-item">
                                            <time datetime="2009-08-29T20:00" class="date c-box__date"><span class="date__dt">23</span><span class="date__rh"><span class="date__rh-m">sept</span><span class="date__rh-d">Mon</span></span><span class="date__ad">8:00 pm</span></time>
                                        </div>--%>

                                        <a href="DrinBiteGallary.aspx?ImgIndex=15" rel="nofollow" class="b-box__link"></a>
                                    </div>
                                    <%--</div>--%>
                                </div>

                                <div class="b-tabs__pane-item col-sm-6">
                                    <%--<div class="c-box">--%>
                                    <div class="c-box__inner" style="margin-bottom: 20px; height: 360px;">
                                        <div class="c-box__cont">
                                            <%--<h3 class="c-box__ttl-s3">TRAP  ONLY. best records plus fit</h3>
                                            <hr class="hr-pattern">
                                            <p class="c-box__txt">Stay B, Andrew Feeling, Someone Else</p>--%>
                                        </div>
                                        <div data-bgimage="images/Drinks&bites14/69226181_1358795767608135_4550953179407187968_n.jpg" class="b-box__img-wrap b-box__grdnt-a bg-image"></div>
                                        <%-- <div class="c-box__date-item">
                                            <time datetime="2009-08-29T20:00" class="date c-box__date"><span class="date__dt">23</span><span class="date__rh"><span class="date__rh-m">sept</span><span class="date__rh-d">Mon</span></span><span class="date__ad">8:00 pm</span></time>
                                        </div>--%>

                                        <a href="DrinBiteGallary.aspx?ImgIndex=15" rel="nofollow" class="b-box__link"></a>
                                    </div>
                                    <%--</div>--%>
                                </div>
                                                              
                                <div class="b-tabs__pane-item col-sm-6">
                                    <%--<div class="c-box">--%>
                                    <div class="c-box__inner" style="margin-bottom: 20px; height: 360px;">
                                        <div class="c-box__cont">
                                            <%--<h3 class="c-box__ttl-s3">TRAP  ONLY. best records plus fit</h3>
                                            <hr class="hr-pattern">
                                            <p class="c-box__txt">Stay B, Andrew Feeling, Someone Else</p>--%>
                                        </div>
                                        <div data-bgimage="images/Drinks&bites14/67828923_1339015016252877_489673168491905024_o.jpg" class="b-box__img-wrap b-box__grdnt-a bg-image"></div>
                                        <%-- <div class="c-box__date-item">
                                            <time datetime="2009-08-29T20:00" class="date c-box__date"><span class="date__dt">23</span><span class="date__rh"><span class="date__rh-m">sept</span><span class="date__rh-d">Mon</span></span><span class="date__ad">8:00 pm</span></time>
                                        </div>--%>

                                        <a href="DrinBiteGallary.aspx?ImgIndex=15" rel="nofollow" class="b-box__link"></a>
                                    </div>
                                    <%--</div>--%>
                                </div>

                                 <div class="b-tabs__pane-item col-sm-6">
                                    <%--<div class="c-box">--%>
                                    <div class="c-box__inner" style="margin-bottom: 20px; height: 360px;">
                                        <div class="c-box__cont">
                                            <%--<h3 class="c-box__ttl-s3">TRAP  ONLY. best records plus fit</h3>
                                            <hr class="hr-pattern">
                                            <p class="c-box__txt">Stay B, Andrew Feeling, Someone Else</p>--%>
                                        </div>
                                        <div data-bgimage="images/Drinks&bites14/69246463_1355464687941243_6968910631936196608_n.jpg" class="b-box__img-wrap b-box__grdnt-a bg-image"></div>
                                        <%-- <div class="c-box__date-item">
                                            <time datetime="2009-08-29T20:00" class="date c-box__date"><span class="date__dt">23</span><span class="date__rh"><span class="date__rh-m">sept</span><span class="date__rh-d">Mon</span></span><span class="date__ad">8:00 pm</span></time>
                                        </div>--%>

                                        <a href="DrinBiteGallary.aspx?ImgIndex=15" rel="nofollow" class="b-box__link"></a>
                                    </div>
                                    <%--</div>--%>
                                </div>
                                
                                <div class="b-tabs__pane-item col-sm-6">
                                    <%--<div class="c-box">--%>
                                    <div class="c-box__inner" style="margin-bottom: 20px; height: 360px;">
                                        <div class="c-box__cont">
                                            <%--<h3 class="c-box__ttl-s3">TRAP  ONLY. best records plus fit</h3>
                                            <hr class="hr-pattern">
                                            <p class="c-box__txt">Stay B, Andrew Feeling, Someone Else</p>--%>
                                        </div>
                                        <div data-bgimage="images/Drinks&bites14/69643136_1359750850845960_8765023258853507072_n.jpg" class="b-box__img-wrap b-box__grdnt-a bg-image"></div>
                                        <%-- <div class="c-box__date-item">
                                            <time datetime="2009-08-29T20:00" class="date c-box__date"><span class="date__dt">23</span><span class="date__rh"><span class="date__rh-m">sept</span><span class="date__rh-d">Mon</span></span><span class="date__ad">8:00 pm</span></time>
                                        </div>--%>

                                        <a href="DrinBiteGallary.aspx?ImgIndex=15" rel="nofollow" class="b-box__link"></a>
                                    </div>
                                    <%--</div>--%>
                                </div>

                                <div class="b-tabs__pane-item col-sm-6">
                                    <%--<div class="c-box">--%>
                                    <div class="c-box__inner" style="margin-bottom: 20px; height: 360px;">
                                        <div class="c-box__cont">
                                            <%--<h3 class="c-box__ttl-s3">TRAP  ONLY. best records plus fit</h3>
                                            <hr class="hr-pattern">
                                            <p class="c-box__txt">Stay B, Andrew Feeling, Someone Else</p>--%>
                                        </div>
                                        <div data-bgimage="images/food/c2.jpg" class="b-box__img-wrap b-box__grdnt-a bg-image"></div>
                                        <%-- <div class="c-box__date-item">
                                            <time datetime="2009-08-29T20:00" class="date c-box__date"><span class="date__dt">23</span><span class="date__rh"><span class="date__rh-m">sept</span><span class="date__rh-d">Mon</span></span><span class="date__ad">8:00 pm</span></time>
                                        </div>--%>

                                        <a href="DrinBiteGallary.aspx?ImgIndex=2" rel="nofollow" class="b-box__link"></a>
                                    </div>
                                    <%--</div>--%>
                                </div>

                                <%--  <div class="b-tabs__pane-item col-sm-6">
                                  
                                        <div class="c-box__inner" style="margin-bottom:20px;height:360px;">
                                            <div class="c-box__cont">
                                               
                                            </div>
                                            <div data-bgimage="images/food/c3.jpg" class="b-box__img-wrap b-box__grdnt-a bg-image"></div>
                                            <a href="DrinBiteGallary.aspx?ImgIndex=3" rel="nofollow" class="b-box__link"></a>
                                        </div>
                                   
                                </div>--%>

                                <div class="b-tabs__pane-item col-sm-6">
                                    <%--<div class="c-box">--%>
                                    <div class="c-box__inner" style="margin-bottom: 20px; height: 360px;">
                                        <div class="c-box__cont">
                                            <%--<h3 class="c-box__ttl-s3">TRAP  ONLY. best records plus fit</h3>
                                            <hr class="hr-pattern">
                                            <p class="c-box__txt">Stay B, Andrew Feeling, Someone Else</p>--%>
                                        </div>
                                        <div data-bgimage="images/food/c4.jpg" class="b-box__img-wrap b-box__grdnt-a bg-image"></div>
                                        <%-- <div class="c-box__date-item">
                                            <time datetime="2009-08-29T20:00" class="date c-box__date"><span class="date__dt">23</span><span class="date__rh"><span class="date__rh-m">sept</span><span class="date__rh-d">Mon</span></span><span class="date__ad">8:00 pm</span></time>
                                        </div>--%>

                                        <a href="DrinBiteGallary.aspx?ImgIndex=4" rel="nofollow" class="b-box__link"></a>
                                    </div>
                                    <%--</div>--%>
                                </div>

                                <div class="b-tabs__pane-item col-sm-6">
                                    <%-- <div class="c-box">--%>
                                    <div class="c-box__inner" style="margin-bottom: 20px; height: 360px;">
                                        <div class="c-box__cont">
                                            <%-- <h3 class="c-box__ttl-s3">TRAP  ONLY. best records plus fit</h3>
                                            <hr class="hr-pattern">
                                            <p class="c-box__txt">Stay B, Andrew Feeling, Someone Else</p>--%>
                                        </div>
                                        <div data-bgimage="images/food/c5.jpg" class="b-box__img-wrap b-box__grdnt-a bg-image"></div>
                                        <%--<div class="c-box__date-item">
                                            <time datetime="2009-08-29T20:00" class="date c-box__date"><span class="date__dt">23</span><span class="date__rh"><span class="date__rh-m">sept</span><span class="date__rh-d">Mon</span></span><span class="date__ad">8:00 pm</span></time>
                                        </div>--%>

                                        <a href="DrinBiteGallary.aspx?ImgIndex=5" rel="nofollow" class="b-box__link"></a>
                                    </div>
                                    <%--</div>--%>
                                </div>

                                <div class="b-tabs__pane-item col-sm-6">
                                    <%--<div class="c-box">--%>
                                    <div class="c-box__inner" style="margin-bottom: 20px; height: 360px;">
                                        <div class="c-box__cont">
                                            <%--<h3 class="c-box__ttl-s3">TRAP  ONLY. best records plus fit</h3>
                                            <hr class="hr-pattern">
                                            <p class="c-box__txt">Stay B, Andrew Feeling, Someone Else</p>--%>
                                        </div>
                                        <div data-bgimage="images/food/c6.jpg" class="b-box__img-wrap b-box__grdnt-a bg-image"></div>
                                        <%-- <div class="c-box__date-item">
                                            <time datetime="2009-08-29T20:00" class="date c-box__date"><span class="date__dt">23</span><span class="date__rh"><span class="date__rh-m">sept</span><span class="date__rh-d">Mon</span></span><span class="date__ad">8:00 pm</span></time>
                                        </div>--%>

                                        <a href="DrinBiteGallary.aspx?ImgIndex=6" rel="nofollow" class="b-box__link"></a>
                                    </div>
                                    <%--</div>--%>
                                </div>

                                <div class="b-tabs__pane-item col-sm-6">
                                    <%--<div class="c-box">--%>
                                    <div class="c-box__inner" style="margin-bottom: 20px; height: 360px;">
                                        <div class="c-box__cont">
                                            <%--<h3 class="c-box__ttl-s3">TRAP  ONLY. best records plus fit</h3>
                                            <hr class="hr-pattern">
                                            <p class="c-box__txt">Stay B, Andrew Feeling, Someone Else</p>--%>
                                        </div>
                                        <div data-bgimage="images/food/c7.jpg" class="b-box__img-wrap b-box__grdnt-a bg-image"></div>
                                        <%-- <div class="c-box__date-item">
                                            <time datetime="2009-08-29T20:00" class="date c-box__date"><span class="date__dt">23</span><span class="date__rh"><span class="date__rh-m">sept</span><span class="date__rh-d">Mon</span></span><span class="date__ad">8:00 pm</span></time>
                                        </div>--%>

                                        <a href="DrinBiteGallary.aspx?ImgIndex=7" rel="nofollow" class="b-box__link"></a>
                                    </div>
                                    <%--</div>--%>
                                </div>

                                <div class="b-tabs__pane-item col-sm-6">
                                    <%--<div class="c-box">--%>
                                    <div class="c-box__inner" style="margin-bottom: 20px; height: 360px;">
                                        <div class="c-box__cont">
                                            <%--<h3 class="c-box__ttl-s3">TRAP  ONLY. best records plus fit</h3>
                                            <hr class="hr-pattern">
                                            <p class="c-box__txt">Stay B, Andrew Feeling, Someone Else</p>--%>
                                        </div>
                                        <div data-bgimage="images/food/b8.jpg" class="b-box__img-wrap b-box__grdnt-a bg-image"></div>
                                        <%-- <div class="c-box__date-item">
                                            <time datetime="2009-08-29T20:00" class="date c-box__date"><span class="date__dt">23</span><span class="date__rh"><span class="date__rh-m">sept</span><span class="date__rh-d">Mon</span></span><span class="date__ad">8:00 pm</span></time>
                                        </div>--%>

                                        <a href="DrinBiteGallary.aspx?ImgIndex=8" rel="nofollow" class="b-box__link"></a>
                                    </div>
                                    <%-- </div>--%>
                                </div>

                                <div class="b-tabs__pane-item col-sm-6">
                                    <%--<div class="c-box">--%>
                                    <div class="c-box__inner" style="margin-bottom: 20px; height: 360px;">
                                        <div class="c-box__cont">
                                            <%-- <h3 class="c-box__ttl-s3">TRAP  ONLY. best records plus fit</h3>
                                            <hr class="hr-pattern">
                                            <p class="c-box__txt">Stay B, Andrew Feeling, Someone Else</p>--%>
                                        </div>
                                        <div data-bgimage="images/food/c999.jpg" class="b-box__img-wrap b-box__grdnt-a bg-image"></div>
                                        <%--<div class="c-box__date-item">
                                            <time datetime="2009-08-29T20:00" class="date c-box__date"><span class="date__dt">23</span><span class="date__rh"><span class="date__rh-m">sept</span><span class="date__rh-d">Mon</span></span><span class="date__ad">8:00 pm</span></time>
                                        </div>--%>

                                        <a href="DrinBiteGallary.aspx?ImgIndex=9" rel="nofollow" class="b-box__link"></a>
                                    </div>
                                    <%--</div>--%>
                                </div>

                                <div class="b-tabs__pane-item col-sm-6">
                                    <%--<div class="c-box">--%>
                                    <div class="c-box__inner" style="margin-bottom: 20px; height: 360px;">
                                        <div class="c-box__cont">
                                            <%--<h3 class="c-box__ttl-s3">TRAP  ONLY. best records plus fit</h3>
                                            <hr class="hr-pattern">
                                            <p class="c-box__txt">Stay B, Andrew Feeling, Someone Else</p>--%>
                                        </div>
                                        <div data-bgimage="images/food/c1000.jpg" class="b-box__img-wrap b-box__grdnt-a bg-image"></div>
                                        <%-- <div class="c-box__date-item">
                                            <time datetime="2009-08-29T20:00" class="date c-box__date"><span class="date__dt">23</span><span class="date__rh"><span class="date__rh-m">sept</span><span class="date__rh-d">Mon</span></span><span class="date__ad">8:00 pm</span></time>
                                        </div>--%>

                                        <a href="DrinBiteGallary.aspx?ImgIndex=10" rel="nofollow" class="b-box__link"></a>
                                    </div>
                                    <%--</div>--%>
                                </div>

                               <%-- <div class="b-tabs__pane-item col-sm-6">
                                    <div class="c-box__inner" style="margin-bottom: 20px; height: 360px;">
                                        <div class="c-box__cont">
                                        </div>
                                        <div data-bgimage="images/food/c111.jpg" class="b-box__img-wrap b-box__grdnt-a bg-image"></div>
                                        <a href="DrinBiteGallary.aspx?ImgIndex=11" rel="nofollow" class="b-box__link"></a>
                                    </div>
                                </div>--%>

                                <div class="b-tabs__pane-item col-sm-6">
                                    <%--<div class="c-box">--%>
                                    <div class="c-box__inner" style="margin-bottom: 20px; height: 360px;">
                                        <div class="c-box__cont">
                                            <%--<h3 class="c-box__ttl-s3">TRAP  ONLY. best records plus fit</h3>
                                            <hr class="hr-pattern">
                                            <p class="c-box__txt">Stay B, Andrew Feeling, Someone Else</p>--%>
                                        </div>
                                        <div data-bgimage="images/food/c1112.jpg" style="background-size: cover" class="b-box__img-wrap b-box__grdnt-a bg-image"></div>
                                        <%-- <div class="c-box__date-item">
                                            <time datetime="2009-08-29T20:00" class="date c-box__date"><span class="date__dt">23</span><span class="date__rh"><span class="date__rh-m">sept</span><span class="date__rh-d">Mon</span></span><span class="date__ad">8:00 pm</span></time>
                                        </div>--%>

                                        <a href="DrinBiteGallary.aspx?ImgIndex=12" rel="nofollow" class="b-box__link"></a>
                                    </div>
                                    <%--</div>--%>
                                </div>


                                <div class="b-tabs__pane-item col-sm-6">
                                    <%--<div class="c-box">--%>
                                    <div class="c-box__inner" style="margin-bottom: 20px; height: 360px;">
                                        <div class="c-box__cont">
                                            <%-- <h3 class="c-box__ttl-s3">TRAP  ONLY. best records plus fit</h3>
                                            <hr class="hr-pattern">
                                            <p class="c-box__txt">Stay B, Andrew Feeling, Someone Else</p>--%>
                                        </div>
                                        <div data-bgimage="images/food/c13.jpg" class="b-box__img-wrap b-box__grdnt-a bg-image"></div>
                                        <%--<div class="c-box__date-item">
                                            <time datetime="2009-08-29T20:00" class="date c-box__date"><span class="date__dt">23</span><span class="date__rh"><span class="date__rh-m">sept</span><span class="date__rh-d">Mon</span></span><span class="date__ad">8:00 pm</span></time>
                                        </div>--%>

                                        <a href="DrinBiteGallary.aspx?ImgIndex=13" rel="nofollow" class="b-box__link"></a>
                                    </div>
                                    <%-- </div>--%>
                                </div>

                                <div class="b-tabs__pane-item col-sm-6">
                                    <%--<div class="c-box">--%>
                                    <div class="c-box__inner" style="margin-bottom: 20px; height: 360px;">
                                        <div class="c-box__cont">
                                            <%--<h3 class="c-box__ttl-s3">TRAP  ONLY. best records plus fit</h3>
                                            <hr class="hr-pattern">
                                            <p class="c-box__txt">Stay B, Andrew Feeling, Someone Else</p>--%>
                                        </div>
                                        <div data-bgimage="images/food/food14.jpg" class="b-box__img-wrap b-box__grdnt-a bg-image"></div>
                                        <%-- <div class="c-box__date-item">
                                            <time datetime="2009-08-29T20:00" class="date c-box__date"><span class="date__dt">23</span><span class="date__rh"><span class="date__rh-m">sept</span><span class="date__rh-d">Mon</span></span><span class="date__ad">8:00 pm</span></time>
                                        </div>--%>

                                        <a href="DrinBiteGallary.aspx?ImgIndex=14" rel="nofollow" class="b-box__link"></a>
                                    </div>
                                    <%--</div>--%>
                                </div>

                                <div class="b-tabs__pane-item col-sm-6">
                                    <%--<div class="c-box">--%>
                                    <div class="c-box__inner" style="margin-bottom: 20px; height: 360px;">
                                        <div class="c-box__cont">
                                            <%--<h3 class="c-box__ttl-s3">TRAP  ONLY. best records plus fit</h3>
                                            <hr class="hr-pattern">
                                            <p class="c-box__txt">Stay B, Andrew Feeling, Someone Else</p>--%>
                                        </div>
                                        <div data-bgimage="images/food/food15.jpg" class="b-box__img-wrap b-box__grdnt-a bg-image"></div>
                                        <%-- <div class="c-box__date-item">
                                            <time datetime="2009-08-29T20:00" class="date c-box__date"><span class="date__dt">23</span><span class="date__rh"><span class="date__rh-m">sept</span><span class="date__rh-d">Mon</span></span><span class="date__ad">8:00 pm</span></time>
                                        </div>--%>

                                        <a href="DrinBiteGallary.aspx?ImgIndex=15" rel="nofollow" class="b-box__link"></a>
                                    </div>
                                    <%--</div>--%>
                                </div>

                                
                              
                               <%-- <div class="b-tabs__pane-item col-sm-6">
                                   
                                    <div class="c-box__inner" style="margin-bottom: 20px; height: 360px;">
                                        <div class="c-box__cont">
                                         
                                        </div>
                                        <div data-bgimage="images/food/c116.jpg" class="b-box__img-wrap b-box__grdnt-a bg-image"></div>
                                       

                                        <a href="DrinBiteGallary.aspx?ImgIndex=16" rel="nofollow" class="b-box__link"></a>
                                    </div>
                                 
                                </div>--%>

                            </div>
                        </div>
                    </div>
                    <%-- </div>--%>
                </div>
            </div>
        </div>
    </div>

</asp:Content>

