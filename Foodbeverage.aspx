<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeFile="Foodbeverage.aspx.cs" Inherits="Foodbeverage" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
   <style type="text/css">
         @import url("https://fast.fonts.net/lt/1.css?apiType=css&c=8d5e1514-dd0b-46fe-8111-004f45677b51&fontids=1476004");
         @font-face {
         font-family: "Univers LT W01_57 Condensed";
         src: url("Fonts/1476004/4a916908-f6f7-4df6-90a1-176fb83137d9.eot?#iefix");
         src: url("Fonts/1476004/4a916908-f6f7-4df6-90a1-176fb83137d9.eot?#iefix") format("eot"),url("Fonts/1476004/bf8f8741-5f64-4db9-a877-a44624092e68.woff2") format("woff2"),url("Fonts/1476004/7ce02c2c-45d4-4dee-90f2-f8034e29ac24.woff") format("woff"),url("Fonts/1476004/0955c906-88fc-47e8-8ea2-0765bdf88050.ttf") format("truetype");
		 }</style>
     <style>
          @media screen and (max-width: 960px) {
            .container {
                width: 100%;
            }

            #a1 {
                width: 20%;
            }
        }
          @media screen and (max-width:640px) {
            .mobim{
                width:100%!important;
                margin:0px!important;
                padding:0px!important
            }
            .mobims{
                height:214px!important;
                width:100%!important;
                background-size:100%!important;
            }
            .ctext{
                width:100% !important;
                 height:auto!important;
            }
            .ctext1{    margin-left: -10.2%!important;

            }
			  .row1{padding-top:90px!important;}
        }
           @media screen and (max-width:960px) {
            .mobim{
                width:100%!important;
                margin:0px!important;
                padding:0px!important
            }
            .mobims{
                height:214px!important;
                width:100%!important;
                background-size:100%!important;
            }
            .ctext{
                width:100% !important;
                 height:auto!important;
                  margin-bottom:0px!important;
                 
            }
        }
    .b-panel-t::before {
    border-left: 0px solid #530d85;
}</style>

    <div class="b-events mobim" style="background-image:url(images/ImagesUpdated/Food.jpg);background-attachment:fixed;">
        <div class="container"style="margin-bottom:100px;" >
          <div class="row row1" style="padding-top:30px;">
            <div class="b-tabs col-lg-8" style="width:99%;">
              <%--<h6 class="s6-heading b-tabs__ttl" style="color:white;"> FOOD & BEVERAGE MENU</h6>--%>
                 <div class="b-content container ">
         
        <div id="a1" class="b-panel-t col-xs-4 ctext " style="top: 21px;    margin-bottom: -66px;    opacity: 0.9;    width: 305px;    height: 5px;    margin-left: -1.23%; border-left: 0px solid #530d85;" >
          <h2 style="color:white; text-align:justify;font-family:'Univers LT W01_57 Condensed' !important;     margin-top: -6px; ">FOOD & BEVERAGE MENU</h2>
    <!--    <div class="b-panel-t__list col-md-12 col-xs-3 ">
    <p  style="color:white;text-align:inherit;"> </p>

            </div>-->
       </div>
</div>




            
              <div class="b-tabs__content">
                <div id="block-a" role="tabpanel" class="b-tabs__pane fade in active row">
                  <div class="b-tabs__pane-item col-sm-6">
                    <%--<div class="c-box">--%>
                      <div class="c-box__inner" style="min-height:385px!important">
                        <div class="c-box__cont" >
                          <h3 class="c-box__ttl-s3"><div class="b-panel-t col-xs-4 ctext ctext1" style="top: -35px;    margin-bottom: -32px;    opacity: 0.7;    width: 60.5%;    height: 10px;   padding-bottom:36px;padding-top:10px;  margin-left: -6.2%; border-left: 0px solid #800080;" >
       <a href="/pdf/HULFoodMenu.pdf">    <b class="ctext " style="color:white; text-align:justify;font-family:'Univers LT W01_57 Condensed' !important; font-size: 24px; padding-top:0px;    padding-left:0%  ">FOOD MENU</b></a> 
        
       </div></h3>
                       <%--   <hr class="hr-pattern">--%>
<%--                          <p class="c-box__txt">Stay B, Andrew Feeling, Someone Else</p>--%>
                        </div>
                        <div  class="b-box__img-wrap  bg-image mobims" style="height:385px;">
                            
                           <a href="/pdf/HULFoodMenu.pdf"><img src="images/ImagesUpdated/foodMenuPic.jpg"  class="b-box__img-wrap bg-image mobims" style="filter:opacity(100%);width:550px;height:385px; border:2px solid #e15e32;" /></a>   
                        </div>
                                                               <!-- <div class="btn-pointer-b__wrap"style="left:108px;bottom:-55PX;">
            <a href="pdf/HULFoodMenu.pdf" target="_blank" class="btn-pointer-b btn-anim-b"><b>FOOD MENU</b></a>
            <svg class="left" x="0px" y="0px" viewBox="0 0 5 50" enable-background="new 0 0 5 50" xml:space="preserve">
            <path fill="#000000" d="M5.0001,49.0002H0V33.8014l4.4054-4.4053L0,24.9905v-0.9612l4.4054-4.4051L0,15.2184V0h5.0001V49.0002z"/>
            </svg>
            
            <svg class="right" x="0px" y="0px" viewBox="0 0 5 50" enable-background="new 0 0 5 50" xml:space="preserve">
            <path fill="#000000" d="M0,0l5.0001,0v15.1988l-4.4054,4.4053l4.4054,4.4056v0.9612L0.5947,29.376l4.4054,4.4058v15.2184H0L0,0z"/>
            </svg>
            
          </div>-->
                      </div>
                  <%--  </div>--%>
                  </div>
                    <div class="b-tabs__pane-item col-sm-6">
                    <%--<div class="c-box">--%>
                      <div class="c-box__inner" style="min-height:385px!important">
                        <div class="c-box__cont" >
                          <h3 class="c-box__ttl-s3"><div class="b-panel-t col-xs-4 ctext ctext1" style="top: -35px;    margin-bottom: -32px;    opacity: 0.7;    width: 73.5%;    height: 10px;   padding-bottom:36px;padding-top:10px;  margin-left: -6.2%; border-left: 0px solid #800080;" >
       <a href="/pdf/HULBeverage.pdf">    <b class="ctext " style="color:white; text-align:justify;font-family:'Univers LT W01_57 Condensed' !important; font-size: 24px; padding-top:0px;    padding-left:0%  ">Beverage Menu</b></a> 
        
       </div></h3>
                       <%--   <hr class="hr-pattern">--%>
<%--                          <p class="c-box__txt">Stay B, Andrew Feeling, Someone Else</p>--%>
                        </div>
                        <div  class="b-box__img-wrap  bg-image mobims" style="height:385px;">
                            
                           <a href="/pdf/HULBeverage.pdf"><img src="images/ImagesUpdated/bevarage12.jpg"  class="b-box__img-wrap bg-image mobims" style="filter:opacity(100%);width:550px;height:385px; border:2px solid #e15e32;" /></a>   
                        </div>
                                                               <!-- <div class="btn-pointer-b__wrap"style="left:108px;bottom:-55PX;">
            <a href="pdf/HULFoodMenu.pdf" target="_blank" class="btn-pointer-b btn-anim-b"><b>FOOD MENU</b></a>
            <svg class="left" x="0px" y="0px" viewBox="0 0 5 50" enable-background="new 0 0 5 50" xml:space="preserve">
            <path fill="#000000" d="M5.0001,49.0002H0V33.8014l4.4054-4.4053L0,24.9905v-0.9612l4.4054-4.4051L0,15.2184V0h5.0001V49.0002z"/>
            </svg>
            
            <svg class="right" x="0px" y="0px" viewBox="0 0 5 50" enable-background="new 0 0 5 50" xml:space="preserve">
            <path fill="#000000" d="M0,0l5.0001,0v15.1988l-4.4054,4.4053l4.4054,4.4056v0.9612L0.5947,29.376l4.4054,4.4058v15.2184H0L0,0z"/>
            </svg>
            
          </div>-->
                      </div>
                  <%--  </div>--%>
                  </div>
                  </div>
                </div></div>
            </div>
        </div>
        </div>
</asp:Content>
