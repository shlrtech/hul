﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeFile="currentoffers.aspx.cs" Inherits="currentoffers" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <!-- Sliding start -->

    <div class="b-headlines"style="background-image:url(images/high-dine.jpg);height:350px;">
        <div class="container" >
          <div class="b-breadcrumbs">
           
          </div>
         
        </div>
      </div>
     <!-- Sliding end -->

    <!-- Current Offers 1-->
      
   <div class="b-events"style="background-color:black">
        <div class="container">
          <div class="row">
            <div class="b-tabs col-lg-8 " style="width:108% ;">
              <h6 class="s6-heading b-tabs__ttl"style="color:white;">Current Offers-Pick and Choose the Appetizers from our menu</h6>
                                          <p class="c-box__txt">NOTE:Cannot be clubbed with other offers. Prices does not include taxes.<br /><BR /><strong style="color:red;">Offer Valid Till JUNE 30TH SAT</strong></p>

            
              <div class="b-tabs__content">
                <div id="block-a" role="tabpanel" class="b-tabs__pane fade in active row">
                  <div class="b-tabs__pane-item col-sm-6">
                    <div class="c-box">
                      <div class="c-box__inner">
                        <div class="c-box__cont">
                          <h3 class="c-box__ttl-s3">OFFER 1 </h3>
                          <hr class="hr-pattern">
                          <p class="c-box__txt"style="width:250px;">2 Vegetarian and 2 Non Vegetarian Appetizers</p><br />
                            <p class="c-box__txt" style="width:250px">Offer Valid Till JUNE 30TH SAT </p><br />
                        <!-- <div class="col-xs-12 col-xm-6"><a href="#" class="btn indent btn-info btn-anim-a btn--act-a"style="width:220px;">
                          <span class="clr-black"style="height:50px;"></span>
                          <span><svg x="0px" y="0px"
                          viewBox="0 0 10 50" style="enable-background:new 0 0 10 50;" xml:space="preserve">
                          <path fill="#A82743" d="M10,50H0l5-5l-5-5l0,0v0l0,0l5-5l-5-5l0,0v0l0,0l5-5l-5-5l0,0v0l0,0l5-5l-5-5l0,0v0l0,0l5-5L0,0l0,0v0h10V50z"/></svg>
                           <b >Price-800&#8377</b></span></a></div>-->
<%--                          <p class="c-box__txt"style="width:350px;">NOTE:Cannot be clubbed with other offers. Prices does not include taxes.<br />Pick and choose appetizers from our menu</p>--%>

                        </div>
                          
                        <div data-bgimage="images/high-food.jpg" class="b-box__img-wrap  bg-image"></div>
                      <!--  <div class="c-box__date-item">
<%--                            <h2 style="color:white;"><strong>VALIDITY</strong></h2>--%>
                          <time datetime="2009-08-29T20:00" class="date c-box__date"><span class="date__dt">30</span><span class="date__rh"><span class="date__rh-m">June</span><span class="date__rh-d">Sat</span></span><span class="date__ad">8:00 pm</span></time>
                        </div>-->
                        <div class="c-box__link-wrap" style="right:380px">
                          <div class="btn-pointer__wrap">
                            <a class="btn btn-pointer" href="contacthigh.aspx"><span class="btn-pointer__txt">Enquire Now</span></a>
                            <svg class="btn-pointer__left" x="0px" y="0px"
                            viewBox="0 0 10 50" style="enable-background:new 0 0 10 50;" xml:space="preserve">
                            <path fill="#A82743" d="M10,50H0l5-5l-5-5l0,0v0l0,0l5-5l-5-5l0,0v0l0,0l5-5l-5-5l0,0v0l0,0l5-5l-5-5l0,0v0l0,0l5-5L0,0l0,0v0h10V50z"/></svg>
                            
                          </div>
                        </div><a href="#" rel="nofollow" class="b-box__link"></a>
                      <div class="c-box__link-wrap"  style="right:2px">
                          <div class="btn-pointer__wrap">
                            <a class="btn btn-pointer" href="https://www.zomato.com/bangalore/high-ultra-lounge-malleshwaram/book"><span class="btn-pointer__txt">Book Now</span></a>
                            <svg class="btn-pointer__left" x="0px" y="0px"
                            viewBox="0 0 10 50" style="enable-background:new 0 0 10 50;" xml:space="preserve">
                            <path fill="#A82743" d="M10,50H0l5-5l-5-5l0,0v0l0,0l5-5l-5-5l0,0v0l0,0l5-5l-5-5l0,0v0l0,0l5-5l-5-5l0,0v0l0,0l5-5L0,0l0,0v0h10V50z"/></svg>
                           </div>
                        </div><a href="#" rel="nofollow" class="b-box__link"></a>

                      </div>
                    </div>
                  </div>
                                      

                  <div class="b-tabs__pane-item col-sm-6">
                    <div class="c-box">
                      <div class="c-box__inner">
                        <div class="c-box__cont">
                          <h3 class="c-box__ttl-s3">OFFER 2</h3>
                          <hr class="hr-pattern">
                          <p class="c-box__txt"style="width:250px;">3 Vegetarian and 3 Non vegetarian appetizers</p>
                            <br /> <p class="c-box__txt" style="width:250px">Offer Valid Till JUNE 30TH SAT </p><br />
                         <!--   <div class="col-xs-12 col-xm-6"><a href="#" class="btn indent btn-info btn-anim-a btn--act-a"style="width:220px;">
                          <span class="clr-black"style="height:50px;"></span>
                          <span><svg x="0px" y="0px"
                          viewBox="0 0 10 50" style="enable-background:new 0 0 10 50;" xml:space="preserve">
                          <path fill="#A82743" d="M10,50H0l5-5l-5-5l0,0v0l0,0l5-5l-5-5l0,0v0l0,0l5-5l-5-5l0,0v0l0,0l5-5l-5-5l0,0v0l0,0l5-5L0,0l0,0v0h10V50z"/></svg>
                              
                           <b >Price-1200&#8377</b></span></a></div>-->
<%--                          <p class="c-box__txt"style="width:350px;">NOTE:Cannot be clubbed with other offers. Prices does not include taxes.<br />Pick and choose appetizers from our menu</p>--%>

                        </div>
                        <div data-bgimage="images/super-high.jpg" class="b-box__img-wrap bg-image"></div>
                       <!-- <div class="c-box__date-item">
<%--                              <h3 style="color:white;top:20px;"><strong>VALIDITY</strong></h3>--%>
                          <time datetime="2009-08-29T20:00" class="date c-box__date"><span class="date__dt">30</span><span class="date__rh"><span class="date__rh-m">June</span><span class="date__rh-d">Sat</span></span><span class="date__ad">8:00 pm</span></time>
                        </div>-->
                        
                  <div class="c-box__link-wrap" style="right:380px">
                          <div class="btn-pointer__wrap">
                            <a class="btn btn-pointer" href="contacthigh.aspx"><span class="btn-pointer__txt">Enquire Now</span></a>
                            <svg class="btn-pointer__left" x="0px" y="0px"
                            viewBox="0 0 10 50" style="enable-background:new 0 0 10 50;" xml:space="preserve">
                            <path fill="#A82743" d="M10,50H0l5-5l-5-5l0,0v0l0,0l5-5l-5-5l0,0v0l0,0l5-5l-5-5l0,0v0l0,0l5-5l-5-5l0,0v0l0,0l5-5L0,0l0,0v0h10V50z"/></svg>
                            
                          </div>
                        </div><a href="#" rel="nofollow" class="b-box__link"></a>



                          <div class="c-box__link-wrap"  style="right:2px">
                          <div class="btn-pointer__wrap">
                            <a class="btn btn-pointer" href="https://www.zomato.com/bangalore/high-ultra-lounge-malleshwaram/book"><span class="btn-pointer__txt">Book Now</span></a>
                            <svg class="btn-pointer__left" x="0px" y="0px"
                            viewBox="0 0 10 50" style="enable-background:new 0 0 10 50;" xml:space="preserve">
                            <path fill="#A82743" d="M10,50H0l5-5l-5-5l0,0v0l0,0l5-5l-5-5l0,0v0l0,0l5-5l-5-5l0,0v0l0,0l5-5l-5-5l0,0v0l0,0l5-5L0,0l0,0v0h10V50z"/></svg>
                           </div>
                        </div><a href="#" rel="nofollow" class="b-box__link"></a>




                      </div>
                    </div>
                  </div>
                  <div class="b-tabs__pane-item col-sm-6">
                    <div class="c-box">
                      <div class="c-box__inner">
                        <div class="c-box__cont">
                            
                          <h3 class="c-box__ttl-s3">OFFER 3 </h3>
                         
                            
                          <hr class="hr-pattern">
                            <p class="c-box__txt"style="width:250px;">4 Vegetarian and 4 Non vegetarian appetizers</p><br />
                             <p class="c-box__txt" style="width:250px">Offer Valid Till JUNE 30TH SAT </p><br />
                          <!--  <div class="col-xs-12 col-xm-6"><a href="#" class="btn indent btn-info btn-anim-a btn--act-a"style="width:220px;">
                          <span class="clr-black"style="height:50px;"></span>
                          <span><svg x="0px" y="0px"
                          viewBox="0 0 10 50" style="enable-background:new 0 0 10 50;" xml:space="preserve">
                          <path fill="#A82743" d="M10,50H0l5-5l-5-5l0,0v0l0,0l5-5l-5-5l0,0v0l0,0l5-5l-5-5l0,0v0l0,0l5-5l-5-5l0,0v0l0,0l5-5L0,0l0,0v0h10V50z"/></svg>
                           <b >Price-1600&#8377</b></span></a></div>-->
                            <br />
                            <br />
                              
<%--                          <p class="c-box__txt">Stay B, Andrew Feeling, Someone Else</p>--%>
                        </div>
                          <br />
                          <br />

                        <div data-bgimage="images/high-food18.jpg" class="b-box__img-wrap  bg-image"></div>
                       <!-- <div class="c-box__date-item">
<%--                            <h3 style="color:white;top:20px;"><strong>VALIDITY</strong></h3>--%>
                          <time datetime="2009-08-29T20:00" class="date c-box__date"><span class="date__dt">30</span><span class="date__rh"><span class="date__rh-m">June</span><span class="date__rh-d">Sat</span></span><span class="date__ad">8:00 pm</span></time>
                        </div>-->
                        <div class="c-box__link-wrap" style="right:380px">
                          <div class="btn-pointer__wrap">
                            <a class="btn btn-pointer" href="contacthigh.aspx"><span class="btn-pointer__txt">Enquire Now</span></a>
                            <svg class="btn-pointer__left" x="0px" y="0px"
                            viewBox="0 0 10 50" style="enable-background:new 0 0 10 50;" xml:space="preserve">
                            <path fill="#A82743" d="M10,50H0l5-5l-5-5l0,0v0l0,0l5-5l-5-5l0,0v0l0,0l5-5l-5-5l0,0v0l0,0l5-5l-5-5l0,0v0l0,0l5-5L0,0l0,0v0h10V50z"/></svg>
                            
                          </div>
                        </div><a href="#" rel="nofollow" class="b-box__link"></a>



                          <div class="c-box__link-wrap"  style="right:2px">
                          <div class="btn-pointer__wrap">
                            <a class="btn btn-pointer" href="https://www.zomato.com/bangalore/high-ultra-lounge-malleshwaram/book"><span class="btn-pointer__txt">Book Now</span></a>
                            <svg class="btn-pointer__left" x="0px" y="0px"
                            viewBox="0 0 10 50" style="enable-background:new 0 0 10 50;" xml:space="preserve">
                            <path fill="#A82743" d="M10,50H0l5-5l-5-5l0,0v0l0,0l5-5l-5-5l0,0v0l0,0l5-5l-5-5l0,0v0l0,0l5-5l-5-5l0,0v0l0,0l5-5L0,0l0,0v0h10V50z"/></svg>
                           </div>
                        </div><a href="#" rel="nofollow" class="b-box__link"></a>




                      </div>
                    </div>
                  </div>
                  <div class="b-tabs__pane-item col-sm-6">
                    <div class="c-box">
                      <div class="c-box__inner">
                        <div class="c-box__cont">
                          <h3 class="c-box__ttl-s3">OFFER 4</h3>
                          <hr class="hr-pattern">
                          <p class="c-box__txt">COMING SOON</p>
                        </div>
                        <div data-bgimage="images/high-food8.jpg" class="b-box__img-wrap  bg-image"></div>
<%--                        <div class="c-box__date-item">
                          <time datetime="2009-08-29T20:00" class="date c-box__date"><span class="date__dt">23</span><span class="date__rh"><span class="date__rh-m">sept</span><span class="date__rh-d">Mon</span></span><span class="date__ad">8:00 pm</span></time>
                        </div>--%>
                        <div class="c-box__link-wrap" style="right:380px">
                          <div class="btn-pointer__wrap">
                            <a class="btn btn-pointer" href="contacthigh.aspx"><span class="btn-pointer__txt">Enquire Now</span></a>
                            <svg class="btn-pointer__left" x="0px" y="0px"
                            viewBox="0 0 10 50" style="enable-background:new 0 0 10 50;" xml:space="preserve">
                            <path fill="#A82743" d="M10,50H0l5-5l-5-5l0,0v0l0,0l5-5l-5-5l0,0v0l0,0l5-5l-5-5l0,0v0l0,0l5-5l-5-5l0,0v0l0,0l5-5L0,0l0,0v0h10V50z"/></svg>
                            
                          </div>
                        </div><a href="event-open.html" rel="nofollow" class="b-box__link"></a>





                          <div class="c-box__link-wrap"  style="right:2px">
                          <div class="btn-pointer__wrap">
                            <a class="btn btn-pointer" href="https://www.zomato.com/bangalore/high-ultra-lounge-malleshwaram/book"><span class="btn-pointer__txt">Book Now</span></a>
                            <svg class="btn-pointer__left" x="0px" y="0px"
                            viewBox="0 0 10 50" style="enable-background:new 0 0 10 50;" xml:space="preserve">
                            <path fill="#A82743" d="M10,50H0l5-5l-5-5l0,0v0l0,0l5-5l-5-5l0,0v0l0,0l5-5l-5-5l0,0v0l0,0l5-5l-5-5l0,0v0l0,0l5-5L0,0l0,0v0h10V50z"/></svg>
                           </div>
                        </div><a href="#" rel="nofollow" class="b-box__link"></a>






                      </div>
                    </div>
                  </div>
                  <div class="b-box__btn-wrap txt--centr col-sm-12">
<%--                    <div class="btn-icon__wrap">
                      <a href="#" class="btn btn-icon">More Offers</a>
                      <svg x="0px" y="0px"
                        width="20px" height="20px" viewBox="0 0 20 20" enable-background="new 0 0 20 20" xml:space="preserve">
                      <path fill-rule="evenodd" clip-rule="evenodd" stroke="#000000" stroke-width="2" stroke-miterlimit="10" d="M10.004,17V3"/>
                      <path fill-rule="evenodd" clip-rule="evenodd" stroke="#000000" stroke-width="2" stroke-miterlimit="10" d="M3,9.98h14"/>
                      </svg>
                    </div>--%>
                   <%--   <div class="btn-pointer-b__wrap"style="right:200px;">
            <a href="pdf/HUL%20Beverage%20(1).pdf" target="_blank" class="btn-pointer-b btn-anim-b"><b>Beverage Menu</b></a>
            <svg class="left" x="0px" y="0px" viewBox="0 0 5 50" enable-background="new 0 0 5 50" xml:space="preserve">
            <path fill="#000000" d="M5.0001,49.0002H0V33.8014l4.4054-4.4053L0,24.9905v-0.9612l4.4054-4.4051L0,15.2184V0h5.0001V49.0002z"/>
            </svg>
            
            <svg class="right" x="0px" y="0px" viewBox="0 0 5 50" enable-background="new 0 0 5 50" xml:space="preserve">
            <path fill="#000000" d="M0,0l5.0001,0v15.1988l-4.4054,4.4053l4.4054,4.4056v0.9612L0.5947,29.376l4.4054,4.4058v15.2184H0L0,0z"/>
            </svg>
            
          </div>--%>
                                         <%--       <div class="btn-pointer-b__wrap"style="left:200px;">
            <a href="pdf/HUL%20Food%20Menu.pdf" target="_blank" class="btn-pointer-b btn-anim-b"><b>FOOD MENU</b></a>
            <svg class="left" x="0px" y="0px" viewBox="0 0 5 50" enable-background="new 0 0 5 50" xml:space="preserve">
            <path fill="#000000" d="M5.0001,49.0002H0V33.8014l4.4054-4.4053L0,24.9905v-0.9612l4.4054-4.4051L0,15.2184V0h5.0001V49.0002z"/>
            </svg>
            
            <svg class="right" x="0px" y="0px" viewBox="0 0 5 50" enable-background="new 0 0 5 50" xml:space="preserve">
            <path fill="#000000" d="M0,0l5.0001,0v15.1988l-4.4054,4.4053l4.4054,4.4056v0.9612L0.5947,29.376l4.4054,4.4058v15.2184H0L0,0z"/>
            </svg>
            
          </div>--%>

                  </div>
                </div>
<%--                <div id="block-b" role="tabpanel" class="b-tabs__pane fade in row">
                  <div class="b-tabs__pane-item col-xs-12">
                    <div class="c-box c-box-v">
                      <div class="c-box__inner">
                        <div class="c-box__cont">
                          <h3 class="c-box__ttl-s3">OFFER 1</h3>
                          <p class="c-box__txt c-box__txt--display">Stay B, Andrew Feeling, Someone Else</p>
                        </div>
                        <div class="c-box__date-item">
                          <time datetime="2009-08-29T20:00" class="date c-box__date"><span class="date__dt">23</span><span class="date__rh"><span class="date__rh-m">sept</span><span class="date__rh-d">Mon</span></span><span class="date__ad">8:00 pm</span></time>
                        </div>
                        <div data-bgimage="../uploads/events/EventsTiles_01_Retina.jpg" class="b-box__img-wrap b-box__grdnt-a bg-image"></div>
                        <div class="c-box__link-wrap">
                          <div class="btn-pointer__wrap">
                            <a class="btn btn-pointer" href="event-open.html"><span class="btn-pointer__txt">More</span></a>
                            <svg class="btn-pointer__left" x="0px" y="0px"
                            viewBox="0 0 10 50" style="enable-background:new 0 0 10 50;" xml:space="preserve">
                            <path fill="#A82743" d="M10,50H0l5-5l-5-5l0,0v0l0,0l5-5l-5-5l0,0v0l0,0l5-5l-5-5l0,0v0l0,0l5-5l-5-5l0,0v0l0,0l5-5L0,0l0,0v0h10V50z"/></svg>
                            
                          </div>
                        </div><a href="event-open.html" rel="nofollow" class="b-box__link"></a>
                      </div>
                    </div>
                  </div>
                  <div class="b-tabs__pane-item col-xs-12">
                    <div class="c-box c-box-v">
                      <div class="c-box__inner">
                        <div class="c-box__cont">
                          <h3 class="c-box__ttl-s3">St. Patrick’s Day in New York</h3>
                          <p class="c-box__txt c-box__txt--display">Cruachan, Lunasa, Flook, McGoldrick's band</p>
                        </div>
                        <div class="c-box__date-item">
                          <time datetime="2016-09-24T20:00" class="date c-box__date"><span class="date__dt">23</span><span class="date__rh"><span class="date__rh-m">sept</span><span class="date__rh-d">Mon</span></span><span class="date__ad">8:00 pm</span></time>
                        </div>
                        <div data-bgimage="../uploads/events/EventsTiles_02_Retina.jpg" class="b-box__img-wrap b-box__grdnt-a bg-image"></div>
                        <div class="c-box__link-wrap">
                          <div class="btn-pointer__wrap">
                            <a class="btn btn-pointer" href="event-open.html"><span class="btn-pointer__txt">More</span></a>
                            <svg class="btn-pointer__left" x="0px" y="0px"
                            viewBox="0 0 10 50" style="enable-background:new 0 0 10 50;" xml:space="preserve">
                            <path fill="#A82743" d="M10,50H0l5-5l-5-5l0,0v0l0,0l5-5l-5-5l0,0v0l0,0l5-5l-5-5l0,0v0l0,0l5-5l-5-5l0,0v0l0,0l5-5L0,0l0,0v0h10V50z"/></svg>
                            
                          </div>
                        </div><a href="event-open.html" rel="nofollow" class="b-box__link"></a>
                      </div>
                    </div>
                  </div>
                  <div class="b-tabs__pane-item col-xs-12">
                    <div class="c-box c-box-v">
                      <div class="c-box__inner">
                        <div class="c-box__cont">
                          <h3 class="c-box__ttl-s3">TRAP  ONLY. best records plus fit</h3>
                          <p class="c-box__txt c-box__txt--display">Stay B, Andrew Feeling, Someone Else</p>
                        </div>
                        <div class="c-box__date-item">
                          <time datetime="2009-08-29T20:00" class="date c-box__date"><span class="date__dt">23</span><span class="date__rh"><span class="date__rh-m">sept</span><span class="date__rh-d">Mon</span></span><span class="date__ad">8:00 pm</span></time>
                        </div>
                        <div data-bgimage="../uploads/events/EventsTiles_03_Retina.jpg" class="b-box__img-wrap b-box__grdnt-a bg-image"></div>
                        <div class="c-box__link-wrap">
                          <div class="btn-pointer__wrap">
                            <a class="btn btn-pointer" href="event-open.html"><span class="btn-pointer__txt">More</span></a>
                            <svg class="btn-pointer__left" x="0px" y="0px"
                            viewBox="0 0 10 50" style="enable-background:new 0 0 10 50;" xml:space="preserve">
                            <path fill="#A82743" d="M10,50H0l5-5l-5-5l0,0v0l0,0l5-5l-5-5l0,0v0l0,0l5-5l-5-5l0,0v0l0,0l5-5l-5-5l0,0v0l0,0l5-5L0,0l0,0v0h10V50z"/></svg>
                            
                          </div>
                        </div><a href="event-open.html" rel="nofollow" class="b-box__link"></a>
                      </div>
                    </div>
                  </div>
                  <div class="b-tabs__pane-item col-xs-12">
                    <div class="c-box c-box-v">
                      <div class="c-box__inner">
                        <div class="c-box__cont">
                          <h3 class="c-box__ttl-s3">TRAP  ONLY. best records plus fit</h3>
                          <p class="c-box__txt c-box__txt--display">Stay B, Andrew Feeling, Someone Else</p>
                        </div>
                        <div class="c-box__date-item">
                          <time datetime="2009-08-29T20:00" class="date c-box__date"><span class="date__dt">23</span><span class="date__rh"><span class="date__rh-m">sept</span><span class="date__rh-d">Mon</span></span><span class="date__ad">8:00 pm</span></time>
                        </div>
                        <div data-bgimage="../uploads/events/EventsTiles_04_Retina.jpg" class="b-box__img-wrap b-box__grdnt-a bg-image"></div>
                        <div class="c-box__link-wrap">
                          <div class="btn-pointer__wrap">
                            <a class="btn btn-pointer" href="event-open.html"><span class="btn-pointer__txt">More</span></a>
                            <svg class="btn-pointer__left" x="0px" y="0px"
                            viewBox="0 0 10 50" style="enable-background:new 0 0 10 50;" xml:space="preserve">
                            <path fill="#A82743" d="M10,50H0l5-5l-5-5l0,0v0l0,0l5-5l-5-5l0,0v0l0,0l5-5l-5-5l0,0v0l0,0l5-5l-5-5l0,0v0l0,0l5-5L0,0l0,0v0h10V50z"/></svg>
                            
                          </div>
                        </div><a href="event-open.html" rel="nofollow" class="b-box__link"></a>
                      </div>
                    </div>
                  </div>
<%--                  <div class="b-box__btn-wrap"><a href="" class="btn btn-icon btn-icon--plus">More events</a></div>
                </div>--%>
              </div>
            </div>
          
          </div>
        </div>
      </div>
    
    <!-- Current Offers 1-->
</asp:Content>

