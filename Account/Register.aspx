﻿<%@ Page Title="Register" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Register.aspx.cs" Inherits="Hul_login.Account.Register" %>

<asp:Content runat="server" ID="BodyContent" ContentPlaceHolderID="ContentPlaceHolder1">
  <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
<script type="text/javascript" src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>  
	<style>   @media screen and (max-width:640px) {
            .container{
               
                margin-top: 115px !important;
              
}
               
            }</style>

    <div class="b-events"style="background-image:url(/images/Party/Party12.jpg);background-attachment:fixed;filter:opacity(80%);background-size:100% 100%">
    
        <div class="container" style="height:100%;width:60%;display:flex;justify-content:center;align-items:center;background-color:#202020;opacity: 0.9;
    border: 1px solid #e15e32;">
           
    <div class="row" style="font-family:'Univers LT W01_57 Condensed' !important;color:red;width:100%;">
        <p >
        <asp:Literal runat="server" ID="ErrorMessage" />
    </p>
        <div class="col-md-8">
            <section id="loginForm">
                <div class="form-horizontal">
                  <%--  <h4>Use a local account to log in.</h4>
                    <hr />--%>
                    <asp:PlaceHolder runat="server" ID="PlaceHolder1" Visible="false">
                        <p class="text-danger">
                            <asp:Literal runat="server" ID="FailureText" />
                        </p>
                    </asp:PlaceHolder>

                    <div class="form-group row">
                        <div class="col-md-6">
                        <label class="c-box__ttl-s3" style=" font-family:'Univers LT W01_57 Condensed' !important">Salutation</label>
                        </div>          
                        <div class="col-md-6" >
                        <asp:DropDownList ID="Salute" runat="server">
                            <asp:ListItem Enabled="true" Text="Miss." Value="Miss."></asp:ListItem>
                            <asp:ListItem Text="Mrs." Value="Mrs."></asp:ListItem>
                            <asp:ListItem Text="Ms." Value="Ms."></asp:ListItem>
                            <asp:ListItem Text="Mr." Value="Mr."></asp:ListItem>
                        </asp:DropDownList>

                        </div>
                    </div>

                     <div class="form-group row">
                        <div class="col-md-6">
                        <label class="c-box__ttl-s3" style=" font-family:'Univers LT W01_57 Condensed' !important">First Name</label>
                        </div>          
                        <div class="col-md-6" >
                           <input type="text" runat="server" id="FirstName" class="form-control col-md-12"  />
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-6">
                        <label class="c-box__ttl-s3" style=" font-family:'Univers LT W01_57 Condensed' !important">Middle Name</label>
                        </div>          
                        <div class="col-md-6" >
                           <input type="text"  runat="server" id="MiddleName" class="form-control col-md-12"  />
                        </div>
                    </div>
                     <div class="form-group row">
                        <div class="col-md-6">
                        <label class="c-box__ttl-s3" style=" font-family:'Univers LT W01_57 Condensed' !important">Last Name</label>
                        </div>          
                        <div class="col-md-6" >
                           <input type="text"  runat="server" id="LastName" class="form-control col-md-12"  />
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-6">
                        <label class="c-box__ttl-s3" style=" font-family:'Univers LT W01_57 Condensed' !important">Date Of Birth</label>
                        </div>          
                        <div class="col-md-6" >
                         <input type="text" runat="server" id="Dob" class="form-control col-md-12"  />
                        </div>
                    </div>
                     <div class="form-group row">
                        <div class="col-md-6">
                        <label class="c-box__ttl-s3" style=" font-family:'Univers LT W01_57 Condensed' !important">Date Of Anniversary</label>
                        </div>          
                        <div class="col-md-6" >
                         <input type="text"  runat="server" id="Doa" class="form-control col-md-12"  />
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-md-6">
                        <label class="c-box__ttl-s3" style=" font-family:'Univers LT W01_57 Condensed' !important">Email</label>
                        </div>
                        <%--<asp:Label runat="server" AssociatedControlID="Email" CssClass="col-md-2 control-label" style="text-decoration-color:black;">Email</asp:Label>--%>
                        <div class="col-md-6" >
                            <asp:TextBox runat="server" ID="Email" CssClass="form-control col-md-12" TextMode="Email" />
                            <asp:RequiredFieldValidator runat="server" ControlToValidate="Email"
                                CssClass="text-danger col-md-12" ErrorMessage="The email field is required." style="margin-left:33%;width:100%;" />
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-6">
                        <label class="c-box__ttl-s3" style=" font-family:'Univers LT W01_57 Condensed' !important">Password</label>
                        </div>
                        <div class="col-md-6">
                            <asp:TextBox runat="server" ID="Password" TextMode="Password" CssClass="form-control  col-md-12"  />
                            <asp:RequiredFieldValidator runat="server" ControlToValidate="Password" CssClass="text-danger  col-md-12" ErrorMessage="The password field is required." style="margin-left:33%;width:100%;" />
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-6">
                        <label class="c-box__ttl-s3" style=" font-family:'Univers LT W01_57 Condensed' !important">Confirm <br /> Password</label>
                        </div>
                        <div class="col-md-6">
                            <asp:TextBox runat="server" ID="ConfirmPassword" TextMode="Password" CssClass="form-control  col-md-12" />
                              <asp:RequiredFieldValidator runat="server" ControlToValidate="ConfirmPassword"
                            CssClass="text-danger  col-md-12" Display="Dynamic" ErrorMessage="The confirm password field is required."   style="margin-left:33%;width:100%;"/>
                            <asp:CompareValidator runat="server" ControlToCompare="Password" ControlToValidate="ConfirmPassword"
                            CssClass="text-danger  col-md-12" Display="Dynamic" ErrorMessage="The password and confirmation password do not match." style="margin-left:33%;width:100%;" />
                        </div>
                    </div>


                      <div class="form-group row">
                        <div class="col-md-6">
                        <label class="c-box__ttl-s3" style=" font-family:'Univers LT W01_57 Condensed' !important">Residential Address</label>
                        </div>          
                        <div class="col-md-6" >
                       <input type="text" runat="server" id="Residential_Address" class="form-control col-md-12"  />
					 </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-6">
                        <label class="c-box__ttl-s3" style=" font-family:'Univers LT W01_57 Condensed' !important">Telephone Number</label>
                        </div>          
                        <div class="col-md-6" >
                           <input type="text" runat="server" id="Telephone_Number" class="form-control col-md-12"  />
                        </div>
                       </div>
                     <div class="form-group row">
                        <div class="col-md-6">
                        <label class="c-box__ttl-s3" style=" font-family:'Univers LT W01_57 Condensed' !important">Mobile Number</label>
                        </div>          
                        <div class="col-md-6" >
                           <input type="text" runat="server" id="Mobile_Number" class="form-control col-md-12"  />
                        </div>
                       </div>
                    <div class="form-group row">
                        <div class="col-md-6">
                        <label class="c-box__ttl-s3" style=" font-family:'Univers LT W01_57 Condensed' !important">Occupation</label>
                        </div>          
                        <div class="col-md-6" >
                           <input type="text" runat="server" id="Occupation" class="form-control col-md-12"  />
                        </div>
                       </div>
                     <div class="form-group row">
                        <div class="col-md-6">
                        <label class="c-box__ttl-s3" style=" font-family:'Univers LT W01_57 Condensed' !important">Organization</label>
                        </div>          
                        <div class="col-md-6" >
                           <input type="text" runat="server" id="Organization" class="form-control col-md-12"  />
                        </div>
                       </div>
                     <div class="form-group row">
                        <div class="col-md-6">
                        <label class="c-box__ttl-s3" style=" font-family:'Univers LT W01_57 Condensed' !important">Organization Address</label>
                        </div>          
                        <div class="col-md-6" >
                           <input type="text" runat="server" id="Organization_ADD" class="form-control col-md-12"  />
                        </div>
                       </div>

                    </div>

                 
                    <div class="form-group">
                        <div class="c-box__link-wrap" style="margin-top:15%;margin-left:45%;position:relative;top:38px;">
                            <asp:Button runat="server" OnClick="CreateUser_Click" Text="Register" CssClass="btn-pointer__txt btn btn-pointer" />
                        </div>
                    </div>
               
                
            </section>
        </div>
        </div>
        </div>
    </div>
  
<script type="text/javascript">


    $(document).ready(function () {
        $("#Dob,#Doa ").datepicker();

        $(".form-group").css("margin-top", 20);

       
    });
</script>




















    





    <%--<h2><%: Title %>.</h2>
    

    <div class="form-horizontal">
        <h4>Create a new account</h4>
        <hr />
        <asp:ValidationSummary runat="server" CssClass="text-danger" />
        <div class="form-group">
            <asp:Label runat="server" AssociatedControlID="Email" CssClass="col-md-2 control-label">Email</asp:Label>
            <div class="col-md-10">
                <asp:TextBox runat="server" ID="Email" CssClass="form-control" TextMode="Email" />
                <asp:RequiredFieldValidator runat="server" ControlToValidate="Email"
                    CssClass="text-danger" ErrorMessage="The email field is required." />
            </div>
        </div>
        <div class="form-group">
            <asp:Label runat="server" AssociatedControlID="Password" CssClass="col-md-2 control-label">Password</asp:Label>
            <div class="col-md-10">
                <asp:TextBox runat="server" ID="Password" TextMode="Password" CssClass="form-control" />
                <asp:RequiredFieldValidator runat="server" ControlToValidate="Password"
                    CssClass="text-danger" ErrorMessage="The password field is required." />
            </div>
        </div>
        <div class="form-group">
            <asp:Label runat="server" AssociatedControlID="ConfirmPassword" CssClass="col-md-2 control-label">Confirm password</asp:Label>
            <div class="col-md-10">
                <asp:TextBox runat="server" ID="ConfirmPassword" TextMode="Password" CssClass="form-control" />
                <asp:RequiredFieldValidator runat="server" ControlToValidate="ConfirmPassword"
                    CssClass="text-danger" Display="Dynamic" ErrorMessage="The confirm password field is required." />
                <asp:CompareValidator runat="server" ControlToCompare="Password" ControlToValidate="ConfirmPassword"
                    CssClass="text-danger" Display="Dynamic" ErrorMessage="The password and confirmation password do not match." />
            </div>
        </div>
        <div class="form-group">
            <div class="col-md-offset-2 col-md-10">
                <asp:Button runat="server" OnClick="CreateUser_Click" Text="Register" CssClass="btn btn-default" />
            </div>
        </div>
    </div>--%>
</asp:Content>
