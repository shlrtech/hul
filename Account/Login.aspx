﻿<%@ Page Title="Log in" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="Hul_login.Account.Login" Async="true" %>

<%@ Register Src="~/Account/OpenAuthProviders.ascx" TagPrefix="uc" TagName="OpenAuthProviders" %>
	
	
	  
<asp:Content runat="server" ID="BodyContent" ContentPlaceHolderID="ContentPlaceHolder1">
    
   <style type="text/css">
        @media screen and (max-width:640px) {
            .b-events {padding-top:200px!important;
            }
            .container { width:95%!important;
        }
             .form-control{
                margin-left:0!important;
                margin-top:10px!important;
            }
             .RememberMe{
                margin-left:0!important;
            }
             .c-box__link-wrap{
                   padding-right:20%!important;
                     margin-bottom:80px!important;
             }
            .linkRef {
            padding-top:10px!important;
            }
             .checkbox{
                margin-left:0!important;
                  margin-bottom:80px!important;
            }
        }
         @media screen and (max-width:960px) {
            .b-events {padding-top:200px!important;
            }
            .container { width:95%!important;
        }
             .form-control{
                margin-left:0!important;
                 margin-top:10px!important;
            }
             .checkbox{
                margin-left:0!important;
                  margin-bottom:80px!important;
            }
              .c-box__link-wrap{
                      padding-right:20%!important;
                        margin-bottom:80px!important;
             } 
               .linkRef {
            padding-top:10px!important;
            }
        }
          @media screen and (max-width:1200px) {
            .b-events {padding-top:200px!important;
            }
            .container { width:95%!important;
        }
            .form-control{
                margin-left:0!important;
                 margin-top:10px!important;
            }
            .checkbox{
                margin-left:0!important;
                margin-bottom:80px!important;
            }
             .c-box__link-wrap{
                 padding-right: 40%!important;
                    margin-bottom: 30px!important;
             } 
              .linkRef {
            padding-top:10px!important;
            }
        }
        
    </style>
  
    <div class="b-events"style="background-image:url(/images/Party/Party12.jpg);background-attachment:fixed;filter:opacity(80%);background-size:100%;">
    
        <div class="container" style="height:100%;width:40%;display:flex;justify-content:center;align-items:center;background-color:#202020;opacity: 0.9;
    border: 1px solid #e15e32; margin-bottom:100px;margin-top:50px;">
           
    <div class="row" style="font-family:'Univers LT W01_57 Condensed' !important;color:black;">

        <div class="col-md-8">
            <section id="loginForm">
                <div class="form-horizontal">
                  <%--  <h4>Use a local account to log in.</h4>
                    <hr />--%>
                    <asp:PlaceHolder runat="server" ID="ErrorMessage" Visible="false">
                        <p class="text-danger">
                            <asp:Literal runat="server" ID="FailureText" />
                        </p>
                    </asp:PlaceHolder>
                    <div class="form-group" style="">
                        <div class="col-md-2 col-sm-12 col-xs-12">
                        <label class="c-box__ttl-s3  " style=" font-family:'Univers LT W01_57 Condensed' !important;">Email</label>
                        </div>
                        <%--<asp:Label runat="server" AssociatedControlID="Email" CssClass="col-md-2 control-label" style="text-decoration-color:black;">Email</asp:Label>--%>
                        <div class="col-md-10" >
                            <asp:TextBox runat="server" ID="Email" CssClass="form-control " TextMode="Email" style="margin-left:33%;width:100%;"/>
                            <asp:RequiredFieldValidator runat="server" ControlToValidate="Email"
                                CssClass="text-danger col-md-10" ErrorMessage="The email field is required." style="margin-left:33%;width:100%;" />
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-2 col-sm-12 colxs-12">
                        <label class="c-box__ttl-s3" style=" font-family:'Univers LT W01_57 Condensed' !important">Password</label>
                        </div>
                        <div class="col-md-10">
                            <asp:TextBox runat="server" ID="Password" TextMode="Password" CssClass="form-control"  style="margin-left:33%;width:100%;"/>
                            <asp:RequiredFieldValidator runat="server" ControlToValidate="Password" CssClass="text-danger col-md-10" ErrorMessage="The password field is required." style="margin-left:33%;width:100%;" />
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-offset-2 col-md-10">
                            <div class="checkbox">
                                <asp:CheckBox runat="server" ID="RememberMe" CssClass="checkbox" style="margin-left:33%"/>
                                <label style=" font-family:'Univers LT W01_57 Condensed' !important;color:white;" >Remember me?</label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group ">
                        <div class="c-box__link-wrap   ">
                            <asp:Button runat="server" OnClick="LogIn" Text="Log in" CssClass="btn-pointer__txt btn btn-pointer " />
                        </div>
                    </div>
                   
                    <p>
                    <asp:HyperLink runat="server" ID="RegisterHyperLink" CssClass="linkRef" ViewStateMode="Disabled" style=" font-family:'Univers LT W01_57 Condensed' !important;margin-left:45%;width:100%;">Register as a new user</asp:HyperLink>
                </p>
                </div>
                
                <p>
                    <%-- Enable this once you have account confirmation enabled for password reset functionality
                    <asp:HyperLink runat="server" ID="ForgotPasswordHyperLink" ViewStateMode="Disabled">Forgot your password?</asp:HyperLink>
                    --%>
                </p>
            </section>
        </div>
        </div>
       <%-- <div class="col-md-4">
            <section id="socialLoginForm">
                <uc:OpenAuthProviders runat="server" ID="OpenAuthLogin" />
            </section>
        </div>--%>
    </div>
        </div>
</asp:Content>


