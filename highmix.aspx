﻿<%@ Page Title="" Language="C#" MasterPageFile="MasterPage.master" AutoEventWireup="true"
    CodeFile="highmix.aspx.cs" Inherits="highmix" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <!-- Sliding start -->
    <div class="main-slide">
        <div class="main-slide__btn-item"></div>
        <div id="sliderMain" class="main-slide__slider">
          <div class="main-slide__item-a">
            <div data-bgimage="images/high-dine.jpg" class="main-slide__img-wrap bg-image"></div>
            <div class="container" >
              
            </div>
          </div>
        
          <div class="main-slide__item-b" >
             
            <div class="main-slide__bg-wrap bg-grdnt"  style="background-image:url(images/high-mix.jpg)"></div>
            <div class="container">
              
            </div>
          </div>
        </div>
      </div>
     <!-- Sliding end -->

    <!-- high mix-->

    <div class="b-promo" style="background-color:black; border-top:solid;border-color:darkgoldenrod;">
        <div class ="row">
        <div class="col-md-6">
          <h2 class="b-promo__s2-ttl" style="color:goldenrod;">‘HIGH MIX'</h2>
          <p style="color:white;text-align: justify;">
         The linear 10,000 sq. ft. of area is cleverly demarcated into 4 zones that can be easily 
              integrated or separated from one another. The rationale behind dividing the spaces
               was that each would meet your mood and occasion.<br />
              'High Mix' is the entry point, a high energy enclosed bar where DJ’s and mixologists
               interact with a dance floor. A reception space with retail and seating is the elevator
               drop off point and leads up one flight of stairs to a generous foyer. The foyers' most 
              arresting feature is a sculpted fluid resin wall that flows into the entrance of 'High Mix'. 
              Its futuristic aesthetic is awash in hues of magenta and purple light and is juxtaposed with
               warm contrasting walls of vertical slatted timber.
            
          </p>
          
        </div>
             <div class="col-md-6">
         
           <img src="images/high_mix_form.jpg" style="width: 100%;height: 315px;"/>
          <div class="btn-pointer-b__wrap">
            <a href="contacthigh.aspx" class="btn-pointer-b btn-anim-b"><b>Enquiry</b></a>
            <svg class="left" x="0px" y="0px" viewBox="0 0 5 50" enable-background="new 0 0 5 50" xml:space="preserve">
            <path fill="#000000" d="M5.0001,49.0002H0V33.8014l4.4054-4.4053L0,24.9905v-0.9612l4.4054-4.4051L0,15.2184V0h5.0001V49.0002z"/>
            </svg>
            
            <svg class="right" x="0px" y="0px" viewBox="0 0 5 50" enable-background="new 0 0 5 50" xml:space="preserve">
            <path fill="#000000" d="M0,0l5.0001,0v15.1988l-4.4054,4.4053l4.4054,4.4056v0.9612L0.5947,29.376l4.4054,4.4058v15.2184H0L0,0z"/>
            </svg>
            
          </div>
        </div>
      </div>
    </div>

    <!--high mix-->
</asp:Content>
