﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeFile="drinksanddine.aspx.cs" Inherits="drinksanddine" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <!-- Sliding start -->

<%--    <div class="main-slide" style="min-height:250px;">
        <div class="main-slide__btn-item"></div>
        <div id="sliderMain" class="main-slide__slider">
          <div class="main-slide__item-a">
            <div data-bgimage="images/high-dine.jpg" class="main-slide__img-wrap bg-image"></div>
            <div class="container" >
              
            </div>
          </div>
        
          <div class="main-slide__item-b" >
             
            <div class="main-slide__bg-wrap bg-grdnt"  style="background-image:url(images/high-mix.jpg)"></div>
            <div class="container">
              
            </div>
          </div>
        </div>
      </div>--%>

 <div class="b-headlines"style="background-image:url(images/high-edge.jpg);height:350px;">
        <div class="container" >
          <div class="b-breadcrumbs">
            
          </div>
           
        </div>
      </div>
        </div>
   
     <!-- Sliding end -->

    <!--drinksanddine-->

    <div class="b-promo" style="background-color:black; border-top:solid;border-color:darkgoldenrod;">
        <div class="container">
          <h2 class="b-promo__s2-ttl" style="color:goldenrod;">‘Drink & Dine !'</h2>
          <p style="color:white;">
          The smoking cocktails are a bespoke experience for our guests ,the apparatus itself is a Taylor made product from hand 
              crafted bohemian glass with a design inspired by elegant organic lines which are indigenous to underwater sea life . <br /><br />
          The smoking cocktails are an innovative integration of traditional hand craft along with cutting edge technologies and
               techniques, the fruits added to the instrument brings a whole new dimension to the experience while adding a new 
              element to the tobacco mixture on a whole .Also the addition of alcohol. Further enhances the flavor and gives you
               an array of various combinations of flavors to choose from like citrus tang ,mojito passion, vanilla sky.
            
          </p>

          <div class="btn-pointer-b__wrap">
            <a href="pdf/High Ultra Lounge Beverage menu.pdf" target="_blank" class="btn-pointer-b btn-anim-b"><b>Download Menu</b></a>
            <svg class="left" x="0px" y="0px" viewBox="0 0 5 50" enable-background="new 0 0 5 50" xml:space="preserve">
            <path fill="#000000" d="M5.0001,49.0002H0V33.8014l4.4054-4.4053L0,24.9905v-0.9612l4.4054-4.4051L0,15.2184V0h5.0001V49.0002z"/>
            </svg>
            
            <svg class="right" x="0px" y="0px" viewBox="0 0 5 50" enable-background="new 0 0 5 50" xml:space="preserve">
            <path fill="#000000" d="M0,0l5.0001,0v15.1988l-4.4054,4.4053l4.4054,4.4056v0.9612L0.5947,29.376l4.4054,4.4058v15.2184H0L0,0z"/>
            </svg>
            
          </div>


        </div>
      </div>
    
    <!--drinksanddine-->
</asp:Content>

